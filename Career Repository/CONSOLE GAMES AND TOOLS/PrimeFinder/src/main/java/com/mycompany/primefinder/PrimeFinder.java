/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.primefinder;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PrimeFinder {

    public static void main(String[] args) {
        int answer = 0;
        int primeNumbers = 0;
        int counter = 0;
        Scanner sc = new Scanner(System.in);
        stateGame();
        answer = getUserInput();
        for (int i = 1; i < answer; i++) {

            if (isPrime(i)) {
                System.out.println(i + " is a prime number!");
                primeNumbers++;

            }
        }
        repeatFinder(primeNumbers);
    }

    public static void stateGame() {
        System.out.println("Prime finder is a program that will find all the prime numbers bewtween 0 and the number you provide.");
    }

    public static int getUserInput() {
        Scanner sc = new Scanner(System.in);
        String userInput = "";
        int userInt = 0;
        System.out.print("Which number do you want to find the prime of? ");
        userInput = sc.nextLine();
        userInt = Integer.parseInt(userInput);
        while (userInt <= 0) {
            userInput = sc.nextLine();
            userInt = Integer.parseInt(userInput);
        }

        return userInt;

    }

    public static void repeatFinder(int prime) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Would you like to play again (yes/no)? ");
        String playAgain = "";
        String userInput = "";
        playAgain = sc.nextLine();
        userInput = cleanInput(userInput);

        do {
            System.out.println("Please input a valid answer (yes/no).");
            playAgain = sc.nextLine();
            userInput = cleanInput(userInput);
        } while (playAgain != "yes" || playAgain != "no");

        if (playAgain == "yes") {
            stateGame();
        } else {
            System.out.println("Thank you for playing, you have found " + prime + " prime numbers!");
        }
    }

    public static String cleanInput(String input) {
        input = input.toLowerCase();
        return input;
    }

    public static boolean isPrime(int x) {
        if (x == 1 || x == 2) {
            return true;
        }
        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }
}
