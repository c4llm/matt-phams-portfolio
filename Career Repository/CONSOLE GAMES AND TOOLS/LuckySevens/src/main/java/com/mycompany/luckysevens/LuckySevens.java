/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.luckysevens;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    public static void main(String[] args) {
        Scanner Input = new Scanner(System.in);
        Random dice = new Random();
        String userInput = "";
        int diceRoll = 0;
        int rollCount = 0;
        float maxMoney = 0;
        int rollAtMaxMoney = 0;
        float userFloat = 0f;

        if (userFloat <= 0f) {
            System.out.print("Please insert bet amount:");
            userInput = Input.nextLine();
            userFloat = Float.parseFloat(userInput);
            maxMoney = userFloat;
        }

        while (userFloat > 0f) {
            diceRoll = dice.nextInt(11) + 2;
            if (diceRoll == 7) {
                userFloat = userFloat + 4f;
                rollCount += 1;
                if (userFloat > maxMoney) {
                    maxMoney = userFloat;
                    rollAtMaxMoney = rollCount;
                }
            } else {
                userFloat -= 1f;
                rollCount += 1;
            }

        }

        easyPrint("You are out of money! GAME OVER");
        easyPrint("You have rolled " + rollCount + " times.");
        easyPrint("Your maximum amount of cash was $" + maxMoney);
        easyPrint("Your number of rolls when you had the most cash was " + rollAtMaxMoney);
    }

    static public void easyPrint(String print) {
        System.out.println(print);
    }
}
