/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interestcalculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculator {

    public static void main(String[] args) {
        float interestRate = 0;
        float initialAmount = 0;
        float numberOfYears = 0;
        float principleBegin = 0;
        float princlpleEnd = 0;
        float interestEarned = 0;
        float princeBegin = 0;
        float princeCurrent = 0;
        float princeEnd = 0;
        boolean askCompound = true;
        int compoundRate = 0;

        Scanner sc = new Scanner(System.in);

        interestRate = takeUserInputNumber("What is your interest rate (%)?");
        initialAmount = takeUserInputNumber("How much would you like to invest? ");
        numberOfYears = takeUserInputNumber("How many years would you like to invest? ");

        while (askCompound) {
            System.out.print("How would you like to compound your interest? (quarterly = q , monthly = m , daily = d): ");
            String userInput = "";
            String userInputLower = "";
            userInput = sc.nextLine();
            userInputLower = userInput.toLowerCase();
            switch (userInput) {     
                case "quarterly":
                    compoundRate = 4;
                    askCompound = false;
                    break;
                case "q":
                    compoundRate = 4;
                    askCompound = false;
                    break;
                case "monthly":
                    compoundRate = 12;
                    askCompound = false;
                    break;
                case "m":
                    compoundRate = 12;
                    askCompound = false;
                    break;
                case "daily":
                    compoundRate = 365;
                    askCompound = false;
                    break;
                case "d":
                    compoundRate = 365;
                    askCompound = false;
                    break;
                default:
                    System.out.println("Please provide valid answer (quarterly = q , monthly = m , daily = d)");
                    askCompound = true;
            }
        }
        interestRate /= compoundRate;
        interestRate /= 100;

        for (int i=1 ; i <= numberOfYears; i++) {
            princeBegin = initialAmount;
            for (int p = 0; p< compoundRate; p++) {
                initialAmount = initialAmount * (1 + interestRate);
            }
            princeEnd = initialAmount;
            interestEarned = princeEnd - princeBegin;
            System.out.println("In year " + i + " you made $" + princeBegin + " at the beginning of the year, "
                    + princeEnd + " at the end of the year, and earned $" + interestEarned + " in interest!");
        }

    }

    public static float takeUserInputNumber(String question) {
        Scanner sc = new Scanner(System.in);
        String userInput = "";
        float userFloat = 0;

        while (userFloat <= 0) {
            System.out.print(question);
            userInput = sc.nextLine();
            userFloat = Float.parseFloat(userInput);
        }
        return userFloat;
    }

}
