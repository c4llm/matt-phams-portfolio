/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rockpaperscissors;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RockPaperScissors {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random compRand = new Random();
        String userAnswer = "";
        String wannaPlayAgain = "";
        int ansNumber = 0;
        int compAnswer = 0;
        boolean askChoice = true;

        while (askChoice) {
            userAnswer = userInput("Let's play rock, paper, scissors. Please choose rock, paper, or scissors: ");
            userAnswer = cleanInput(userAnswer);
            switch (userAnswer) {
                case "rock":
                    ansNumber = 1;
                    askChoice = false;
                    break;
                case "paper":
                    ansNumber = 2;
                    askChoice = false;
                    break;
                case "scissors":
                    ansNumber = 3;
                    askChoice = false;
                    break;
            }

            compAnswer = compRand.nextInt(3) + 1;
            calculate(ansNumber, compAnswer);

        }

    }

    public static String userInput(String askQuestion) {
        Scanner scan = new Scanner(System.in);
        String userAnswer = "";
        System.out.print(askQuestion);
        userAnswer = scan.nextLine();
        return userAnswer;

    }

    public static String cleanInput(String input) {
        input = input.toLowerCase();
        return input;
    }

    public static void calculate(int myAnswer, int computerAnswer) {
        if (myAnswer == 1 && computerAnswer == 1) {
            System.out.println("You both chose rock, and have tied!");
        }
        else if (myAnswer == 1 && computerAnswer == 2)
        {
            System.out.println("You chose rock and the computer chose paper, YOU HAVE LOST!");
        }
        else if (myAnswer == 1 && computerAnswer == 3)
        {
            System.out.println("You chose rock and the computer chose scissors, YOU HAVE WON!");
        }
        else if (myAnswer == 2 && computerAnswer == 1)
        {
            System.out.println("You chose paper and the computer chose rock, YOU HAVE WON!");
        }
        else if (myAnswer == 2 && computerAnswer == 2) 
        {
            System.out.println("You both chose paper, and have tied!");
        }
        else if (myAnswer == 2 && computerAnswer == 3)
        {
            System.out.println("You chose paper and the computer chose scissors, YOU HAVE LOST!");
        }
        else if (myAnswer == 3 && computerAnswer == 1)
        {
            System.out.println("You chose scissors and the computer chose rock, YOU HAVE LOST!");
        }
        else if(myAnswer == 3 && computerAnswer == 2) 
        {
            System.out.println("You chose scissors and the computer chose paper, YOU HAVE WON");
        }
        else if (myAnswer == 3 && computerAnswer == 3)
        {
            System.out.println("You both chose scissors, and have tied!");
        }     
    }

}
