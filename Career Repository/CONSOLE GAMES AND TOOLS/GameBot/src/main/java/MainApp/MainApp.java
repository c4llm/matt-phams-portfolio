/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainApp;

import com.mycompany.gamebot.blackjack.BlackJack;
import com.mycompany.gamebot.interest.ConsoleIo;
import com.mycompany.gamebot.interest.InterestCalculator;
import com.mycompany.gamebot.luckysevens.LuckySevens;
import com.mycompany.gamebot.rps.RockPaperScissors;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class MainApp {
    static ConsoleIo console = new ConsoleIo();
    public static void main(String[] args) {
        
        List<Game> games = new ArrayList<>();
        
        Game blackjack = new BlackJack();
        Game luckysevens = new LuckySevens();
        Game interest = new InterestCalculator();
        Game rps = new RockPaperScissors();
        
        games.add(blackjack);
        games.add(luckysevens);
        games.add(interest);
        games.add(rps);
        
        boolean playAgain = true;
        
        while(playAgain){
            Game game = gameToPlay( games);
            game.play();
            
           int selection = console.getUserInputInt("Type 1 to play again. Any other number to exit ");
        
           if(selection != 1){
               playAgain = false;
           }
        }
    }

    private static Game gameToPlay (List<Game> games) {
     
        console.print("Please select a number to choose a game to play");
        
        int i = 0;
        
        for(Game game : games){
            console.print(++i + ". "+game.getName());
        }
        
        int choice = console.getUserInputInt("");
        
        Game game=games.get(choice-1);
        return game;
    }
}
