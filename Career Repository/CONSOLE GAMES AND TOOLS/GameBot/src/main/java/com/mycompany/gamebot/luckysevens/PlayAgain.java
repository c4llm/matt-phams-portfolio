/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gamebot.luckysevens;

/**
 *
 * @author apprentice
 */
public class PlayAgain {
      
     ConsoleIO consoleio = new ConsoleIO();
    
    public boolean doesUserWantToPlayAgain() {
        
        
        
        boolean isValid = true;
        boolean play = false;
        
        String userInput;
        
        while (isValid) {
            
            userInput = consoleio.getUserInputString("Do you want to play again? Please type yes or no. ");
            
            
            
            String yesOrNo = userInput.toLowerCase();
            switch (yesOrNo) {
                case "yes":
                    play = true;
                    isValid = false;
                    break;
                case "no":
                    consoleio.print("Okay, bye.");
                    play = false;
                    isValid = false;
                    break;
                default:
                    consoleio.print("C'mon. Don't you know how to type? \n");
                  
            }
            
        }
        return play;
        
    }
}
