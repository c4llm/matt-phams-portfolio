/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gamebot.blackjack;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIo {
   
       private Scanner sc = new Scanner(System.in);
    
    
    public void print(String message) {
        System.out.println(message);
    }
    
      public void printSameLine(String message){
        System.out.print(message);
    }
    
    
    public String getUserInputString(String prompt) {
        
        String userInput="";
        
        while(!userInput.equals("hit") && !userInput.equals("stay")){
        printSameLine(prompt);
        userInput=sc.nextLine();
        userInput=userInput.toLowerCase();
        }
        return userInput;
        
        
        
    }
    
    public String getUserInputStringPlayAgain(String prompt) {
    
    String userInput="";
        
        while(!userInput.equals("yes") && !userInput.equals("no")){
        printSameLine(prompt);
        userInput=sc.nextLine();
        userInput=userInput.toLowerCase();
        }
        return userInput;
        
    
    }
    
    
    
}
