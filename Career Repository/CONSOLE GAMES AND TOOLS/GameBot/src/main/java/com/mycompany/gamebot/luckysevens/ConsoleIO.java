/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gamebot.luckysevens;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIO {
     
      private Scanner sc = new Scanner(System.in);
    
    
    public void print(String message) {
        System.out.println(message);
    }
    
    
    public String getUserInputString(String prompt) {
        
        print(prompt);
        
        return sc.nextLine();
        
        
    }
    
    public float getUserInputFloat(String prompt) {
        
        System.out.print(prompt);
        
        boolean isValid = false;
        float input = 0;
        
        
        while (!isValid) {
            
                String userInput = sc.nextLine();

                input = Float.parseFloat(userInput);    
                
                if (input > 0) {
                    isValid = true;
                }
            

        }
        
        return input;
    }
    
    
    
}
