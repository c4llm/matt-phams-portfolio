/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gamebot.interest;

import MainApp.Game;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculator implements Game {

    float interestRate = 0;
    float initialAmount = 0;
    float numberOfYears = 0;
    float principleBegin = 0;
    float princlpleEnd = 0;
    float interestEarned = 0;
    float princeBegin = 0;
    float princeCurrent = 0;
    float princeEnd = 0;
    boolean askCompound = true;
    int compoundRate = 0;

    private ConsoleIo consoleio;
    private PlayAgain playAgain = new PlayAgain();
    
    public String getName(){
        return "Interest Calculator";
    }


    public void play() {

        consoleio = new ConsoleIo();
        boolean play = true;

        while (play) {

            interestRate = consoleio.getUserInputInt("What is your interest rate (%)?");
            initialAmount = consoleio.getUserInputInt("How much would you like to invest? ");
            numberOfYears = consoleio.getUserInputInt("How many years would you like to invest? ");

            while (askCompound) {
                String userInput="";
               
                userInput = consoleio.getUserInputString("How would you like to compound your interest? (quarterly = q , monthly = m , daily = d): ");
              
                switch (userInput) {
                    case "quarterly":
                        compoundRate = 4;
                        askCompound = false;
                        break;
                    case "q":
                        compoundRate = 4;
                        askCompound = false;
                        break;
                    case "monthly":
                        compoundRate = 12;
                        askCompound = false;
                        break;
                    case "m":
                        compoundRate = 12;
                        askCompound = false;
                        break;
                    case "daily":
                        compoundRate = 365;
                        askCompound = false;
                        break;
                    case "d":
                        compoundRate = 365;
                        askCompound = false;
                        break;
                    default:
                        System.out.println("Please provide valid answer (quarterly = q , monthly = m , daily = d)");
                        askCompound = true;
                }
            }
            interestRate /= compoundRate;
            interestRate /= 100;

            for (int i = 1; i <= numberOfYears; i++) {
                princeBegin = initialAmount;
                for (int p = 0; p < compoundRate; p++) {
                    initialAmount = initialAmount * (1 + interestRate);
                }
                princeEnd = initialAmount;
                interestEarned = princeEnd - princeBegin;
                consoleio.print("In year " + i + " you made $" + princeBegin + " at the beginning of the year, "
                        + princeEnd + " at the end of the year, and earned $" + interestEarned + " in interest!");
            }
            play = playAgain.doesUserWantToPlayAgain();
        }
    }

}
