/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tenintegers;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class tenIntegers {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int[] integers;
        integers = new int[10];
        int sum = 0;
        double average = 0;
        int current = 0;
        int max = 0;
        boolean play = true;

        while (play) {
            System.out.println("Input 10 numbers (one at a time).");

            for (int i = 0; i < 10; i++) {
                System.out.print("Please input your integer for index [" + i + "]: ");
                integers[i] = scan.nextInt();
            }
            int min = integers[0];
            for (int j = 0; j < 10; j++) {
                sum += integers[j];
            }
            average = (double) sum / 10;
            for (int i = 0; i < 10; i++) {
                current = integers[i];

                if (current > max) {
                    max = current;
                } else if (current < min) {
                    min = current;
                }

            }
            System.out.println("Your integer's min is: " + min);
            System.out.println("Your integer's max is: " + max);
            System.out.println("Your integer's sum is: " + sum);
            System.out.println("Your integer's average is: " + average);
            play = playAgain();
        }
    }

    public static String cleanInput(String input) {
        input = input.toLowerCase();
        return input;
    }

    public static boolean playAgain() {
        Scanner scan = new Scanner(System.in);
        String userInput = "";
        boolean truth = true;
        while (truth) {
            System.out.println("Would you like to play again (y/n)?");
            userInput = scan.nextLine();
            userInput = cleanInput(userInput);
            if (userInput.equals("y") || userInput.equals("n")) {
                truth = false;
            } 
        }
        if (userInput.equals("y")) {
            return true;
        }
         System.out.println("GAME OVER, thanks for playing!");
         return false;
       
    }
}
