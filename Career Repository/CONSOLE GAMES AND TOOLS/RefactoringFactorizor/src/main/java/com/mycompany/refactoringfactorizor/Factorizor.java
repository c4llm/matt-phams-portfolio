/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.refactoringfactorizor;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Factorizor {

    double userDouble = 0;
    int numberOfFactors = 0;
    int perfectNumber = 0;
    int primeNumber = 0;
    int userInt = 0;

    private ConsoleIO consoleio;
    private PlayAgain playAgain = new PlayAgain();

    public void play() {

        consoleio = new ConsoleIO();
        boolean play = true;

        while (play) {

            int userInt = consoleio.getUserInputInt("What number do you want to be factored?");
            userDouble = userInt;

            if (userInt <= 0) {
                userInt = consoleio.getUserInputInt("What number do you want to be factored? ");
                userDouble = userInt;
            }

            for (int i = 1; i < userInt; i++) {
                if ((userDouble / i) == (userInt / i)) {
                    consoleio.print(i + " is a factor of " + userInt);
                    numberOfFactors = numberOfFactors + 1;
                    perfectNumber += i;
                    primeNumber++;
                }
            }
            consoleio.print("You have " + numberOfFactors + " factors.");
            if (perfectNumber == userInt) {
                consoleio.print(userInt + " is a perfect number.");
            } else {
                consoleio.print(userInt + " is not a perfect number.");
            }
            if (primeNumber > 2) {
                consoleio.print(userInt + " is not a prime number.");
            } else {
                consoleio.print(userInt + " is a prime number.");
            }
            play = playAgain.doesUserWantToPlayAgain();
        }
    }

}
