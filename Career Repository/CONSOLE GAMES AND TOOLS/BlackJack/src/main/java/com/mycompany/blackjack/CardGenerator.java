/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack;

/**
 *
 * @author apprentice
 */
public class CardGenerator {
    
    private int minValue;
    private int maxValue;
    
    
    public CardGenerator(int min, int max) {
        this.minValue = min;
        this.maxValue = max;
    }
    
    
    public int generate() {

       
        int card = (int)((Math.random() * maxValue)) + minValue;


        return card;
    }
}
