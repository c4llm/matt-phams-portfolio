/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack;

/**
 *
 * @author apprentice
 */
public class BlackJack {

    private CardGenerator card = new CardGenerator(2, 11);
    private ConsoleIo consoleio = new ConsoleIo();
    private PlayAgain playAgain = new PlayAgain();
    private int card1 = 0;
    private int card2 = 0;
    private int cardDraw = 0;
    private int cardTotal = 0;
    private int compCard1 = 0;
    private int compCard2 = 0;
    private int compCardDraw = 0;
    private int compCardTotal = 0;
    private boolean hitOrStay = true;
    private String answer = "";

    public void play() {
        boolean play = true;

        consoleio.print("Welcome to BLACKJACK!!!");
        while (play) {
            card1 = card.generate();
            card2 = card.generate();
            cardTotal = card1 + card2;
            consoleio.print("You get a " + card1 + " and a " + card2);
            consoleio.print("Your total is " + cardTotal + "\n");

            while (cardTotal < 21) {
                compCard1 = card.generate();
                compCard2 = card.generate();
                consoleio.print("The dealer has a " + compCard1 + " showing, and a hidden card.");
                consoleio.print("His total is hidden, too.\n");

                while (hitOrStay && cardTotal < 21) {
                    answer = consoleio.getUserInputString("Would you like to \"hit\" or \"stay\"?  ");
                    if (answer.equals("stay")) {
                        hitOrStay = false;
                    } else if (cardTotal < 21) {
                        cardDraw = card.generate();
                        consoleio.print("You drew a " + cardDraw);
                        cardTotal = cardDraw + cardTotal;
                        consoleio.print("Your total is " + cardTotal + "\n");
                    }
                }

                if (cardTotal >= 21) {
                    break;
                }

                consoleio.print("\nOkay, dealer's turn.");
                consoleio.print("His hidden card was a " + compCard2);
                compCardTotal = compCard1 + compCard2;
                consoleio.print("His total was " + compCardTotal + "\n");

                while (compCardTotal <= 16) {
                    consoleio.print("Dealer chooses to hit.");
                    compCardDraw = card.generate();
                    compCardTotal = compCardDraw + compCardTotal;
                    consoleio.print("He draws a " + compCardDraw);
                    consoleio.print("His total is " + compCardTotal + "\n");
                }

                consoleio.print("Dealer stays. \n");
                consoleio.print("Dealer total is " + compCardTotal);
                if (compCardTotal > 21) {
                    consoleio.print("The dealer went over 21");

                    consoleio.print("YOU WIN");
                } else {

                    while (compCardTotal < 21) {
                        consoleio.print("Your total is " + cardTotal + "\n");
                        if (compCardTotal > cardTotal) {
                            consoleio.print("YOU LOSE!");
                        } else {
                            consoleio.print("YOU WIN!");
                        }

                    }
                    if (compCardTotal == 21) {
                        consoleio.print("YOU LOSE, the dealer got exactly 21!");
                    }
                }

                play = playAgain.doesUserWantToPlayAgain();
            }
            if (cardTotal == 21 && compCardTotal != 21) {
                consoleio.print("YOU WIN!");
            } else if (compCardTotal != 21) {
                consoleio.print("YOU LOSE!");

            }
            play = playAgain.doesUserWantToPlayAgain();
        }

    }
}
