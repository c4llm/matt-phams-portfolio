
import com.mycompany.simplecalculator.SimpleMath;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class SimpleCalculatorTest {
    SimpleMath target = new SimpleMath();
    public SimpleCalculatorTest(){}
    
    @Before
    public void setUp(){;}
    
    @After
    public void tearDown(){}
    
    @Test
    public void testAddTwoNumbers(){
    double expecteds = 5;
    double result = target.add(3,2);
    Assert.assertEquals(expecteds, result,0);
    }
    
    @Test
    public void testSubtracTwoNumbers(){
    double expecteds = 7;
    double result = target.subtract(10, 3);
    Assert.assertEquals(expecteds, result,0);
    }
    
    @Test
    public void testMultiplyTwoNumbers(){
    double expecteds = 15;
    double result = target.multiply(5, 3);
    Assert.assertEquals(expecteds, result,0);
    }
    
    @Test
    public void testDivideTwoNumbers(){
    double expecteds = 10;
    double result = target.divide(100, 10);
    Assert.assertEquals(expecteds, result, 0);
    
    }
    
    
}
