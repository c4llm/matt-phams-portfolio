/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.simplecalculator;

/**
 *
 * @author apprentice
 */
public class SimpleMath {
    
    public SimpleMath(){
    }
    
    public double add(double num1, double num2){
    double answer;
    answer = num1 + num2;
    return answer;
    }
    
     public double subtract(double num1, double num2){
    double answer;
    answer = num1 - num2;
    return answer;
    }
     
      public double divide(double num1, double num2){
    double answer;
    answer = num1 / num2;
    return answer;
    }
      
      public double multiply(double num1, double num2){
    double answer;
    answer = num1 * num2;
    return answer;
    }
    
}
