/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.simplecalculator;

/**
 *
 * @author apprentice
 */
public class PlayAgain {
    
     ConsoleIo consoleio = new ConsoleIo();
    
    public boolean doesUserWantToPlayAgain() {
        
        boolean isValid = true;
        boolean play = false;
        
        String userInput;
        
        while (isValid) {
            
            userInput = consoleio.getUserPlayAgain("Do you want to play again (yes/no)?: ");
            
            switch (userInput) {
                case "yes":
                    play = true;
                    isValid = false;
                    break;
                case "no":
                    consoleio.print("Goodbye.");
                    play = false;
                    isValid = false;
                    break;
                default:
                    consoleio.print("Type yes or no!!!");
                  
            }
            
        }
        return play;
        
    }
}
