/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.simplecalculator;

/**
 *
 * @author apprentice
 */
public class SimpleCalculator {

    private ConsoleIo consoleio = new ConsoleIo();
    private PlayAgain playAgain = new PlayAgain();
    private SimpleMath math = new SimpleMath();
    private String mathOperator = "";
    private double number1 = 0d;
    private double number2 = 0d;
    private double answer = 0d;
    boolean playIt = true;

    public void play() {

        consoleio.print("Welcome Simple Calculator");

        while (playIt) {

            mathOperator = consoleio.getUserInputString("Which math operation would you like to perform ( add, subtract, multiply, divide, or exit (to exit the calculator)? ");
            if (mathOperator.equals("exit")) {
                playIt = false;
            }
            number1 = consoleio.getUserInputDouble("Please choose your first operand: ");
            number2 = consoleio.getUserInputDouble("Please choose your second operand: ");

            switch (mathOperator) {
                case "add":
                    answer = math.add(number1, number2);
                    break;
                case "substract":
                    answer = math.subtract(number1, number2);
                    break;
                case "divide":
                    answer = math.divide(number1, number2);
                    break;
                case "multiply":
                    answer = math.multiply(number1, number2);
                    break;
            }

            consoleio.print(number1 + " " + mathOperator + " " + number2 + " = " + answer);
            playIt = playAgain.doesUserWantToPlayAgain();

        }

    }
}
