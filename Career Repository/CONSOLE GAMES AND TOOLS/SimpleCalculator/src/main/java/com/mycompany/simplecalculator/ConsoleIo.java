/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.simplecalculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIo {

    private Scanner sc = new Scanner(System.in);

    public void print(String message) {
        System.out.println(message);
    }

    public void printSameLine(String message) {
        System.out.print(message);
    }

    public String getUserInputString(String prompt) {
        String userInput = "";

        while (!userInput.equals("add") && !userInput.equals("subtract") && !userInput.equals("divide") && !userInput.equals("multiply") && !userInput.equals("exit")) {
            printSameLine(prompt);
            userInput = sc.next();
            userInput = cleanInput(userInput);
        }

        return userInput;

    }

    public String getUserPlayAgain(String prompt) {
        String userInput = "";
        printSameLine(prompt);
        userInput = sc.next();
        userInput = cleanInput(userInput);
        return userInput;
    }

    public double getUserInputDouble(String prompt) {

        System.out.print(prompt);

        boolean isValid = false;
        double input = 0d;
        String userInput = "";

        while (!isValid) {

            userInput = sc.next();
            input = Double.parseDouble(userInput);
            if (input > 0) {
                isValid = true;
            }

        }

        return input;
    }

    public static String cleanInput(String input) {
        input = input.toLowerCase();
        return input;
    }

    public int getInteger(String prompt) {
        print(prompt);
        int integer = sc.nextInt();
        return integer;
    }

    public int getNumberBetweenMaxAndMin(String prompt, int max, int min) {
        print(prompt);
        int integer = sc.nextInt();

        while (integer < min || integer > max) {
            print("Please input a number between " + min + " & " + max);
            integer = sc.nextInt();
        }
        return integer;
    }

    public float getFloat(String prompt) {
        print(prompt);
        float numberFloat = sc.nextFloat();
        return numberFloat;

    }

    public float getFloatBetweenMaxAndMin(String prompt, float max, float min) {
        print(prompt);
        float numberFloat = sc.nextFloat();

        while (numberFloat < min || numberFloat > max) {
            print("Please input a number between " + min + " & " + max);
            numberFloat = sc.nextFloat();
        }
        return numberFloat;
    }

    public double getDoubleBetweenMaxAndMin(String prompt, double max, double min) {
        print(prompt);
        double numberDouble = sc.nextDouble();

        while (numberDouble < min || numberDouble > max) {
            print("Please input a number between " + min + " & " + max);
            numberDouble = sc.nextDouble();
        }
        return numberDouble;
    }

}
