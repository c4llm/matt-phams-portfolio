/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.refactorintoobjectsluckysevens;

/**
 *
 * @author apprentice
 */
public class Dice {
    
       
    private int minValue;
    private int maxValue;
    
    
    public Dice(int min, int max) {
        this.minValue = min;
        this.maxValue = max;
    }
    
    
    public int roll() {

        //Dice roll is two random values between 1 and 6
        int die = (int) ((Math.random() * maxValue)) + minValue;


        return die;
    }
}
