/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.refactorintoobjectsluckysevens;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    private Dice dice;
    private ConsoleIO consoleio;
    private PlayAgain playAgain = new PlayAgain();


    int diceRoll = 0;
    int rollCount = 0;
    float maxMoney = 0;
    int rollAtMaxMoney = 0;
    float userFloat = 0f;

    public void play() {

        dice = new Dice(2, 12);
        consoleio = new ConsoleIO();
        boolean play = true;

        consoleio.print("Welcome to Lucky Sevens");

        while (play) {

            if (userFloat <= 0f) {
                userFloat = consoleio.getUserInputFloat("Please insert bet amount: ");
                maxMoney = userFloat;
            }

            while (userFloat > 0f) {
                diceRoll = dice.roll();
                if (diceRoll == 7) {
                    userFloat = userFloat + 4f;
                    rollCount += 1;
                    if (userFloat > maxMoney) {
                        maxMoney = userFloat;
                        rollAtMaxMoney = rollCount;
                    }
                } else {
                    userFloat -= 1f;
                    rollCount += 1;
                }

            }

            consoleio.print("You are out of money! GAME OVER");
            consoleio.print("You have rolled " + rollCount + " times.");
            consoleio.print("Your maximum amount of cash was $" + maxMoney);
            consoleio.print("Your number of rolls when you had the most cash was " + rollAtMaxMoney);
            play = playAgain.doesUserWantToPlayAgain();
        }
        
    }

}
