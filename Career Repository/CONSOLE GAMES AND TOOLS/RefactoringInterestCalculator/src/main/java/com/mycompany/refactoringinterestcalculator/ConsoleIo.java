/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.refactoringinterestcalculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIo {
    
    
    
      private Scanner sc = new Scanner(System.in);
    
    
    public void print(String message) {
        System.out.println(message);
    }
    
    
    public String getUserInputString(String prompt) {
        String userInput="";
        print(prompt);
        userInput=sc.nextLine();
        userInput=userInput.toLowerCase();
        return userInput;
        
        
    }
    
    public int getUserInputInt(String prompt) {
        
        System.out.print(prompt);
        
        boolean isValid = false;
        int input = 0;
        
        
        while (!isValid) {
            
                String userInput = sc.nextLine();
                input = Integer.parseInt(userInput);    
                if (input > 0) {
                    isValid = true;
                }
            

        }
        
        return input;
    }
    
    
    
}
