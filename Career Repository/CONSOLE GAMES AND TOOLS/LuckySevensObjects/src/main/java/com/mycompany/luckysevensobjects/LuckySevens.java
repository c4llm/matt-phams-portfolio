/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.luckysevensobjects;

import java.util.Scanner;

/**
 *
 * @author Ollie
 */
public class LuckySevens {
    
    private Dice dice;
    private ConsoleIO consoleio;
    private PlayDecider playDecider = new PlayDecider();
    
    private float highestBank = 0f;
    private int highestRoll = 1;
    private float bankRoll = 0.0f;
    private int rollNumber = 0; //How many rolls have been thrown

    
    public void play() {

        dice = new Dice(2, 12);
        consoleio = new ConsoleIO();
        
        //Start loop to play game, true or false
        boolean play = true;

        System.out.println("$$$$7777 Welcome to Lucky Sevens 7777$$$$\n ");

        while (play) {

            //Initialize variables
            float input = 0;
            
            int rollValue = 0; //The value of the dice roll


            //Start while loop to check for a valid input of a number above zero
            boolean isValid = true;

            while (isValid) {

                //Try catch for anything other than a number
                try {
                    
                    input = consoleio.getUserInputFloat("How much money do you want to bet?");
                    
                    
                    if (input <= 0) {
                        consoleio.print("You have to bet more than zero.");
                        isValid = true;
                    } else {
                        bankRoll = input; //Set bankroll to dollar amount user put in
                        isValid = false; //Break from while loop
                    }

               } catch (NumberFormatException nfe) {
                   consoleio.print("Just numbers please.");
                }

            }

            highestBank = bankRoll; //Set highest bank amount to initial bet amount

            //While bank account is greater than zero, throw dice, increment roll number, and add or decrement from the bankroll
            while (bankRoll > 0) {

                rollNumber++;

               rollValue = dice.roll(); //Call dice roll method
                if (rollValue == 7) { //If the dice roll is 7, add to bankroll
                    bankRoll += 4;
                    if (bankRoll > highestBank) { //If current bankroll is higher then highest bank value is set to current bankroll
                        highestBank = bankRoll;
                        highestRoll = rollNumber; //And the highest roll number is collected

                    } else { //If bankroll is not highest, do nothing

                    }

                } else { //If dice roll is not 7, decrement bank roll
                    bankRoll--;

                }

            }

            //Print out to user
            System.out.println("You have lost all  your money. The dice rolled " + rollNumber + " times. You should have quit on roll " + highestRoll + " when your bankroll was " + highestBank);

            play = playDecider.doesUserWantToPlayAgain();
        }
    }





}