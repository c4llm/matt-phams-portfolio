/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.factorizer;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Factorizer {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String userInput = "";
        double userDouble = 0;
        int numberOfFactors = 0;
        int perfectNumber = 0;
        int primeNumber = 0;

        printLine("What number do you want to be factored?");
        userInput = sc.nextLine();
        int userInt = Integer.parseInt(userInput);
        userDouble = userInt;
        if (userInt <= 0) {
            printLine("What number do you want to be factored?");
            userInput = sc.nextLine();
        }

        for (int i = 1; i < userInt; i++) {
            if ((userDouble / i) == (userInt / i)) {
                printLine(i + " is a factor of " + userInput);
                numberOfFactors = numberOfFactors + 1;
                perfectNumber += i;
                primeNumber++;
            }
        }
        printLine("You have " + numberOfFactors + " factors.");
        if (perfectNumber == userInt) {
            printLine(userInt + " is a perfect number.");
        } else {
            printLine(userInt + " is not a perfect number.");
        } 
        if (primeNumber>2){
            printLine(userInt+" is not a prime number.");}
        else {
            printLine(userInt+" is a prime number.");}
    }
    
    public static void printLine (String answer){
        System.out.println(answer);
    }
}
