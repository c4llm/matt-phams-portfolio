/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.rsvp;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */

// name is name of server and urlPattern is path to put in URL to access server
@WebServlet(name = "RSVPServlet", urlPatterns = {"/RSVPServlet"})
public class RSVPServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
//        try {
//            PrintWriter out = response.getWriter();
//            
//            out.println("<html>");
//            out.println("<body>");
//            out.println("<h1>This is my header</h1>");
//            out.println("</body>");
//            out.println("</html>");
//            
//        } catch (IOException ex) {
//            Logger.getLogger(RSVPServlet.class.getName()).log(Level.SEVERE, null, ex);
//        }

        RequestDispatcher rd = request.getRequestDispatcher("entry.jsp");
        rd.forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        String myAnswer = request.getParameter("myAnswer");
        String myReason = request.getParameter("myReason");
        String myNotes = request.getParameter("myNotes");
        
        String messageToUser = "";
        //do this is in this order because if myAnswer is null it won't throw null pointer exception
        if("No".equals(myAnswer)){
            messageToUser= "Sorry you can't make it.";
        } else {
            messageToUser= "Can't wait to see you.";
        }
        
        //set attribute is like put method for Map
        request.setAttribute("message", messageToUser);
        
        RequestDispatcher rd = request.getRequestDispatcher("response.jsp");
        rd.forward(request, response);
        
    }
    
}
