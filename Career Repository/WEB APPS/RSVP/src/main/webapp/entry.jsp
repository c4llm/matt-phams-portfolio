<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

        <title>Starter Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

    </head>

    <body>
        
        <style>
            .party-form {
                margin-top: 60px;
            }
        </style>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://getbootstrap.com/examples/starter-template/#">Project name</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="http://getbootstrap.com/examples/starter-template/#">Home</a></li>;
                        <li><a href="http://getbootstrap.com/examples/starter-template/#about">About</a></li>;
                        <li><a href="http://getbootstrap.com/examples/starter-template/#contact">Contact</a></li>;
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container">

            <div class="party-form">
                <h1>I'm having a party and you're invited<small>Except Bevan</small></h1>
                Can you attend? (not bevan)
                <br />

                <form action='RSVPServlet' method='post'>
                    
                    Yes <input type="radio" name="myAnswer" value="Yes" checked/>
                    No  <input type="radio" name="myAnswer" value="No" />
                    
                    Reason (if not attending): <br />
                    <select name="myReason">
                        <option value="Out of town">I'm out of town</option>
                        <option value="Schedule conflict">Schedule conflict</option>
                        <option value="I\'m bevan">I'm bevan</option>
                    </select>
                    
                    Notes:<br/>
                    <input type='text' name='myNotes' />
                    <br />
                    <input type='submit' value="Submit Form" />
                    
                </form>
            </div>




        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>


    </body>
</html>