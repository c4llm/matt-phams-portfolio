<%-- 
    Document   : response
    Created on : Feb 12, 2016, 11:53:59 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Response</h1>
        Your answer was ${param.myAnswer}:
        ${message}
    </body>
</html>
