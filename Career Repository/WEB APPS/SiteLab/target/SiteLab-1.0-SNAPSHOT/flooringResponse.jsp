<%-- 
    Document   : flooringResponse
    Created on : Feb 13, 2016, 8:08:33 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Calculator Results</title>
    </head>
    <body>
        <style>
            body{
                text-align: center;
                background-image: url("http://www.taahil.com/wp-content/uploads/2016/01/wood-flooring-wood-floors.jpeg");
            }
        </style>

        <h1>Flooring Calculator Results</h1>

        <h2>Your area is ${area}.</br>
            Your total cost is $${totalCost}. We charge $86 per hour of labor.
        </h2>
    </body>
</html>
