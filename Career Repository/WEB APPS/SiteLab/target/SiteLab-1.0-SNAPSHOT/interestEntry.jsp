
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

        <title>Starter Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

    </head>

    <body>

        <style>
            .party-form {
                margin-top: 60px;
            }
            body{
                background-color: #66CCCC;
                text-align: center;
            }
            img { 
                border:3px solid #021a40;
            }
        </style>



        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Interest Calculator!</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="FactorizerServlet">Factorizer</a></li>
                        <li><a href="LuckySevensServlet">Lucky Sevens</a></li>
                        <li class="active"><a href="InterestServlet">Interest Calculator</a></li>
                        <li><a href="FlooringCalculator">Flooring Calculator</a></li>
                        <li><a href='RPSServlet'>Rock Paper Scissors</a></li>
      
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>



        <div class="container">

            <div class="party-form">

                <h1>WELCOME TO INTEREST CALCUALTOR!</h1>


                <br />

                <form action='InterestServlet' method='post'>
                    <h3>
                        Interest Rate(%): <input type='text' name='interestRate' />
                        </br>
                        Initial Amount: <input type="text" name="initialAmount" />
                        </br>
                        Number of Years: <input type="text" name="numberOfYears" />
                        </br>
                        Compound Rate (q = quarterly, m = monthly, d = daily): <input type="text" name="compoundRate" />
                    </h3>
                    <br />

                    <input type="submit" value="Submit">

                    <div>
                        <img src="http://www.thecalculatorsite.com/images/compound-interest-coins.jpg">
                    </div>

                </form>








            </div>



        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>


    </body>
</html>
