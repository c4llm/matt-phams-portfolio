
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

        <title>Starter Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

    </head>

    <body>

        <style>
            .party-form {
                margin-top: 60px;
            }
            body {
                background-color: #228b22;
                text-align: center;
            }
        </style>
        
        
        
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Factorizer!</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="FactorizerServlet">Factorizer</a></li>
                        <li><a href="LuckySevensServlet">Lucky Sevens</a></li>
                        <li><a href="InterestServlet">Interest Calculator</a></li>
                        <li><a href="FlooringCalculator">Flooring Calculator</a></li>
                        <li><a href='RPSServlet'>Rock Paper Scissors</a></li>
      
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
        
        

        <div class="container">
            
            <div class="party-form">
                
                <h1>WELCOME TO FACTORIZER!</h1>
                
                
                <br />
                
                <form action='FactorizerServlet' method='post'>

                    <h3>Number to factorize: </h3>
                    <input type='text' name='factorizedNumber' />
                    
                    <br />
                    <div>
                        <img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSvrgkHwn22i06g9LoVwVYO4Bbs72CZKlCFqMxgKtdLVMRpALRW">
                    </div>
                    
                    
                    
                </form>
                
                
                
                
                
                
                
                
            </div>

            

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>


    </body>
    </html>