<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

        <title>Starter Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

    </head>

    <body>

        <style>
            .party-form {
                margin-top: 60px;
            }
            body {
                background-image: url("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQxXsWlYh1e_ANxdBvvwtiYR2f1qPEkUn76R088nHhI43F4ZMEH");
                text-align: center;
                color: white;
            }
            h1{
                margin-top: 100px;
            }
        </style>
        
        
       
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Rock Paper Scissors!</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="FactorizerServlet">Factorizer</a></li>
                        <li><a href="LuckySevensServlet">Lucky Sevens</a></li>
                        <li><a href="InterestServlet">Interest Calculator</a></li>
                        <li><a href="FlooringCalculator">Flooring Calculator</a></li>
                        <li class="active"><a href='RPSServlet'>Rock Paper Scissors</a></li>
      
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
        
        

        <div class="container">
            
            <div class="party-form">
                
                <h1>WELCOME TO</h1>
                <div>
                    <img src="http://www.eagle1065.com/image/kegxr/UserFiles/Image/RPS%20Logo.jpg" width="500" height="350">
                </div>
                
                
                <br />
                
                <form action='RPSServlet' method='post'>

                    <h3>Choose rock, paper or scissors:</h3> <input type="text" name="userChoice" />
                    
                    
                    
                </form>
                
                
                
                
                
                
                
                
            </div>

            

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>


    </body>
    </html>
      