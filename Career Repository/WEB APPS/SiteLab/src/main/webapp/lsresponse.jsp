<%-- 
    Document   : lsresponse
    Created on : Feb 12, 2016, 2:52:45 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Seven's Results</title>
    </head>
    <body>
        <style>
            body{
                background-color: #c0c0c0;
            }
            
        </style>
        <div align='center' vertical-align='center'>
            <div>
                <img src="http://skylinemediainc.com/wp-content/uploads/2015/09/Lucky7s-02.21-1024x449.png" width="400" height="200">
            </div>
            <h1>
                HOT DIGGITY DOO-DAH
                </br></br>
                You rolled ${rollNumber} times and your highest roll for your highest amount of money in hand was ${highestRoll} and highest bank was ${highestBank}
            </h1>
        </div>
    </body>
</html>
