/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.sitelab;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "InterestServlet", urlPatterns = {"/InterestServlet"})
public class InterestCalculator extends HttpServlet {

    String userInput = "";
    String userLowerCase = "";
    float interestRate = 0;
    float initialAmount = 0;
    float numberOfYears = 0;
    float principleBegin = 0;
    float princlpleEnd = 0;
    float interestEarned = 0;
    float princeBegin = 0;
    float princeCurrent = 0;
    float princeEnd = 0;
    boolean askCompound = true;
    int compoundRate = 0;

//        interestRate = takeUserInputNumber("What is your interest rate (%)?");
//        initialAmount = takeUserInputNumber("How much would you like to invest? ");
//        numberOfYears = takeUserInputNumber("How many years would you like to invest? ");
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("interestEntry.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        userInput = (request.getParameter("compoundRate"));
        interestRate = Float.parseFloat(request.getParameter("interestRate"));
        numberOfYears = Float.parseFloat(request.getParameter("numberOfYears"));
        initialAmount = Float.parseFloat(request.getParameter("initialAmount"));

        userLowerCase = userInput.toLowerCase();
        switch (userLowerCase) {
            case "quarterly":
                compoundRate = 4;
                askCompound = false;
                break;
            case "q":
                compoundRate = 4;
                askCompound = false;
                break;
            case "monthly":
                compoundRate = 12;
                askCompound = false;
                break;
            case "m":
                compoundRate = 12;
                askCompound = false;
                break;
            case "daily":
                compoundRate = 365;
                askCompound = false;
                break;
            case "d":
                compoundRate = 365;
                askCompound = false;
                break;

        }
        interestRate /= compoundRate;
        interestRate /= 100;

        List<String> printList = new ArrayList<>();
        for (int i = 1; i <= numberOfYears; i++) {
            princeBegin = initialAmount;
            for (int p = 0; p < compoundRate; p++) {
                initialAmount = initialAmount * (1 + interestRate);
            }
            princeEnd = initialAmount;
            interestEarned = princeEnd - princeBegin;
            String print = ("In year " + i + " you made $" + princeBegin + " at the beginning of the year, "
                    + princeEnd + " at the end of the year, and earned $" + interestEarned + " in interest!");
            printList.add(print);
        }

//        request.setAttribute("numberOfYears", numberOfYears);
//        request.setAttribute("initialAmount", initialAmount);
//        request.setAttribute("interestRate", interestRate);
//        request.setAttribute("compoundRate", userLowerCase);
//        request.setAttribute("princeBegin", princeBegin);
//        request.setAttribute("princeEnd", princeEnd);
//        request.setAttribute("interestEarned", interestEarned);
        request.setAttribute("printList", printList);

        RequestDispatcher rd = request.getRequestDispatcher("interestResponse.jsp");
        rd.forward(request, response);

    }

//    public static float takeUserInputNumber(String question) {
//        Scanner sc = new Scanner(System.in);
//        String userInput = "";
//        float userFloat = 0;
//
//        while (userFloat <= 0) {
//            System.out.print(question);
//            userInput = sc.nextLine();
//            userFloat = Float.parseFloat(userInput);
//        }
//        return userFloat;
//    }
}
