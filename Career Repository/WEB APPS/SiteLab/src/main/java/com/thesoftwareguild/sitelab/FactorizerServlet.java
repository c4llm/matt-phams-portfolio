/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.sitelab;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */

@WebServlet(name = "FactorizerServlet", urlPatterns = { "/FactorizerServlet" })
public class FactorizerServlet extends HttpServlet {

    double userDouble = 0;
    int numberOfFactors = 0;
    int perfectNumber = 0;
    int primeNumber = 0;
    String factors = "";
    String isPrime = "";
    String isPerfect = "";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("factorizerEntry.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int userInt = Integer.parseInt(request.getParameter("factorizedNumber"));
        userDouble = userInt;

        for (int i = 1; i < userInt; i++) {
            if ((userDouble / i) == (userInt / i)) {
                factors += i + ", ";
                numberOfFactors = numberOfFactors + 1;
                perfectNumber += i;
                primeNumber++;
            }
        }
        
        if (perfectNumber == userInt) {
            isPerfect = " is a perfect number";
        } else {
            isPerfect = " is not a perfect number";
        }
        if (primeNumber > 2) {
            isPrime = " is not a prime number";
        } else {
            isPrime = " is a prime number";
        }
        
        request.setAttribute("factorized", userInt);
        request.setAttribute("factorNum", numberOfFactors);
        request.setAttribute("factors", factors);
        request.setAttribute("isPrime", isPrime);
        request.setAttribute("isPerfect", isPerfect);
        
        RequestDispatcher rd = request.getRequestDispatcher("factorizerresponse.jsp");
        rd.forward(request, response);
    }

}
