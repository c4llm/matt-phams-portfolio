/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.sitelab;

import java.io.IOException;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */

@WebServlet(name = "LuckySevensServlet", urlPatterns = { "/LuckySevensServlet" })
public class LuckySevensServlet extends HttpServlet {

    int diceRoll = 0;
    int rollCount = 0;
    float maxMoney = 0;
    int rollAtMaxMoney = 0;
    float userFloat = 0f;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("luckySevensEntry.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        Scanner Input = new Scanner(System.in);
        Random dice = new Random();
//        String userInput = "";

        if (userFloat <= 0f) {
//            System.out.print("Please insert bet amount:");
//            userInput = Input.nextLine();
            userFloat = Float.parseFloat(request.getParameter("moneyToBet"));//= Float.parseFloat(userInput);
            maxMoney = userFloat;
        }

        while (userFloat > 0f) {
            diceRoll = dice.nextInt(6) + 1;
            int diceRoll2 = dice.nextInt(6) + 1;
            if (diceRoll+diceRoll2 == 7) {
                userFloat = userFloat + 4f;
                rollCount += 1;
                if (userFloat > maxMoney) {
                    maxMoney = userFloat;
                    rollAtMaxMoney = rollCount;
                }
            } else {
                userFloat -= 1f;
                rollCount += 1;
            }

        }

//        easyPrint("You are out of money! GAME OVER");
//        easyPrint("You have rolled " + rollCount + " times.");
//        easyPrint("Your maximum amount of cash was $" + maxMoney);
//        easyPrint("Your number of rolls when you had the most cash was " + rollAtMaxMoney);
   
    request.setAttribute("rollNumber", rollCount);
    request.setAttribute("highestRoll", rollAtMaxMoney);
    request.setAttribute("highestBank", maxMoney);
    
    
        RequestDispatcher rd = request.getRequestDispatcher("lsresponse.jsp");
        rd.forward(request, response);

    }
    
}
