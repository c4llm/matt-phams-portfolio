/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.sitelab;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FlooringCalculator", urlPatterns = {"/FlooringCalculator"})
public class FlooringCalculator extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("flooringEntry.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        float length = Float.parseFloat(request.getParameter("length"));
        float width = Float.parseFloat(request.getParameter("width"));
        float area = length * width;
        float cost = Float.parseFloat(request.getParameter("cost"));
        float totalCost = 0f;
        float laborCost = 86;
        int temp = (int)area / 20;

        float preLabor = area * cost;
        totalCost = temp * laborCost;
        totalCost += ((Math.round(((area % 20) / 20) * 4f) / 4f) * laborCost);
        totalCost += preLabor;

        request.setAttribute("totalCost", totalCost);
        request.setAttribute("area", area);

        RequestDispatcher rd = request.getRequestDispatcher("flooringResponse.jsp");
        rd.forward(request, response);

    }

}
