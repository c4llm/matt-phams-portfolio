/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.sitelab;

import com.mycompany.blackjack.CardGenerator;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "Blackjack", urlPatterns = {"/Blackjack"})
public class BlackJack extends HttpServlet {

    private CardGenerator card = new CardGenerator(2, 11);

    private int card1 = 0;
    private int card2 = 0;
    private int cardDraw = 0;
    private int cardTotal = 0;
    private int compCard1 = 0;
    private int compCard2 = 0;
    private int compCardDraw = 0;
    private int compCardTotal = 0;
    private boolean hitOrStay = true;
    private String answer = "";
    private String print="";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("blackjackEntry.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        card1 = card.generate();
        card2 = card.generate();
        cardTotal = card1 + card2;

//            
//            consoleio.print("You get a " + card1 + " and a " + card2);
//            consoleio.print("Your total is " + cardTotal + "\n");
        while (cardTotal < 21) {
            compCard1 = card.generate();
            compCard2 = card.generate();
            print = "";
            print += ("The dealer has a " + compCard1 + " showing, and a hidden card.");
            print += ("His total is hidden, too.\n");

            while (hitOrStay && cardTotal < 21) {
                answer = request.getParameter("hitStay");
                
                if (("stay").equals(answer)){
//                if (answer.equals("stay")) {
                    hitOrStay = false;
                } else if (cardTotal < 21) {
                    cardDraw = card.generate();
                    print=("You drew a " + cardDraw);
                    cardTotal = cardDraw + cardTotal;
                    print = "";
                    print +=("Your total is " + cardTotal + "\n");
                }
            }

            if (cardTotal >= 21) {
                break;
            }
            
            print = "";
            print += ("\nOkay, dealer's turn.");
            print +=("His hidden card was a " + compCard2);
            compCardTotal = compCard1 + compCard2;
            print +=("His total was " + compCardTotal + "\n");

            while (compCardTotal <= 16) {
                print +=("Dealer chooses to hit.");
                compCardDraw = card.generate();
                compCardTotal = compCardDraw + compCardTotal;
                print +=("He draws a " + compCardDraw);
                print +=("His total is " + compCardTotal + "\n");
            }
            print = "";
            print +=("Dealer stays. \n");
            print +=("Dealer total is " + compCardTotal);
            if (compCardTotal > 21) {
                print="";
                print+=("The dealer went over 21");

                print+=("YOU WIN");
            } else {

                while (compCardTotal < 21) {
                    print = "";
                    print+=("Your total is " + cardTotal + "\n");
                    if (compCardTotal > cardTotal) {
                        print+=("YOU LOSE!");
                    } else {
                        print+=("YOU WIN!");
                    }

                }
                if (compCardTotal == 21) {
                    print="";
                    print+=("YOU LOSE, the dealer got exactly 21!");
                }
            }

//                play = playAgain.doesUserWantToPlayAgain();
        }
        if (cardTotal == 21 && compCardTotal != 21) {
            print="";
            print+=("YOU WIN!");
        } else if (compCardTotal != 21) {
            print+=("YOU LOSE!");

        }
//            play = playAgain.doesUserWantToPlayAgain();


        
        request.setAttribute("print", print);

        RequestDispatcher rd = request.getRequestDispatcher("blackjackEntry.jsp");
        rd.forward(request, response);
    }
}

