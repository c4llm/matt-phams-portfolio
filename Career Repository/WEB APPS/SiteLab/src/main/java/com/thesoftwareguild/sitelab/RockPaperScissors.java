/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.sitelab;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "RPSServlet", urlPatterns = {"/RPSServlet"})
public class RockPaperScissors extends HttpServlet {

    Random compRand = new Random();
    String userAnswer = "";
//        String wannaPlayAgain = "";
    int ansNumber = 0;
    int compAnswer = 0;
    String outcome = "";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("rpsEntry.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        Scanner scan = new Scanner(System.in);
//        while (askChoice) {
        userAnswer = request.getParameter("userChoice");
//            userAnswer = cleanInput(userAnswer);
        switch (userAnswer) {
            case "rock":
                ansNumber = 1;
//                    askChoice = false;
                break;
            case "paper":
                ansNumber = 2;
//                    askChoice = false;
                break;
            case "scissors":
                ansNumber = 3;
//                    askChoice = false;
                break;
        }

        compAnswer = compRand.nextInt(3) + 1;
        request.setAttribute("outcome", calculate(ansNumber, compAnswer));

//        }
        RequestDispatcher rd = request.getRequestDispatcher("rpsResponse.jsp");
        rd.forward(request, response);

    }

//    public static String userInput(String askQuestion) {
//        Scanner scan = new Scanner(System.in);
//        String userAnswer = "";
//        System.out.print(askQuestion);
//        userAnswer = scan.nextLine();
//        return userAnswer;
//
//    }
//    public static String cleanInput(String input) {
//        input = input.toLowerCase();
//        return input;
//    }
    public String calculate(int myAnswer, int computerAnswer) {
        if (myAnswer == 1 && computerAnswer == 1) {
            outcome = "You both chose rock, and have tied!";
        } else if (myAnswer == 1 && computerAnswer == 2) {
            outcome = "You chose rock and the computer chose paper, YOU HAVE LOST!";
        } else if (myAnswer == 1 && computerAnswer == 3) {
            outcome = "You chose rock and the computer chose scissors, YOU HAVE WON!";
        } else if (myAnswer == 2 && computerAnswer == 1) {
            outcome = "You chose paper and the computer chose rock, YOU HAVE WON!";
        } else if (myAnswer == 2 && computerAnswer == 2) {
            outcome = "You both chose paper, and have tied!";
        } else if (myAnswer == 2 && computerAnswer == 3) {
            outcome = "You chose paper and the computer chose scissors, YOU HAVE LOST!";
        } else if (myAnswer == 3 && computerAnswer == 1) {
            outcome = "You chose scissors and the computer chose rock, YOU HAVE LOST!";
        } else if (myAnswer == 3 && computerAnswer == 2) {
            outcome = "You chose scissors and the computer chose paper, YOU HAVE WON";
        } else if (myAnswer == 3 && computerAnswer == 3) {
            outcome = "You both chose scissors, and have tied!";
        }

        return outcome;
    }

}
