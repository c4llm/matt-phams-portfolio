//package com.thesoftwareguild.sitelab;
//
//
//import com.mycompany.flooringmasteryprojectaop.dao.ConfigDao;
//import com.mycompany.flooringmasteryprojectaop.dao.DaoInterface;
//import com.mycompany.flooringmasteryprojectaop.dao.ProductsDao;
//import com.mycompany.flooringmasteryprojectaop.dao.TaxesDao;
//import com.mycompany.flooringmasteryprojectaop.dto.ConfigDto;
//import com.mycompany.flooringmasteryprojectaop.dto.OrdersDto;
//import com.mycompany.flooringmasteryprojectaop.dto.ProductsDto;
//import com.mycompany.flooringmasteryprojectaop.dto.TaxesDto;
//import java.io.IOException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import java.util.Scanner;
//import java.util.Set;
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import static org.springframework.util.Assert.state;
//import static org.springframework.util.Assert.state;
//
///**
// *
// * @author apprentice
// */
//public class FlooringController extends HttpServlet{
//
//    private DaoInterface ordersdao;
//    private final ProductsDao productsdao = new ProductsDao();
//    private final TaxesDao taxesdao = new TaxesDao();
//    private final ConfigDao configdao = new ConfigDao();
//    private final ProductsDto productsdto = new ProductsDto();
//    private final TaxesDto taxesdto = new TaxesDto();
//    private String FILENAME = "";
//    private ConfigDto config = new ConfigDto();
//    private boolean test = false;
//    
//    public FlooringController(DaoInterface ordersdao) {
//        this.ordersdao = ordersdao;
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//        RequestDispatcher rd = request.getRequestDispatcher("interestEntry.jsp");
//        rd.forward(request, response);
//
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        test = configdao.getConfig();
//
//        boolean runAgain = true;
//
//            displayMenu();
//
//            int menuSelection = Integer.parseInt(request.getParameter("menuSelection"));
//
//            switch (menuSelection) {
//
//                case 1:
//                    displayOrders();
//                    break;
//                case 2:
//                    addOrder(request, response);
//                    break;
//                case 3:
//                    editOrder(request, response);
//                    break;
//                case 4:
//                    removeOrder(request, response);
//                    break;
////                case 5:
////                    runAgain = false;
////                    consoleio.print("Goodbye");
//
//            }
//        
//    }
//
//    private String displayMenu() {
//        String stuff =("\n1. Display Orders\n"
//                + "2. Add an Order\n"
//                + "3. Edit an Order\n"
//                + "4. Remove an Order\n");
//        return stuff;
//    }
//
//    private String displayOrders() {
//        boolean noOrders = true;
//        String allOrders = "";
//
//        String date = getUserDateInput();
//
//        ordersdao.setOrderDate(date);
//
//        List<OrdersDto> orders = ordersdao.list();
//
//        for (OrdersDto theOrders : orders) {
//            allOrders += ("Order Number: " + theOrders.getOrderNumber() + "\nName: " + theOrders.getName() + "\nCustomer State: "
//                    + theOrders.getCustomerState() + "\nTaxRate: " + theOrders.getTaxRate() + "\nProduct Type: "
//                    + theOrders.getProductType() + "\nArea: " + theOrders.getArea() + "\nCost Per Square Foot: "
//                    + theOrders.getMaterialsCostPerSqFoot() + "\nLabor Cost Per Square Foot: " + "\nMaterial Cost: "
//                    + theOrders.getMaterialsCost() + "\nLabor Cost: " + theOrders.getLaborCost() + "\nTax: "
//                    + theOrders.getTotalTax() + "\nTotal Cost: " + theOrders.getGrandTotal() + "\n");
//            noOrders = false;
//        }
//
//        if (noOrders == true) {
////            consoleio.println("Sorry, there are no orders for that date.");
//        }
//        return allOrders;
//        
//
//    }
//
//    private void addOrder(HttpServletRequest request, HttpServletResponse response) {
////        int counter = 0;
////        int counter2 = 0;
////        int counter3 = 0;
////        int counter4 = 0;
//        double materialCostPerSquareFoot = 0;
//        double laborCostPerSquareFoot = 0;
//        double materialCost = 0;
//        double laborCost = 0;
//        double total = 0;
//        double tax = 0;
//        double taxRate = 0;
//        double totalFinal = 0;
//        boolean fileFound = false;
//        OrdersDto order = new OrdersDto();
//
//        String date = getUserDateInput();
//
//        ordersdao.setOrderDate(date);
//        ordersdao.findFileMap();
//
//        if (ordersdao.findFileMap() != null) {
//            fileFound = true;
//        }
//
//        if (fileFound = false) {
//            ordersdao.createOrderFile(FILENAME);
//        }
//
//        String customerName = request.getParameter("customerName");
//
//        List<TaxesDto> stateTaxes = taxesdao.getTaxesList();
//
////        for (TaxesDto entry : stateTaxes) {
////            counter++;
////        }
////        String[] stateArray = new String[counter];
//
////        for (TaxesDto entry : stateTaxes) {
////            stateArray[counter2] = (entry.getState());
////            counter2++;
////        }
//
//        String state = request.getParameter("state");
//
//        for (TaxesDto taxR : stateTaxes) {
//            if (state.equals(taxR.getState())) {
//                taxRate = taxR.getTaxRate();
//            }
//        }
//
//        List<ProductsDto> productType = productsdao.getProductsList();
////        for (ProductsDto products : productType) {
////            products.getProductType();
////            counter3++;
////        }
////
////        String[] productsArray = new String[counter3];
////
////        for (ProductsDto products : productType) {
////            productsArray[counter4] = products.getProductType();
////            counter4++;
////        }
//
//        String prodType = request.getParameter("product");
//
//        double area = Double.parseDouble(request.getParameter("area"));
//
//        for (ProductsDto products : productType) {
//            if (products.getProductType().equals(prodType)) {
//                materialCostPerSquareFoot = products.getMaterialCostPerSqFoot();
//                laborCostPerSquareFoot = products.getLaborCostPerSqFoot();
//            }
//        }
//
//        materialCost = calculateMaterial(area, materialCostPerSquareFoot);
//        laborCost = calculateLabor(area, laborCostPerSquareFoot);
//        total = calculateTotal(materialCost, laborCost);
//        tax = calculateTax(total, taxRate);
//        totalFinal = calculateTrueTotal(total, tax);
//
//        order.setName(customerName);
//        order.setCustomerState(state);
//        order.setTaxRate(taxRate);
//        order.setProductType(prodType);
//        order.setArea(area);
//        order.setMaterialsCostPerSqFoot(materialCostPerSquareFoot);
//        order.setLaborCostPerSqFoot(laborCostPerSquareFoot);
//        order.setMaterialsCost(materialCost);
//        order.setLaborCost(laborCost);
//        order.setTotalTax(tax);
//        order.setGrandTotal(totalFinal);
//
//        ordersdao.create(order);
////
////         List<OrdersDto> orders = ordersdao.list();
//
////        for (OrdersDto theOrders : orders) {
////            String ("Order Number: " + theOrders.getOrderNumber() + "\nName: " + theOrders.getName() + "\nCustomer State: "
////                    + theOrders.getCustomerState() + "\nTaxRate: " + theOrders.getTaxRate() + "\nProduct Type: "
////                    + theOrders.getProductType() + "\nArea: " + theOrders.getArea() + "\nCost Per Square Foot: "
////                    + theOrders.getMaterialsCostPerSqFoot() + "\nLabor Cost Per Square Foot: " + "\nMaterial Cost: "
////                    + theOrders.getMaterialsCost() + "\nLabor Cost: " + theOrders.getLaborCost() + "\nTax: "
////                    + theOrders.getTotalTax() + "\nTotal Cost: " + theOrders.getGrandTotal() + "\n");
////        }
//    }
//
//    public void editOrder(HttpServletRequest request, HttpServletResponse response) {
////        int counter = 0;
////        int counter2 = 0;
////        int counter3 = 0;
////        int counter4 = 0;
////        int counter5 = 0;
////        int counter6 = 0;
//        String fileName = "";
//        OrdersDto orders = null;
//        double materialCostPerSquareFoot = 0;
//        double laborCostPerSquareFoot = 0;
//        double taxRate = 0;
//        double materialCost = 0;
//        double laborCost = 0;
//        double total = 0;
//        double tax = 0;
//        double totalFinal = 0;
//        boolean editPossible = true;
//        Map<Integer, OrdersDto> map = null;
//
//        String date = getUserDateInput();
//
//        ordersdao.setOrderDate(date);
//
//        ordersdao.findFileMap();
//
//        if (ordersdao.findFileMap() == null) 
//            editPossible = false;
//
//        while (editPossible) {
//
//            Set<Map.Entry<Integer, OrdersDto>> set = ordersdao.findFileMap().entrySet();
////            for (Map.Entry<Integer, OrdersDto> entry : set) {
////                counter++;
////            }
////            String[] idArray = new String[counter];
////
////            for (Map.Entry<Integer, OrdersDto> entry : set) {
////                idArray[counter2] = (entry.getKey().toString());
////                counter2++;
////            }
//
//            Integer orderNum = Integer.parseInt(request.getParameter("orderNum"));
//
//            for (Map.Entry<Integer, OrdersDto> entry : set) {
//                if (orderNum == entry.getKey()) {
//                    orders = entry.getValue();
//                }
//            }
//
//            String name = request.getParameter("name");
//
//            if (!name.equals("")) {
//                orders.setName(name);
//            }
//
//            List<TaxesDto> stateTaxes = taxesdao.getTaxesList();
////            for (TaxesDto entry : stateTaxes) {
////                counter3++;
////            }
////            String[] stateArray = new String[counter3];
////
////            for (TaxesDto entry : stateTaxes) {
////                stateArray[counter4] = (entry.getState());
////                counter4++;
////            }
//            String state = request.getParameter("editState");
//
//            if (!state.equals("")) {
//                for (TaxesDto taxR : stateTaxes) {
//                    if (state.equals(taxR.getState())) {
//                        taxRate = taxR.getTaxRate();
//                    }
//                }
//                orders.setCustomerState(state);
//                orders.setTaxRate(taxRate);
//            }
//
//            List<ProductsDto> productType = productsdao.getProductsList();
////            for (ProductsDto products : productType) {
////                products.getProductType();
////                counter5++;
////            }
////
////            String[] productsArray = new String[counter5];
////
////            for (ProductsDto products : productType) {
////                productsArray[counter6] = products.getProductType();
////                counter6++;
////            }
//
//            String prodType = request.getParameter("editProd");
//            double area = Double.parseDouble(request.getParameter("editArea"));
//            orders.setArea(area);
//
//            if (!prodType.equals("")) {
//                for (ProductsDto products : productType) {
//                    if (products.getProductType().equals(prodType)) {
//                        materialCostPerSquareFoot = products.getMaterialCostPerSqFoot();
//                        laborCostPerSquareFoot = products.getLaborCostPerSqFoot();
//                    }
//                }
//                orders.setProductType(prodType);
//                orders.setMaterialsCostPerSqFoot(materialCostPerSquareFoot);
//                orders.setLaborCostPerSqFoot(laborCostPerSquareFoot);
//            }
//            if (!prodType.equals("")) {
//                materialCost = calculateMaterial(area, materialCostPerSquareFoot);
//                laborCost = calculateLabor(area, laborCostPerSquareFoot);
//                total = calculateTotal(materialCost, laborCost);
//                orders.setMaterialsCost(materialCost);
//                orders.setLaborCost(laborCost);
//            } else {
//                materialCost = orders.getMaterialsCost();
//                laborCost = orders.getLaborCost();
//                total = calculateTotal(materialCost, laborCost);
//            }
//
//            if (!state.equals("")) {
//                tax = calculateTax(total, taxRate);
//                orders.setTotalTax(total);
//            } else {
//                tax = orders.getTotalTax();
//            }
//
//            if (!prodType.equals("") || !state.equals("")) {
//                totalFinal = calculateTrueTotal(total, tax);
//                orders.setGrandTotal(total);
//            } else {
//                totalFinal = orders.getGrandTotal();
//            }
//
//            ordersdao.update(orders);
//            editPossible = false;
//            
////                List<OrdersDto> ordersList = ordersdao.list();
//
////        for (OrdersDto theOrders : ordersList) {
////            consoleio.println("Order Number: " + theOrders.getOrderNumber() + "\nName: " + theOrders.getName() + "\nCustomer State: "
////                    + theOrders.getCustomerState() + "\nTaxRate: " + theOrders.getTaxRate() + "\nProduct Type: "
////                    + theOrders.getProductType() + "\nArea: " + theOrders.getArea() + "\nCost Per Square Foot: "
////                    + theOrders.getMaterialsCostPerSqFoot() + "\nLabor Cost Per Square Foot: " + "\nMaterial Cost: "
////                    + theOrders.getMaterialsCost() + "\nLabor Cost: " + theOrders.getLaborCost() + "\nTax: "
////                    + theOrders.getTotalTax() + "\nTotal Cost: " + theOrders.getGrandTotal() + "\n");
////        }
//        }
//    }
//    
//
//    public void removeOrder(HttpServletRequest request, HttpServletResponse response) {
//
//        String fileName = "";
//        boolean removePossible = true;
//        int counter = 0;
//        int counter2 = 0;
//        Map<Integer, OrdersDto> map = null;
//        OrdersDto orders = null;
//
//        String date = getUserDateInput();
//
//        ordersdao.setOrderDate(date);
//
//        ordersdao.findFileMap();
//
//        if (ordersdao.findFileMap() == null) 
//            removePossible = false;
//
//        while (removePossible) {
//
//            Set<Map.Entry<Integer, OrdersDto>> set = ordersdao.findFileMap().entrySet();
//            for (Map.Entry<Integer, OrdersDto> entry : set) {
//                counter++;
//            }
//            String[] idArray = new String[counter];
//
//            for (Map.Entry<Integer, OrdersDto> entry : set) {
//                idArray[counter2] = (entry.getKey().toString());
//                counter2++;
//            }
//
//            Integer orderNum = Integer.parseInt(request.getParameter("deleteNum"));
//
//            for (Map.Entry<Integer, OrdersDto> entry : set) {
//                if (orderNum == entry.getKey()) {
//                    orders = entry.getValue();
//                }
//            }
//            ordersdao.delete(orders);
//            removePossible = false;
//        }
//    }
//
//    public double calculateTax(double total, double taxRate) {
//        double tax = total * taxRate;
//        return tax;
//    }
//
//    public double calculateMaterial(double costPerSquareFoot, double area) {
//        double material = costPerSquareFoot * area;
//        return material;
//    }
//
//    public double calculateLabor(double laborCostPerSquareFoot, double area) {
//        double labor = laborCostPerSquareFoot * area;
//        return labor;
//    }
//
//    public double calculateTotal(double a, double b) {
//        double c = 0;
//        c = a + b;
//        return c;
//    }
//
//    public double calculateTrueTotal(double total, double tax) {
//        double a = 0;
//        a = total + tax;
//        return a;
//    }
//
//    private String getUserDateInput() {
//        Scanner sc = new Scanner(System.in);
//        boolean isValidDate = false;
//        SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");
//        String userInput = "";
//        while (isValidDate == false) {
//
//            System.out.println("Type in the date like this: MMddyyyy Enter Date Here: ");
//            userInput = sc.nextLine();
//
//            formatter.setLenient(false);
//
//            try {
//                Date date = formatter.parse(userInput);
//                formatter.parse(userInput.trim());
//                isValidDate = true;
//
//            } catch (ParseException pe) {
//                System.out.println("Invalid Date");
//                isValidDate = false;
//            }
//            //String userInputDay = (userInput.substring(0, 2));
//            //String userInputMonth = (userInput.substring(2,4));
//            //String userInputYear = (userInput.substring(4,8));
//            //String userInputFileName = ("Order_"+userInputDay+userInputMonth+userInputYear+ ".txt");
//        }
//        return userInput;
//    }
//
//}
