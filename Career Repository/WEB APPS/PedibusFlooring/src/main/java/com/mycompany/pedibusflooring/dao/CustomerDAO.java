/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.dao;

import com.mycompany.pedibusflooring.dto.Customer;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class CustomerDAO {

    private Customer customer = new Customer();

    public Customer createCustomer(String firstName, String lastName) {

        this.customer.setFirstName(firstName);
        this.customer.setLastName(lastName);

        return this.customer;
    }

    public Customer getCustomer() {

        return this.customer;

    }

    public void removeCustomer() {

    }

    public Customer searchCustomer(String customer) {
        return null;
    }

    public List<Customer> listCustomers() {
        return null;
    }

}
