/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.controllers;

import com.mycompany.pedibusflooring.dao.OrderDAO;
import com.mycompany.pedibusflooring.dto.Order;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
public class SearchController {

    private int datePass;

    private OrderDAO dao;

    @Inject
    public SearchController(OrderDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search() {

        return "searchViewCust";
    }

    @RequestMapping(value = "search/searchName")
    public String searchName() {

        return "searchViewCust";

    }

    @RequestMapping(value = "search/searchDate", method = RequestMethod.POST)
    public String searchCustName(@RequestParam(value = "searchDate") String date, Model model) {

        String dateArray[] = date.split("/");
        Integer dateSplit = Integer.parseInt(dateArray[0] + dateArray[1] + dateArray[2]);

        datePass = dateSplit;

        dao.setDate(datePass);

        List<Order> custSearch = new ArrayList();
        custSearch = dao.searchDate();

        model.addAttribute("custSearch", custSearch);

        return "searchViewCust";

    }

    @RequestMapping(value = "search/view/{id}/{date}")
    public String show(@PathVariable("id") int id, @PathVariable("date") int date, Model model) {

        ArrayList<Order> newOrderList = dao.getOrderMap().get(date);

        for (Order order : newOrderList) {
            if (order.getId() == id) {
                model.addAttribute("order", order);
            }
        }

        return "view";

    }

    @RequestMapping(value = "search/delete/{id}/{date}")
    public String delete(@PathVariable("id") int id, @PathVariable("date") int date, Model model) {

        dao.removeOrder(date, id);

        return "searchViewCust";

    }

    @RequestMapping(value = "search/showEdit/{id}/{date}")
    public String showEdit(@PathVariable("id") Integer id, @PathVariable("date") Integer date, Model model) {

        datePass = date;

        ArrayList<Order> newOrderList = dao.getOrderMap().get(date);

        for (Order order : newOrderList) {
            if (order.getId() == id) {
                model.addAttribute("order", order);
            }
        }

        return "showEdit";

    }

    @RequestMapping(value = "search/edit/{id}", method = RequestMethod.POST)
    public String edit(@Valid @ModelAttribute Order order, BindingResult bindingResult,  @PathVariable("id") Integer id, Model model) {
        
        if(bindingResult.hasErrors()){
            String message = "Please Enter all Information";
            model.addAttribute("message", message);
           return "showEdit";
       }
        
        dao.editOrder(order, id, datePass);

        return "searchViewCust";
    }

    @RequestMapping(value = "/search/listAllFiles", method = RequestMethod.POST)
    public String listAllFiles(Model model) {
        
        int show =1;

        dao.listAllFiles();

        Set<Integer> files = dao.getOrderMap().keySet();

        List<ArrayList> list = new ArrayList<ArrayList>(dao.getOrderMap().values());

        model.addAttribute("list", list);
        model.addAttribute("show", show);

        return "searchViewCust";

    }
    
    

}
