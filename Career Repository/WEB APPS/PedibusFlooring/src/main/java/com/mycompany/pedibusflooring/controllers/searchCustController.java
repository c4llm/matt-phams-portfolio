/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.controllers;

import com.mycompany.pedibusflooring.dao.OrderDAO;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class searchCustController {

    private OrderDAO dao = new OrderDAO();


    @Inject
    public searchCustController(OrderDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/searchViewCust", method = RequestMethod.GET)
    public String search() {

        return "searchViewCust";
    }

}
