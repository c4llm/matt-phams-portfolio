/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.dao;

import com.mycompany.pedibusflooring.dto.Material;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class MaterialDAO {

    private Map<String, Material> materialMap = new HashMap<>();
    private Material material;
    private final String DELIMETER = ",";
    private final String FILENAME = "materials.txt";

    public Material createMaterial(Material material) {

        String type = material.getType();
        material.getCostSq();
        material.getLaborCostSq();

        getMaterialMap().put(type, material);

        return material;
    }

    public MaterialDAO() {

        materialDecode();

    }

//    public Material getMaterial() {
//
//        return this.material;
//    }
    public Set getMaterialSet() {

        Set<String> setMat = getMaterialMap().keySet();
        for (String value : setMat) {

        }

        return setMat;

    }

    public void removeMaterial() {

    }

    public Material searchMaterial(String material) {
        return null;
    }

    public List<Material> listMaterials() {
        return null;
    }

    public Material getMaterial(String type){
        Set<String> materialSet = getMaterialMap().keySet();
        for (String value : materialSet) {
            this.material = getMaterialMap().get(type);
          }
        return this.material;
    }

    /**
     * @return the materialMap
     */
    public Map<String, Material> getMaterialMap() {
        return materialMap;
    }

    public void materialDecode() {

        try {

            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                Material material = new Material();

                material.setType(values[0]);
                material.setCostSq(Double.parseDouble(values[1]));
                material.setLaborCostSq(Double.parseDouble(values[2]));

                createMaterial(material);

            }

        } catch (FileNotFoundException ex) {

        }

    }

}
