/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.dao;

import com.mycompany.pedibusflooring.dto.Customer;
import com.mycompany.pedibusflooring.dto.Material;
import com.mycompany.pedibusflooring.dto.State;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class StateDAO {

    private Map<String, State> stateMap = new HashMap<>();
    private final String DELIMETER = ",";
    private final String FILENAME = "states.txt";
    private State state;

    public StateDAO() {

        stateDecode();
    }

    public State createState(State state) {

        String name = state.getStateName();

        getStateMap().put(name, state);

        return state;
    }

    public void removeState() {

    }

    public List<State> listStates() {
        return null;
    }

    /**
     * @return the stateMap
     */
    public Map<String, State> getStateMap() {
        return stateMap;
    }

    /**
     * @param stateMap the stateMap to set
     */
    public void setStateMap(Map<String, State> stateMap) {
        this.stateMap = stateMap;
    }

    public State getState(String type) {
        Set<String> stateSet = getStateMap().keySet();
        for (String value : stateSet) {
            this.state = getStateMap().get(type);
        }
        return this.state;
    }

    public void stateDecode() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                State state = new State();

                state.setStateName(values[0]);
                state.setTaxRate(Double.parseDouble(values[1]));

                createState(state);

            }

        } catch (FileNotFoundException ex) {

        }

    }

}
