/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.dto;

/**
 *
 * @author apprentice
 */
public class Material {

    private String type;
    private double costSq;
    private double laborCostSq;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the costSq
     */
    public double getCostSq() {
        return costSq;
    }

    /**
     * @param costSq the costSq to set
     */
    public void setCostSq(double costSq) {
        this.costSq = costSq;
    }

    /**
     * @return the laborCostSq
     */
    public double getLaborCostSq() {
        return laborCostSq;
    }

    /**
     * @param laborCostSq the laborCostSq to set
     */
    public void setLaborCostSq(double laborCostSq) {
        this.laborCostSq = laborCostSq;
    }

}
