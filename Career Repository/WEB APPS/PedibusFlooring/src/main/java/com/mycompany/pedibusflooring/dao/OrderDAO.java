/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.dao;

import com.mycompany.pedibusflooring.dto.Customer;
import com.mycompany.pedibusflooring.dto.Material;
import com.mycompany.pedibusflooring.dto.Order;
import com.mycompany.pedibusflooring.dto.State;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class OrderDAO {

    private Map<Integer, ArrayList> orderMap = new HashMap<>();

    private ArrayList<Order> allOrders = new ArrayList<>();

    private int nextID = 1;

    private String dateOrder;

    private final String DELIMETER = ",";

    private final String FILENAME = dateOrder;

    private final String FILENAME2 = "config.txt";

    private Order order = new Order();

    private int theDate = 0;

    public OrderDAO() {

    }

    public Order createOrder(Order order) {

        orderMap.clear();

        dateOrder = filename(order);

        this.setAllOrders(decode());

        order.setId(nextID);

        nextID++;

        checkAddMap(order.getDate(), order);

        this.allOrders.add(order);

        addOrderToMap(order.getDate());

        this.setDateOrder(filename(order));

        prodEncode();

        orderMap.clear();

        return order;

    }

    public void editAddMessage(Order oldOrder, Order order) {

        dateOrder = filename(order);
        this.setAllOrders(decode());
        this.allOrders.add(oldOrder.getId(), order);
        addOrderToMap(order.getDate());
        prodEncode();
        orderMap.clear();

    }

//    public Order get(Integer id){
//       List array= new ArrayList();
//    }
//    public List searchByDate(int firstName) {
//
//        List<Order> values = new ArrayList(orderMap.values());
//        List<Order> searchList = new ArrayList();
//
//        for (Order order : values) {
//
//            if (order.getCustomer().getFirstName().equalsIgnoreCase(firstName)) {
//
//                searchList.add(order);
//            }
//        }
//        return searchList;
//    }
//    public void getCustomer(Customer customer) {
//
//        getOrder().setCustomer(customer);
//
//    }
//    public void getState(State state) {
//        StateDAO stateDao = new StateDAO();
//
//    }
    public List searchDate() {

        dateOrder = "Order_" + theDate + ".txt";
//
//            File f = new File(dateOrder);
//            if (f.exists() && !f.isDirectory()) {
//                isValid = true;
//            } else {
//                System.out.println("No Records Found");
//                isValid = false;
//            }
        setDateOrder(dateOrder);
        decode();

        ArrayList<Order> values = getOrderMap().get(theDate);
        return values;

    }

    public void setDate(int date) {

        //  getOrder().setDate(date);
        this.theDate = date;

    }

    public void addOrderToMap(int date) {

        this.orderMap.put(date, allOrders);

    }

    public void checkAddMap(int date, Order order) {

        Set<Integer> allSet = orderMap.keySet();

        for (Integer value : allSet) {

            if (date != value) {

                this.setAllOrders(new ArrayList<>());

            }

        }

    }

    public void removeOrder(int date, int ID) {

        ArrayList<Order> newOrderList = this.orderMap.get(date);

        for (Order orderRemove : newOrderList) {
            if (ID == orderRemove.getId()) {

                newOrderList.remove(orderRemove);
                break;
            }
        }
        this.orderMap.clear();
        this.orderMap.put(date, newOrderList);

        prodEncode();

    }

    public Order editOrder(Order order, int id, int oldDate) {

        dateOrder = filename(order);

        this.setAllOrders(decode());

        checkAddMap(order.getDate(), order);

        MaterialDAO materialDao = new MaterialDAO();
        StateDAO stateDao = new StateDAO();

        State state = stateDao.getState(order.getState().getStateName());
        order.setState(state);

        Material material = materialDao.getMaterial(order.getMaterial().getType());
        order.setMaterial(material);

        order.setLaborCost(order.getArea() * order.getMaterial().getLaborCostSq());
        order.setMaterialCost(order.getArea() * order.getMaterial().getCostSq());
        order.setTaxCost((order.getState().getTaxRate() / 100) * (order.getLaborCost() + order.getMaterialCost()));
        order.setTotal(order.getLaborCost() + order.getMaterialCost() + order.getTaxCost());

        if (!allOrders.isEmpty()) {
            allOrders.set(id - 1, order);
        } else {
            ArrayList<Order> newOrderList = orderMap.get(oldDate);
            this.orderMap.clear();
            for (Order editOrder : newOrderList) {
                if (editOrder.getId() == id) {
                    editOrder.getCustomer().setFirstName("File Edited | ");
                    editOrder.getCustomer().setLastName("Moved to " + dateView(order.getDate()));
                }

                dateOrder = filename(editOrder);
                this.allOrders.add(editOrder);

            }
            addOrderToMap(oldDate);
            prodEncode();
            this.allOrders.clear();
            this.allOrders.add(order);

        }

        this.orderMap.clear();

        addOrderToMap(order.getDate());

        this.setDateOrder(filename(order));

        prodEncode();

        return order;

    }

    public Set viewOrders() {

        Set<Integer> allSet = orderMap.keySet();

        return allSet;

    }

    public Map<Integer, ArrayList> getOrderMap() {
        return orderMap;
    }

    public String filename(Order order) {

        int date = order.getDate();

        String filename2 = String.valueOf(date);

        String filename = "Order_" + filename2 + ".txt";

        return filename;

    }

    public Order searchOrder(int date, int ID) {
        Order anOrder = null;
        ArrayList<Order> newOrderList = orderMap.get(date);
        for (Order order : newOrderList) {
            if (ID == order.getId()) //put in arraylist and return iterated arrayList 
            {
                anOrder = newOrderList.get(ID - 1);
            }
        }
        return anOrder;
    }

    private void encode() {

        int nextLine = 1;

        try {

            PrintWriter pw = new PrintWriter(new FileWriter(dateOrder));

            Set<Integer> setOrder = orderMap.keySet();
            for (Integer order : setOrder) {

                ArrayList<Order> billows = orderMap.get(order);

                for (Order key : billows) {

                    String line
                            = nextLine + ", "
                            + key.getCustomer().getFirstName() + DELIMETER
                            + key.getCustomer().getLastName() + DELIMETER
                            + key.getState().getStateName() + DELIMETER
                            + key.getState().getTaxRate() + DELIMETER
                            + key.getMaterial().getType() + DELIMETER
                            + key.getArea() + DELIMETER
                            + key.getMaterial().getCostSq() + DELIMETER
                            + key.getMaterial().getLaborCostSq() + DELIMETER
                            + key.getMaterialCost() + DELIMETER
                            + key.getLaborCost() + DELIMETER
                            + key.getTaxCost() + DELIMETER
                            + key.getTotal();

                    pw.println(line);

                    pw.flush();

                    nextLine++;

                }

            }
        } catch (IOException ex) {

        }

    }

    public void editEncode() {

    }

    public boolean decodeConfig() {

        boolean config = true;

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME2)));

            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                String value = currentLine;

                if (currentLine.contains("test")) {

                    config = false;
                }
                break;
            }
        } catch (FileNotFoundException ex) {

        }

        return config;
    }

    public void prodEncode() {

        boolean isValid = decodeConfig();

        if (isValid) {
            encode();
        } else {
            isValid = false;
        }

    }

    public ArrayList<Order> decode() {

        ArrayList<Order> orders = new ArrayList<>();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(dateOrder)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                Order order = new Order();

                Customer customer = new Customer();

                order.setId(Integer.parseInt(values[0]));

                customer.setFirstName(values[1]);
                customer.setLastName(values[2]);
                order.setCustomer(customer);

                State state = new State();
                state.setStateName(values[3]);
                state.setTaxRate(Double.parseDouble(values[4]));
                order.setState(state);

                Material material = new Material();
                material.setType(values[5]);

                order.setArea(Double.parseDouble(values[6]));

                material.setCostSq(Double.parseDouble(values[7]));
                material.setLaborCostSq(Double.parseDouble(values[8]));

                order.setMaterial(material);
                order.setMaterialCost(Double.parseDouble(values[9]));
                order.setLaborCost(Double.parseDouble(values[10]));
                order.setTaxCost(Double.parseDouble(values[11]));
                order.setTotal(Double.parseDouble(values[12]));

                String[] date = dateOrder.split("_|\\W+");
                int date2 = Integer.parseInt(date[1]);

                order.setDate(date2);

                order.setDateView(dateView(date2));

                orders.add(order);

                orderMap.put(date2, orders);

            }

        } catch (FileNotFoundException ex) {

        }
        return orders;
    }

    /**
     * @param dateOrder the dateOrder to set
     */
    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    public String checkComma(String value) {

        for (int i = 0; i < value.length(); i++) {
            if (value.contains("/")) {
                int index = value.indexOf("/");
                StringBuilder newName = new StringBuilder(value);
                newName.setCharAt(index, ',');
                value = newName.toString();
            }

        }
        return value;

    }

    /**
     * @param allOrders the allOrders to set
     */
    public void setAllOrders(ArrayList<Order> allOrders) {
        this.allOrders = allOrders;
    }

    /**
     * @return the FILENAME
     */
    public String getFILENAME() {
        return FILENAME;
    }

    public void listAllFiles() {

        File sourceDirectory = new File(".");
        File[] anyFile = sourceDirectory.listFiles();
        ArrayList<File> result = new ArrayList<>();
        if (null != anyFile) {
            for (int i = 0; i < anyFile.length; i++) {
                if (anyFile[i].getName().startsWith("Order_")) {
                    result.add(anyFile[i]);
                }
            }
        }

        for (int j = 0; j < result.size(); j++) {
            setDateOrder(result.get(j).getName());
            decode();
        }

    }

    public String dateView(int date) {

        String userDateFm = date + "";

        for (int i = 0; i <= 5; i++) {
            StringBuilder userDateFm1 = new StringBuilder(userDateFm);
            if (userDateFm1.toString().length() < 8) {
                userDateFm1.insert(0, 0);
            }
            userDateFm1.insert(i + 2, "/");
            i = i + 2;
            userDateFm = userDateFm1.toString();
        }
        return userDateFm;
    }

}
