package com.mycompany.pedibusflooring.controllers;

import com.mycompany.pedibusflooring.dao.OrderDAO;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    private OrderDAO dao = new OrderDAO();

    @Inject
    public HomeController(OrderDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {

        return "index";
    }

}
