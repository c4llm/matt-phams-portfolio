/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.controllers;

import com.mycompany.pedibusflooring.dao.MaterialDAO;
import com.mycompany.pedibusflooring.dao.OrderDAO;
import com.mycompany.pedibusflooring.dao.StateDAO;
import com.mycompany.pedibusflooring.dto.Customer;
import com.mycompany.pedibusflooring.dto.Material;
import com.mycompany.pedibusflooring.dto.Order;
import com.mycompany.pedibusflooring.dto.State;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
public class OrderController {

    private OrderDAO dao;
    Customer customer;
    Material material;
    StateDAO stateDao;
    State state;
    MaterialDAO materialDao;

    @Inject
    public OrderController(OrderDAO dao, Customer customer, Material material, StateDAO stateDao, State state, MaterialDAO materialDao) {
        this.dao = dao;
        this.customer = customer;
        this.material = material;
        this.stateDao = stateDao;
        this.state = state;
        this.materialDao = materialDao;
    }

    @RequestMapping(value = "/addOrder", method = RequestMethod.GET)
    public String search() {

        return "redirect:/";
    }

    @RequestMapping(value = "/addOrder/addOrder", method = RequestMethod.GET)
    public String addOrder() {

        return "redirect:/";

    }

    @RequestMapping(value = "/addOrder/add", method = RequestMethod.POST)
    public String add(@Valid @ModelAttribute Order order, BindingResult bindingResult, @RequestParam(value = "date") String date, @RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName, @RequestParam(value = "state") String statePass, @RequestParam(value = "material") String materialPass, @RequestParam(value = "area") String areaPass, Model model) {

        if (bindingResult.hasErrors()) {
            

            return "redirect:/";
        }

        String dateArray[] = date.split("/");
        Integer dateSplit = Integer.parseInt(dateArray[0] + dateArray[1] + dateArray[2]);

        order.setDate(dateSplit);

        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        order.setCustomer(customer);

        state = stateDao.getState(statePass);
        order.setState(state);

        material = materialDao.getMaterial(materialPass);
        order.setMaterial(material);

        order.setLaborCost(order.getArea() * order.getMaterial().getLaborCostSq());
        order.setMaterialCost(order.getArea() * order.getMaterial().getCostSq());
        order.setTaxCost((order.getState().getTaxRate() / 100) * (order.getLaborCost() + order.getMaterialCost()));
        order.setTotal(order.getLaborCost() + order.getMaterialCost() + order.getTaxCost());

        dao.createOrder(order);

        String message = "You Created an Order for " + date;

        model.addAttribute("message", message);

        return "redirect:/";
    }

}
