/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pedibusflooring.dto;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Order {

    private int date;
    private String dateView;
    private Customer customer;
    private Material material;
    private State state;
    @NotNull(message = "Please add an area for the order")
    private double area;
    private double materialCost;
    private double laborCost;
    private double total;
    private int id;
    private double taxCost;

    public Order() {

    }

    public Order(Order copyOrder) {

        Customer customer = new Customer();
        customer.setFirstName(copyOrder.getCustomer().getFirstName());
        customer.setLastName(copyOrder.getCustomer().getLastName());
        this.setCustomer(customer);

        State state = new State();
        state.setStateName(copyOrder.getState().getStateName());
        state.setTaxRate(copyOrder.getState().getTaxRate());
        this.setState(state);

        Material material = new Material();
        material.setType(copyOrder.getMaterial().getType());
        material.setLaborCostSq(copyOrder.getMaterial().getLaborCostSq());
        material.setCostSq(copyOrder.getMaterial().getCostSq());
        this.setMaterial(material);

        this.date = copyOrder.date;
        this.id = copyOrder.id + 1;
        this.laborCost = copyOrder.laborCost;
        this.taxCost = copyOrder.taxCost;
        this.total = copyOrder.total;
        this.materialCost = copyOrder.materialCost;
        this.area = copyOrder.area;

    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(Material material) {
        this.material = material;
    }

    /**
     * @param State the State to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @return the material
     */
    public Material getMaterial() {
        return material;
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @return the area
     */
    public double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */

    public void setArea(double area) {
        this.area = area;
    }

    /**
     * @return the materialCost
     */
    public double getMaterialCost() {
        return materialCost;
    }

    /**
     * @param materialCost the materialCost to set
     */
    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    /**
     * @return the laborCost
     */
    public double getLaborCost() {
        return laborCost;
    }

    /**
     * @param laborCost the laborCost to set
     */
    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the taxCost
     */
    public double getTaxCost() {
        return taxCost;
    }

    /**
     * @param taxCost the taxCost to set
     */
    public void setTaxCost(double taxCost) {
        this.taxCost = taxCost;
    }

    /**
     * @return the dateView
     */
    public String getDateView() {
        return dateView;
    }

    /**
     * @param dateView the dateView to set
     */
    public void setDateView(String dateView) {
        this.dateView = dateView;
    }

}
