<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Pedibus Flooring</title>
        <!-- Include Required Prerequisites -->
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.searchable.js"></script>

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/Foot-icon.png">


        <!-- Custom CSS -->
        <link href="${pageContext.request.contextPath}/css/freelancer.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <style>

            #bootstrap-override  th  {
                text-align: center;

            }
            #bootstrap-override .edit{

                color:#003300;
            }

            #bootstrap-override .remove{

                color:#e60000;
            }


            #bootstrap-override table caption {
                padding: .5em 0;
            }

            #bootstrap-override .p {

                text-align:center;
                padding-top: 140px;
                font-size: 14px;

            }
            #bootstrap-override tr:hover{
                background-color:rgb(88,88,88) ;
            }

        </style>
        <script type="text/javascript">

            $(document).ready(function () {
                if (${show == 1}) {
                    $('.doobs').show();
                }
            });
        </script> 

        <script type="text/javascript">

            $(document).ready(function () {
                $('.table').searchable({
                    striped: true,
                    searchType: 'fuzzy',
                    show: function (elem) {
                        elem.slideDown(100);
                    },
                    hide: function (elem) {
                        elem.slideUp(100);
                    }
                });
            });
        </script>  

    </head>
    <body id="bootstrap-override">
        <!-- Navigation -->
        <jsp:include page="/jsp/navbar.jsp"></jsp:include>


            <header>
                <div class="container">
                    <div class="row">
                    <form:form class="form-horizontal" action="${pageContext.request.contextPath}/search/searchDate" method="post">

                        <!-- search box for text input -->
                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="search-date-order"></label>

                                <input type="text" name="searchDate" value="01/01/2010" class="form-control" id="search-date-order" />
                            </div>
                        </div>
                        <h2>Search Orders</h2>

                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tbody>
                                        <tr>
                                            <th width="20%">Customer Name</th>
                                            <th width="20%">Material Cost </th>
                                            <th width="20%">Labor Cost</th>
                                            <th width="20%">Total Cost</th>
                                            <th width="5%">Details</th>
                                            <th width="5%">View</th>
                                            <th width="5%">Edit</th>
                                            <th width="5%">Remove</th>
                                        </tr>
                                        <tr class="accordion-toggle">
                                            <c:forEach items="${custSearch}" var="cust" varStatus="topCount">
                                            <tr>
                                                <td>${cust.getCustomer().getFirstName()} ${cust.getCustomer().getLastName()}</td>
                                                <td>${cust.getMaterialCost()}</td>
                                                <td>${cust.getLaborCost()}</td>
                                                <td>${cust.getTotal()}</td>
                                                <td class="details" data-target="#co${topCount.count}" data-toggle="collapse"><span class= "glyphicon glyphicon-chevron-down"></span></td>
                                                <td><a  class="view" href="${pageContext.request.contextPath}/search/view/${cust.getId()}/${cust.getDate()}"><span class= "glyphicon glyphicon-eye-open"></span></a></td>
                                                <td><a  class="edit" href="${pageContext.request.contextPath}/search/showEdit/${cust.getId()}/${cust.getDate()}"><span class= "glyphicon glyphicon-pencil"></span></a></td>
                                                <td><a  class= "remove" href="${pageContext.request.contextPath}/search/delete/${cust.getId()}/${cust.getDate()}"><span class= "glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">
                                                    <div id="co${topCount.count}" class="collapse">
                                                        State: ${cust.state.stateName}<br/>
                                                        Material: ${cust.material.type} <br/>
                                                        Material Total: ${cust.materialCost} <br/>
                                                        Labor Total: ${cust.laborCost} <br/>
                                                        Tax Total: ${cust.taxCost} <br/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div><!--end of .table-responsive-->
                        </form:form>
                    </div>


                    <form:form class="form-horizontal" action="${pageContext.request.contextPath}/search/listAllFiles" method="post">
                        <input type="submit" name="showAll"  id="showAll" class="btn btn-primary" value="Show All"  />
                        <input type="search" id="search" value="" class="doobs" placeholder="Search All Orders" style="display:none">
                        <br  />
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">

                                    <thead>
                                    <tbody>

                                        <tr>
                                            <th width="20%">Customer Name</th>
                                            <th width="20%">Order Date</th>
                                            <th width="20%">State</th>
                                            <th width="20%">Total Cost</th>
                                            <th width="5%">Details</th>
                                            <th width="5%">View</th>
                                            <th width="5%">Edit</th>
                                            <th width="5%">Remove</th>
                                        </tr>
                                        <c:set var="count" value="0" scope="page" />
                                        <c:forEach items="${list}" var="lista">

                                            <tr class="accordion-toggle">
                                                <c:forEach items="${lista}" var="list"  varStatus="theCount">
                                                    <c:set var="count" value="${count + 1}" scope="page"/>
                                                    <td>${list.customer.firstName} ${list.customer.lastName}</td>
                                                    <td>${list.dateView}</td>
                                                    <td>${list.state.stateName}</td>
                                                    <td>${list.total}</td>
                                                    <td class="details" data-target="#co${count}" data-toggle="collapse"><span class= "glyphicon glyphicon-chevron-down"></span></td>
                                                    <td><a  class="view" href="${pageContext.request.contextPath}/search/view/${list.getId()}/${list.getDate()}"><span class= "glyphicon glyphicon-eye-open"></span></a></td>
                                                    <td><a  class="edit" href="${pageContext.request.contextPath}/search/showEdit/${list.getId()}/${list.getDate()}"><span class= "glyphicon glyphicon-pencil"></span></a></td>
                                                    <td><a  class= "remove" href="${pageContext.request.contextPath}/search/delete/${list.getId()}/${list.getDate()}"><span class= "glyphicon glyphicon-trash"></span></a></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">
                                                        <div id="co${count}" class="collapse">

                                                            Material Total: ${list.materialCost} <br/>
                                                            Labor Total: ${list.laborCost} <br/>
                                                            Tax Total: ${list.taxCost} <br/>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </c:forEach>

                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div><!--end of .table-responsive-->


                        </div>
                    </div>


                </form:form>
            </div>
        </header>



        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

        <script type="text/javascript">
                $('input[name="searchDate"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    autoUpdateInput: true

                });
        </script>
    </body>
</html>

