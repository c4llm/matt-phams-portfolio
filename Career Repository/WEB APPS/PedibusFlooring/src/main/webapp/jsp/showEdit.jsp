<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Order Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">


        <!-- Custom CSS -->
        <link href="${pageContext.request.contextPath}/css/freelancer.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

        <style> 
            .add{
                padding-left: 160px;
                margin-bottom:50px;
            }

        </style>
    </head>
    <body>


        <header>
            <div class="container">
                <div class="col-md-9">
                    <h3>${message}</h3>

                    <h2 class="add">Edit Order</h2>

                    <form:form class="form-horizontal" commandName="order" action="${pageContext.request.contextPath}/search/edit/${order.getId()}" method="post">

                        ${date}


                        <div class="form-group">
                            <label for="add-order-date" class="col-md-4 control-label">Date:</label>
                            <div class="col-md-8">
                                <form:input path="date" type="text" name="date" class="form-control" id="add-order-date" placeholder="Date" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-firstName" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8">
                                <form:input path="customer.firstName" type="text" name="firstName" class="form-control" id="add-order-date" placeholder="Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-release-date" class="col-md-4 control-label">Last Name:</label>
                            <div class="col-md-8">
                                <form:input path="customer.lastName" type="text" name="lastName" class="form-control" id="add-order-date" placeholder="DVD Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-release-date" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8">
                                <form:input path="state.stateName" type="text" name="stateName" class="form-control" id="add-order-date" placeholder="DVD Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-release-date" class="col-md-4 control-label">Material:</label>
                            <div class="col-md-8">
                                <form:input path="material.type" type="text" name="type" class="form-control" id="add-order-date" placeholder="DVD Name" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-dvd-name" class="col-md-4 control-label">Set Area:</label>
                            <div class="col-md-8">
                                <form:input path="area" type="text" name="area" class="form-control" id="add-order-date" placeholder="Area" />
                            </div>
                        </div>

                        <input type="submit" class="btn btn-default pull-right" value="Create Order" />
                    </form:form>
                </div>
            </div>
        </header>
    </body>
</html>
