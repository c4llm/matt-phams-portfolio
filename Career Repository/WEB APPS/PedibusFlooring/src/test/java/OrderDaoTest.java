/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.pedibusflooring.dao.MaterialDAO;
import com.mycompany.pedibusflooring.dao.OrderDAO;
import com.mycompany.pedibusflooring.dao.StateDAO;
import com.mycompany.pedibusflooring.dto.Customer;
import com.mycompany.pedibusflooring.dto.Material;
import com.mycompany.pedibusflooring.dto.Order;
import com.mycompany.pedibusflooring.dto.State;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class OrderDaoTest {

    OrderDAO orderdao = new OrderDAO();
    Order order;

    @Inject
    public OrderDaoTest(OrderDAO order) {
        this.orderdao = order;
    }

    @Before
    public void setUp() {
        Material material = new Material();
        StateDAO stateDao = new StateDAO();
        State state = new State();
        MaterialDAO materialDao = new MaterialDAO();

        Customer customer = new Customer();

        order.setDate(01012016);
        customer.setFirstName("Matt");
        customer.setLastName("Pham");
        order.setCustomer(customer);
        state = stateDao.getState("OHIO");
        order.setState(state);
        material = materialDao.getMaterial("WOOD");
        order.setMaterial(material);
        order.setArea(500);

        order.setLaborCost(order.getArea() * order.getMaterial().getLaborCostSq());
        order.setMaterialCost(order.getArea() * order.getMaterial().getCostSq());
        order.setTaxCost((order.getState().getTaxRate() / 100) * (order.getLaborCost() + order.getMaterialCost()));
        order.setTotal(order.getLaborCost() + order.getMaterialCost() + order.getTaxCost());

        orderdao.createOrder(order);
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void addOrder() {
        Order target = order;
        Assert.assertEquals(target.getId(), order.getId());

    }

//    @Test
//    public void editOrder() {
//        Order editThisOrder = null;
//        Customer customer = new Customer();
//        ArrayList<Order> newOrderList = orderdao.getOrderMap().get(01012015);
//
//        for (Order o : newOrderList) {
//            if (o == order) {
//                editThisOrder = order;
//            }
//
//            customer.setFirstName("Henry");
//            customer.setLastName("Locke");
//            editThisOrder.setCustomer(customer);
//            orderdao.editOrder(editThisOrder, editThisOrder.getId());
//
//            Assert.assertEquals(editThisOrder, order);
//
//        }
//    }

    @Test
    public void deleteOrder() {
        Material material = new Material();
        StateDAO stateDao = new StateDAO();
        State state = new State();
        MaterialDAO materialDao = new MaterialDAO();
        Customer customer = new Customer();
        boolean failed = false;

        order.setDate(01012015);
        customer.setFirstName("Dave");
        customer.setLastName("Phantom");
        order.setCustomer(customer);
        state = stateDao.getState("OHIO");
        order.setState(state);
        material = materialDao.getMaterial("WOOD");
        order.setMaterial(material);
        order.setArea(3000);

        order.setLaborCost(order.getArea() * order.getMaterial().getLaborCostSq());
        order.setMaterialCost(order.getArea() * order.getMaterial().getCostSq());
        order.setTaxCost((order.getState().getTaxRate() / 100) * (order.getLaborCost() + order.getMaterialCost()));
        order.setTotal(order.getLaborCost() + order.getMaterialCost() + order.getTaxCost());

        orderdao.createOrder(order);
        orderdao.removeOrder(01012016, order.getId());

        ArrayList<Order> newOrderList = orderdao.getOrderMap().get(01012015);

        for (Order o : newOrderList) {
            if (o.getId() == order.getId()) {
                failed = true;
            }
        }
        Assert.assertEquals(failed, false);
    }
    
    @Test
    public void searchDate(){
        Material material = new Material();
        StateDAO stateDao = new StateDAO();
        State state = new State();
        MaterialDAO materialDao = new MaterialDAO();
        boolean test1 = false;
        boolean test2 = false;

        Customer customer = new Customer();

        order.setDate(01012016);
        customer.setFirstName("Todd");
        customer.setLastName("Rank");
        order.setCustomer(customer);
        state = stateDao.getState("OHIO");
        order.setState(state);
        material = materialDao.getMaterial("WOOD");
        order.setMaterial(material);
        order.setArea(5000);

        order.setLaborCost(order.getArea() * order.getMaterial().getLaborCostSq());
        order.setMaterialCost(order.getArea() * order.getMaterial().getCostSq());
        order.setTaxCost((order.getState().getTaxRate() / 100) * (order.getLaborCost() + order.getMaterialCost()));
        order.setTotal(order.getLaborCost() + order.getMaterialCost() + order.getTaxCost());

        orderdao.createOrder(order);
        
        List date=orderdao.searchDate();
        List <Order> dates = new ArrayList(date);
//        dates
//             .stream()
//             .filter(o -> o.getCustomer().getFirstName().equals("Matt"))
//             .forEach(o -> );
        for (Order o: dates){
            if(o.getCustomer().getFirstName().equals("Matt")){
                test1 = true;
            }
            if(o.getCustomer().getFirstName().equals("Todd")){
                test2 = true;
            }
        }
        
        Assert.assertEquals(test1,true);
        Assert.assertEquals(test2,true);
    }
}
