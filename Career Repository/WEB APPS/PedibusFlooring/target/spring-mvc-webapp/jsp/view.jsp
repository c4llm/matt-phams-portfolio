<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Pedibus Flooring</title>

        <!-- Javascript for tablegrowth-->
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/tablegrow.js"></script>


        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/Foot-icon.png">


        <!-- Custom CSS -->
        <link href="${pageContext.request.contextPath}/css/freelancer.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <style>

            #bootstrap-override h2, #bootstrap-override  th  {
                text-align: center;

            }
            #bootstrap-override .edit{

                color:#003300;
            }

            #bootstrap-override .remove{

                color:#e60000;
            }


            #bootstrap-override table caption {
                padding: .5em 0;
            }

            #bootstrap-override .p {

                text-align:center;
                padding-top: 140px;
                font-size: 14px;

            }
            #bootstrap-override tr:hover{
                background-color: #4d4dff;
            }

        </style>

    </head>
    <body id="bootstrap-override">
        <!-- Navigation -->
        <jsp:include page="/jsp/navbar.jsp"></jsp:include>


            <header>

                <h2>Orders View</h2>

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>

                                        <tr>
                                            <th width="20%">Customer Name</th>
                                            <th width="11.5%">Material </th>
                                            <th width="11.5%">Material Cost</th>
                                            <th width="11.5%">State</th>
                                            <th width="11.5%">Labor Cost </th>
                                            <th width="11.5%">Area(sq/ft)</th>
                                            <th width="11.5%">Tax Cost</th>
                                            <th width="11.5%">Total Cost</th>

                                        </tr>
                                    </thead>
                                    <tbody>       

                                        <tr>

                                        <td>${order.customer.firstName} ${order.customer.lastName}</td>
                                        <td>${order.material.type}</td>
                                        <td>${order.materialCost}</td>
                                        <td>${order.state.stateName}</td>
                                        <td>${order.laborCost}</td>
                                        <td>${order.area}</td>
                                        <td>${order.taxCost}</td>
                                        <td>${order.total}</td>

                                    </tr>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="8" class="text-center"> <h2>${order.dateView} | ${order.customer.lastName}</h2></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!--end of .table-responsive--
                    </div>
                </div>
            </div>
        </header>


        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

