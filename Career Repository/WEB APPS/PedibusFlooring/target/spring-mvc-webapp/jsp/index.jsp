<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Pedibus Flooring</title>
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">


        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/Foot-icon.png">


        <!-- Custom CSS -->
        <link href="${pageContext.request.contextPath}/css/freelancer.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

        <style>
            #addform{
                padding-top: 35px;

            }
            #selector{

              
            }
        </style>

    </head>

    <body>

        <div class="container">

            <!-- Navigation -->
            <jsp:include page="/jsp/navbar.jsp"></jsp:include>


                <header>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5">
                                <a href="${pageContext.request.contextPath}/search">
                                <img class="img-responsive" src="img/search.png" alt="">
                            </a>
                            <div class="intro-text">
                                <span class="name">Search</span>                              
                            </div>
                        </div>
                        <div id=addform  class="col-lg-7">
                            <form:form class="form-horizontal" action="${pageContext.request.contextPath}/addOrder/add" method="post">
                                ${message}
                                <div class="form-group">
                                    <label for="add-date" class="col-md-4 control-label">Date:</label>
                                    <div class="col-md-5">
                                        <input type="text" name="date" class="form-control" id="add-dvd-name" value="01/01/2010" placeholder="Order Date" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="add-dvd-name" class="col-md-4 control-label">First Name:</label>
                                    <div class="col-md-5">
                                        <input type="text" name="firstName" class="form-control" id="add-dvd-name" placeholder="Customer First Name" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="add-release-date" class="col-md-4 control-label">Last Name:</label>
                                    <div class="col-md-5">
                                        <input type="text" name="lastName" class="form-control" id="add-release-date" placeholder="Customer Last Name" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="add-state" class="col-md-4 control-label">State:</label>
                                    <div id="selector" class="col-md-3">

                                        <select name="state" class="form-control" id="add-state">
                                            <option value="INDIANA">Indiana</option>
                                            <option value="MICHIGAN">Michigan</option>
                                            <option value="OHIO">Ohio</option>
                                            <option value="PENNSYLVANIA">Pennsylvania</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="add-material" class="col-md-4 control-label">Material:</label>
                                    <div id="selector" class="col-md-3">

                                        <select name="material" class="form-control" id="add-state">
                                            <option value="CARPET">Carpet</option>
                                            <option value="LAMINATE">Laminate</option>
                                            <option value="TILE">Tile</option>
                                            <option value="WOOD">Wood</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="add-dvd-name" class="col-md-4 control-label">Set Area:</label>
                                    <div class="col-md-5">
                                        <input type="text" name="area" class="form-control" id="add-dvd-name" placeholder="Set Area" />
                                    </div>
                                </div>

                                <input type="submit" class="btn btn-default" value="Create Order" />
                            </form:form>
                            <div class="intro-text">
                                <span class="name">Add Order</span>                              
                            </div>
                        </div>
                    </div>
                </div>
            </header>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $('input[name="date"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
        </script>

    </body>
</html>

