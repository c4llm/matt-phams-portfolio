<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Order Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">


        <!-- Custom CSS -->
        <link href="${pageContext.request.contextPath}/css/freelancer.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

        <style> 

        </style>
    </head>
    <body>

        <div class="container">

            <!-- Navigation -->
            <jsp:include page="/jsp/navbar.jsp"></jsp:include>

                <header>
                    <div class="container">
                        <h1 style="background: -webkit-linear-gradient(top, rgba(252,202,194,1) 0%, rgba(252,202,194,1) 42%, rgba(255,101,84,1) 175%);
                            position: relative; bottom: 50px;   -webkit-background-clip: text;
                            -webkit-text-fill-color: transparent; text-shadow">Order Menu</h1>

                        <div class="row">
                            <div class="col-lg-4">
                                <a href="${pageContext.request.contextPath}/order/addOrder">
                                <img class="img-responsive" src="img/check.png" alt="">
                            </a>
                            <div class="intro-text">
                                <span class="name">Add</span>                              
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <a href="${pageContext.request.contextPath}/order/">
                                <img class="img-responsive" src="img/pencil-icon.png" style ="width:256px;" alt="">
                            </a>
                            <div class="intro-text">
                                <span class="name">Edit</span>        
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <a href="${pageContext.request.contextPath}/order/">
                                <img class="img-responsive" src="img/Remove-icon.png"  style ="width:256px;" alt="">
                            </a>
                            <div class="intro-text">
                                <span class="name">Remove</span>        
                            </div>
                        </div>
                    </div>
                </div>
            </header>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
