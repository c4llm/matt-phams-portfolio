/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary_web.controllers;

import com.mycompany.dvdlibrary_web.dao.DVDDaoInter;
import com.mycompany.dvdlibrary_web.dto.DVD;
import com.thesoftwareguild.interfaces.dto.Dvd;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {

    private DVDDaoInter dao = new DVDDaoInter();

    @Inject
    public HomeController(DVDDaoInter dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        List<DVD> dvds = dao.listAll();

        model.addAttribute("dvdList", dvds);

        return "index";
    }

}
