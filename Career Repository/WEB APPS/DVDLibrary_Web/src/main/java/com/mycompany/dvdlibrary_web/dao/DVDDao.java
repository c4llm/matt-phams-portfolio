package com.mycompany.dvdlibrary_web.dao;

import com.mycompany.DVDLibraryDto.DVD;
import com.thesoftwareguild.interfaces.dao.DvdLibraryDao;
import com.thesoftwareguild.interfaces.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class DVDDao implements DvdLibraryDao {

    private final String FILENAME = "dvdLibrary.txt";
    private final String DELIMETER = "::::";

    private Map<Integer, Dvd> DVDMap = new HashMap<>();
    private Integer nextID = 1;

    public DVDDao() {
        try {
            List<Dvd> dvdList = decode();
            for (Dvd dvd : dvdList) {
                DVDMap.put(dvd.getId(), dvd);
                if (dvd.getId() > nextID) {
                    nextID = dvd.getId() + 1;
                }
            }
            nextID++;
        } catch (Exception ex) {

        }
    }

    public List<Dvd> listAllDVD() {
        List<Dvd> dvdList = new ArrayList<>();
        dvdList.addAll(getDVDMap().values());
        return dvdList;
    }

    public Set<Integer> getkeySet() {
        return DVDMap.keySet();
    }

//    @Override
//    public DVD createDVD(DVD newDVD) {
//
//        newDVD.setDVDID(nextID);
//        getDVDMap().put(newDVD.getDVDID(), newDVD);
//        encode();
//        nextID++;
//        return newDVD;
//
//    }
//    @Override
//    public void removeDVD(int id) {
////
////        listAllDVD()
////                .stream()
////                .filter(s -> s.getDVDID() == id)
////                .forEach(s -> getDVDMap().remove(id));
//
//        
//        
//        getDVDMap().remove(dvdToDelete.getDVDID());
//        encode();
//    }
//    @Override
//    public DVD updateDVD(DVD thisDVD) {
//
//        getDVDMap().put(thisDVD.getDVDID(), thisDVD);
//        encode();
//        return thisDVD;
//    }
    public Map<Integer, Dvd> getDVDMap() {
        return DVDMap;
    }

//    @Override
//    public List<DVD> getDvdList(String dvdTitle) {
//        List<DVD> dvdList = new ArrayList<>();
//
//        Set<Integer> keys = getDVDMap().keySet();
//        for (Integer key : keys) {
//            if (DVDMap.get(key).getTitle().equalsIgnoreCase(dvdTitle)) {
//                dvdList.add(DVDMap.get(key));
//            }
//        }
//        return dvdList;
//    }
//    @Override
//    public Dvd getDVD(int dvdId) {
//        return getDvdMap().get(dvdId);
//    }
//    @Override
//    public DVD getDvd(String dvdTitle) {
//        Set<Integer> keys = getDVDMap().keySet();
//        for (Integer key : keys) {
//            if (getDVDMap().get(key).getTitle().equalsIgnoreCase(dvdTitle)) {
//                return getDVDMap().get(key);
//            }
//        }
//        return null;
//    }
    //_____________))))))))))))))))___________________))))))))))))))))))))))))))))
    private void encode() {
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(FILENAME));
            Set<Integer> iD = DVDMap.keySet();
            for (Integer key : iD) {
                String line = key + DELIMETER
                        + DVDMap.get(key).getTitle() + DELIMETER
                        + DVDMap.get(key).getReleaseDate() + DELIMETER
                        + DVDMap.get(key).getDirector() + DELIMETER
                        + DVDMap.get(key).getStudio() + DELIMETER
                        + DVDMap.get(key).getMpaaRating() + DELIMETER
                        + DVDMap.get(key).getMpaaRating() + DELIMETER
                        + DVDMap.get(key).getNote() + DELIMETER;
                pw.println(line);
                pw.flush();
            }
        } catch (IOException ex) {
        }
    }

    private List<Dvd> decode() {
        List<Dvd> dvdList = new ArrayList<>();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                Dvd dvdHolder = new Dvd();

                dvdHolder.setId(Integer.parseInt(values[0]));
                dvdHolder.setTitle(values[1]);

                String date = values[2];
                LocalDate newdate = LocalDate.parse(date);
                dvdHolder.setReleaseDate(newdate);

                dvdHolder.setDirector(values[3]);
                dvdHolder.setStudio(values[4]);
                dvdHolder.setMpaaRating(values[5]);
                dvdHolder.setNote(values[6]);

                dvdList.add(dvdHolder);

            }

        } catch (FileNotFoundException ex) {

        }

        return dvdList;
    }

    @Override
    public void add(Dvd newDVD) {

        newDVD.setId(nextID);
        getDVDMap().put(newDVD.getId(), newDVD);
        encode();
        nextID++;
    }

    @Override
    public void remove(int id) {
        getDVDMap().remove(id);
        encode();

    }

    @Override
    public List<Dvd> listAll() {
        List<Dvd> dvdList = new ArrayList<>();
        dvdList.addAll(getDVDMap().values());
        return dvdList;

    }

    @Override
    public Dvd getById(int id) {
        return getDVDMap().get(id);
    }

    @Override
    public List<Dvd> getByTitle(String title) {
        List<Dvd> dvdList = new ArrayList<>();

        Set<Integer> keys = getDVDMap().keySet();
        for (Integer key : keys) {
            if (DVDMap.get(key).getTitle().equalsIgnoreCase(title)) {
                dvdList.add(DVDMap.get(key));
            }
        }
        return dvdList;
    }

    @Override
    public List<Dvd> getByRating(String rating) {
        List<Dvd> dvdList = new ArrayList<>();
        Set<Integer> keys = getDVDMap().keySet();

        for (Integer key : keys) {
            if (getDVDMap().get(key).getMpaaRating().equals(rating)) {
                dvdList.add(getDVDMap().get(key));
            }
        }
        return dvdList;
    }

    @Override
    public List<Dvd> getByStudio(String studio) {
        List<Dvd> dvdList = new ArrayList<>();
        Set<Integer> keys = getDVDMap().keySet();

        for (Integer key : keys) {
            if (getDVDMap().get(key).getMpaaRating().equals(studio)) {
                dvdList.add(getDVDMap().get(key));
            }
        }
        return dvdList;
    }


    public void update(Dvd dvd) {

        DVDMap.put(dvd.getId(), dvd);
        encode();

    }

}
