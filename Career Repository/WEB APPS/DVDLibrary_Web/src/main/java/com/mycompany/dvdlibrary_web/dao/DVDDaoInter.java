package com.mycompany.dvdlibrary_web.dao;

import com.mycompany.dvdlibrary_web.dto.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class DVDDaoInter {

    private final String NAMEFILE = "dvdLibrary.txt";
    private final String DELIMETER = "::::";

    private Map<Integer, DVD> DVDMap = new HashMap<>();
    private Integer nextID = 1;

    public DVDDaoInter() {

        try {
            List<DVD> dvdList = decode();
            dvdList
                    .stream()
                    .map((dvd) -> {
                        DVDMap.put(dvd.getDVDID(), dvd);
                        return dvd;
                    })
                    .filter((dvd) -> (dvd.getDVDID() > nextID))
                    .forEach((dvd) -> {
                        nextID = dvd.getDVDID() + 1;
                    });
            nextID++;
        } catch (Exception ex) {

        }

    }

    public List<DVD> listAllDVD() {
        List<DVD> dvdList = new ArrayList<>();
        dvdList.addAll(DVDMap.values());
        return dvdList;
    }

    public Set<Integer> getkeySet() {
        return DVDMap.keySet();
    }
//    @Override
//    public DVD createDVD(DVD newDVD) {
//
//        newDVD.setDVDID(nextID);
//        getDVDMap().put(newDVD.getDVDID(), newDVD);
//        encode();
//        nextID++;
//        return newDVD;
//
//    }
//    public void removeDVD(int id) {
//
//        listAllDVD()
//                .stream()
//                .filter(s -> s.getDVDID() == id)
//                .forEach(s -> getDVDMap().remove(id));
//        encode();
//
//    }
//    @Override
//    public DVD updateDVD(DVD thisDVD) {
//
//        getDVDMap().put(thisDVD.getDVDID(), thisDVD);
//        encode();
//        return thisDVD;
//    }
//

    public Map<Integer, DVD> getDVDMap() {
        return DVDMap;
    }
//
//    @Override
//    public List<DVD> getDVDList(String dvdTitle) {
//        List<DVD> dvdList = new ArrayList<>();
//
//        Set<Integer> keys = getDVDMap().keySet();
//        keys.stream()
//                .filter((key) -> (DVDMap.get(key).getTitle().equalsIgnoreCase(dvdTitle)))
//                .forEach((key) -> {
//                    dvdList.add(DVDMap.get(key));
//                });
//        return dvdList;
//    }
//    
//    @Override
//    public DVD getDVD(int dvdId) {
//        return getDVDMap().get(dvdId);
//    }

//    @Override
//    public DVD getDVD(String dvdTitle) {
//
//        return DVDMap.values()
//                .stream()
//                .filter(d -> d.getTitle().equalsIgnoreCase(dvdTitle))
//                .findFirst()
//                .get();
//
//    }
//    public List getDVDForYear(int date) {
//        return listAllDVD()
//                .stream()
//                .filter(a -> Integer.parseInt(a.getReleaseDate()) > date)
//                .collect(Collectors.toList());
//    }
//    public List<DVD> getDVDByRating(String rating) {
//
//        return listAllDVD()
//                .stream()
//                .filter(a -> (a.getMpaaRating().equalsIgnoreCase(rating)))
//                .collect(Collectors.toList());
//    }
//    public double getAverageAge() {
//        return listAllDVD()
//                .stream()
//                .mapToLong(a -> Integer.parseInt(a.getReleaseDate()))
//                .average().getAsDouble();
//
//    }
//    public int[] getNewestAndOldest() {
//
//        int[] minMax = new int[2];
//
//        int max = listAllDVD()
//                .stream()
//                .mapToInt(a -> Integer.parseInt(a.getReleaseDate()))
//                .max().getAsInt();
//
//        int min = listAllDVD()
//                .stream()
//                .mapToInt(a -> Integer.parseInt(a.getReleaseDate()))
//                .min().getAsInt();
//
//        minMax[0] = min;
//        minMax[1] = max;
//        return minMax;
//
//    }
    private void encode() {
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(NAMEFILE));
            Set<Integer> iD = DVDMap.keySet();
            for (Integer key : iD) {
                String line = key + DELIMETER
                        + DVDMap.get(key).getTitle() + DELIMETER
                        + DVDMap.get(key).getReleaseDate() + DELIMETER
                        + DVDMap.get(key).getDirector() + DELIMETER
                        + DVDMap.get(key).getStudio() + DELIMETER
                        + DVDMap.get(key).getMpaaRating() + DELIMETER
                        + DVDMap.get(key).getUserNote() + DELIMETER;
                pw.println(line);
                pw.flush();
            }
        } catch (IOException ex) {
        }
    }

    private List<DVD> decode() {
        List<DVD> dvdList = new ArrayList<>();

        try {
            FileReader x = new FileReader(NAMEFILE);
            Scanner sc = new Scanner(new BufferedReader(x));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                DVD dvdHolder = new DVD();

                dvdHolder.setDVDID(Integer.parseInt(values[0]));
                dvdHolder.setTitle(values[1]);

                dvdHolder.setReleaseDate(values[2]);

                dvdHolder.setDirector(values[3]);
                dvdHolder.setStudio(values[4]);
                dvdHolder.setMpaaRating(values[5]);
                dvdHolder.setUserNote(values[6]);

                dvdList.add(dvdHolder);

            }

        } catch (FileNotFoundException ex) {

        }

        return dvdList;
    }

    //____________+++++++++++++++++_________________++++++++++++++++++++++++++
    public void add(DVD newDVD) {

        newDVD.setDVDID(nextID);
        getDVDMap().put(newDVD.getDVDID(), newDVD);
        encode();
        nextID++;

    }

    public void remove(int id) {
        listAllDVD()
                .stream()
                .filter(s -> s.getDVDID() == id)
                .forEach(s -> getDVDMap().remove(id));
        encode();
    }

    public List<DVD> listAll() {
        List<DVD> dvdList = new ArrayList<>();
        dvdList.addAll(getDVDMap().values());
        return dvdList;
    }

    public DVD getById(int id) {
        return getDVDMap().get(id);

    }

    public List<DVD> getByTitle(String title) {

        return DVDMap.values()
                .stream()
                .filter(d -> d.getTitle().equalsIgnoreCase(title))
                .collect(Collectors.toList());
    }

    public List<DVD> getByRating(String rating) {
        return listAllDVD()
                .stream()
                .filter(a -> (a.getMpaaRating().equalsIgnoreCase(rating)))
                .collect(Collectors.toList());

    }

    public List<DVD> getByStudio(String studio) {
        return listAllDVD()
                .stream()
                .filter(a -> (a.getStudio().equalsIgnoreCase(studio)))
                .collect(Collectors.toList());

    }

    public void update(DVD dvd) {
        getDVDMap().put(dvd.getDVDID(), dvd);
        encode();

    }

}
