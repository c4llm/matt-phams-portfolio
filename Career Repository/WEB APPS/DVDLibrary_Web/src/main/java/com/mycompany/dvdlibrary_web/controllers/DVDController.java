/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary_web.controllers;

import com.mycompany.dvdlibrary_web.dao.DVDDaoInter;
import com.mycompany.dvdlibrary_web.dto.DVD;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/dvd")
public class DVDController {

    private DVDDaoInter dao = new DVDDaoInter();

    @Inject
    public DVDController(DVDDaoInter dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@ModelAttribute DVD dvd, BindingResult bindingResult, Model model) {

        dao.add(dvd);

        return "redirect:/";
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Integer dvdId) {

        dao.remove(dvdId);

        return "redirect:/";

    }

    @RequestMapping(value = "/view/{id}")
    public String show(@PathVariable("id") Integer dvdId, Model model) {

        DVD dvd = dao.getById(dvdId);

        model.addAttribute("dvd", dvd);

        return "viewContact";

    }

    @RequestMapping(value = "/showEdit/{id}")
    public String showEdit(@PathVariable("id") Integer dvdId, Model model) {

        DVD dvd = dao.getById(dvdId);

        model.addAttribute("dvd", dvd);

        return "showEdit";

    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String edit(@ModelAttribute DVD dvd, @PathVariable("id") Integer DVDID, BindingResult bindingResult, Model model) {
        dvd.setDVDID(DVDID);
        dao.update(dvd);

        return "redirect:/";
    }

    @RequestMapping(value = "/searchName/{id}", method = RequestMethod.POST)
    public String searchName(@RequestParam(value = "title") String title, Model model) {

        List<DVD> dvds = new ArrayList<>();

        dvds = dao.getByTitle(title);
        model.addAttribute("dvd", dvds);

        return "redirect:/";
    }

}
