/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary_web.controllers;

import com.mycompany.dvdlibrary_web.dao.DVDDaoInter;
import com.mycompany.dvdlibrary_web.dto.DVD;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
public class SearchController {

    private DVDDaoInter dao = new DVDDaoInter();

    @Inject
    public SearchController(DVDDaoInter dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search() {

        return "search";
    }

    @RequestMapping(value = "/search/searchName", method = RequestMethod.POST)
    public String searchName(@RequestParam(value = "search") String search, @RequestParam(value = "optradio") String value, Model model) {

        List<DVD> dvds = new ArrayList<>();

        switch (value) {

            case "title":
                dvds = dao.getByTitle(search);
                break;
            case "rating":
                dvds = dao.getByRating(search);
                break;
            case "studio":
                dvds = dao.getByStudio(search);
                break;

        }

        model.addAttribute("dvd", dvds);

        return "search";
    }

}
