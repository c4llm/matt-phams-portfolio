<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

        <style>
            .add{                    
                padding-left:60px;
                color: #ff4d4d;
                text-shadow: 1px 2px 2px #d6d6c2;

            }

        </style>

    </head>

    <body>
        <div class="container">

            <jsp:include page="/jsp/navbar.jsp"></jsp:include>


                <div class="col-md-6">


                    <h2>DVD Library</h2>

                    <table id="contactsTable" class="table">
                        <tr>
                            <th width="40%">DVD Title</th>
                            <th width="30%">Release Date</th>
                            <th width="15%"></th>
                            <th width="15%"></th>
                        </tr>

                    <c:forEach items="${dvdList}" var="dvd">
                        <tr>

                            <td>${dvd.title}</td>
                            <td>${dvd.releaseDate}</td>
                            <td><a href="${pageContext.request.contextPath}/dvd/showEdit/${dvd.DVDID}">Edit</a></td>
                            <td><a href="${pageContext.request.contextPath}/dvd/delete/${dvd.DVDID}">Remove</a></td>
                        </tr>
                    </c:forEach>

                </table>

            </div>

            <div class="col-md-6">

                <h2 class="add">Add New DVD</h2>

                <form:form class="form-horizontal" action="${pageContext.request.contextPath}/dvd/add" method="post">

                    <div class="form-group">
                        <label for="add-dvd-name" class="col-md-4 control-label">DVD Name:</label>
                        <div class="col-md-8">
                            <input type="text" name="title" class="form-control" id="add-dvd-name" placeholder="DVD Name" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="add-release-date" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8">
                            <input type="text" name="releaseDate" class="form-control" id="add-release-date" placeholder="Release Date" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="add-Rating" class="col-md-4 control-label">Rating:</label>
                        <div class="col-md-8">
                            <input type="text" name="mpaaRating" class="form-control" id="add-rating" placeholder="MPAA Rating" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="add-director" class="col-md-4 control-label">Director:</label>
                        <div class="col-md-8">
                            <input type="text" name="director" class="form-control" id="add-director" placeholder="Director" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-studio" class="col-md-4 control-label">Studio:</label>
                        <div class="col-md-8">
                            <input type="text" name="studio" class="form-control" id="add-studio" placeholder="Studio" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-note" class="col-md-4 control-label">Add Note:</label>
                        <div class="col-md-8">
                            <input type="text" name="userNote" class="form-control" id="add-note" placeholder="Add a Note" />
                        </div>
                    </div>

                    <input type="submit" class="btn btn-default pull-right" value="Create DVD" />


                </form:form>

            </div>


        </div>




        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

