<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <title>Search DVDs</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>

        <div class="container">

            <jsp:include page="/jsp/navbar.jsp"></jsp:include>
                <div class="col-md-6">

                    <h2 class="add">Search DVD</h2>

                <form:form class="form-horizontal" action="${pageContext.request.contextPath}/search/searchName" method="post">

                    <!-- search box for text input -->
                    <div class="form-group">

                        <div class="col-md-4">
                            <label for="search-dvd-name"></label>

                            <input type="text" name="search" class="form-control" id="search-dvd-name" placeholder="Search Criteria" />
                        </div>
                        <div class="col-md-8" style="vertical-align:center; margin-top:20px;">
                            <label class="radio-inline">
                                <input type="radio" name="optradio" value="title">Title Search
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optradio" value="rating">Rating Search
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optradio" value="studio">Studio Search
                            </label>
                        </div>
                    </div>

                    <input type="submit" name="searchButton" value="Search DVD">


                </form:form>

            </div>

            <div class="col-md-12">
                <table id="contactsTable" class="table">
                    <tr>
                        <th width="25%">DVD Title</th>
                        <th width="25%">Release Date</th>
                        <th width="25%">Prod Studio</th>
                        <th width="25%">MPAA Rating</th>
                    </tr>

                    <c:forEach items="${dvd}" var="dvd">
                        <tr>
                            <td>${dvd.title}</td>
                            <td>${dvd.releaseDate}</td>
                            <td>${dvd.studio}</td>
                            <td>${dvd.mpaaRating}</td>
                        </tr>
                    </c:forEach>

                </table>

            </div>

        </div>
    </body>
</html>