<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Search Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        
        <div class="container">
            <jsp:include page="navbar.jsp"></jsp:include>
                <div class="row">
                <form:form class="form-horizontal" action="${pageContext.request.contextPath}/search/searchby" method="post">
                    <div class="col-md-6">
                        <input type="text" name="searchTerm" class="form-control" id="search-term-by" placeholder="what to search?"/>
                    </div>
                    <div class="col-md-6">
                        <select name="dropdownname">
                            <option value="lastName" name="lastName">Last Name</option>
                            <option value="city" name="city">City</option>
                            <option value="state" name="state">State</option>
                            <option value="zip" name="zip">Zip</option>
                        </select>
                    </div>
                </form:form>

            </div>
            <div class="row" id="pushdown">
                <table width="100%">
                    <div class ="col-md-12">
                        <c:forEach items="${searchlist}" var="searchItem">
                            <tr>
                                <th width="20%">Name</th>
                                <th width="80%">Address</th>
                            </tr>
                            <tr>
                                <td width="20%">${searchItem.firstName} ${searchItem.lastName}</td>
                                <td width="80%">${searchItem.street}, </br>
                                    ${searchItem.city}, ${searchItem.state}, ${searchItem.zip}</td>
                            </tr>
                        </c:forEach>
                    </div> 
                </table>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

