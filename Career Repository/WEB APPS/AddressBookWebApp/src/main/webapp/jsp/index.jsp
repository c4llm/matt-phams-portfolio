<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <jsp:include page="navbar.jsp"></jsp:include>

                <div class="row">
                    <div class="col-md-6">
                        <table id="addressesTable" class="table">
                            <h2>Address Book</h2>
                            <tr>
                                <th width="40%">Name</th>
                                <th width="30%">State</th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                            </tr>
                        <c:forEach items="${addressList}" var="address">
                            <tr>
                                <td><a href="${pageContext.request.contextPath}/address/view/${address.id}">${address.firstName} ${address.lastName}</a></td>
                                <td>${address.state}</td>
                                <td><a href="${pageContext.request.contextPath}/address/showEdit/${address.id}"> Edit</a></td>
                                <td><a href="${pageContext.request.contextPath}/address/delete/${address.id}"> Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>

                </div>

                <div class ="col-md-6">
                    <h2>Add to Address Book</h2>
                    <form:form class="form-horizontal" action="${pageContext.request.contextPath}/address/add" method="post">
                        <div class="form-group">
                            <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8">
                                <input type="text" name="firstName" class="form-control" id="add-first-name" placeholder="First Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                            <div class="col-md-8">
                                <input type="text" name="lastName" class="form-control" id="add-last-name" placeholder="Last Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-street" class="col-md-4 control-label">Street:</label>
                            <div class="col-md-8">
                                <input type="text" name="street" class="form-control" id="add-street-name" placeholder="Street" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-city" class="col-md-4 control-label">City:</label>
                            <div class="col-md-8">
                                <input type="text" name="city" class="form-control" id="add-city" placeholder="City" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-state" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8">
                                <input type="text" name="state" class="form-control" id="add-state" placeholder="State" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-zip" class="col-md-4 control-label">Zip:</label>
                            <div class="col-md-8">
                                <input type="text" name="zip" class="form-control" id="add-zip" placeholder="Zip" />
                            </div>
                        </div>
                        <input type="submit" class="btn btn-default pull-right" value="Create Address" />
                    </form:form>
                </div>


            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

