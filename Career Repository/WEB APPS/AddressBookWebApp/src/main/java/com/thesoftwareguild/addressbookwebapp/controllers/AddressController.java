/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbookwebapp.controllers;

import com.thesoftwareguild.addressbookwebapp.dao.AddressDao;
import com.thesoftwareguild.addressbookwebapp.model.Address;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/address")
public class AddressController {
    
    private AddressDao addressDao;
    
    @Inject
    public AddressController(AddressDao dao){
        this.addressDao = dao;
    }
    
    @RequestMapping(value="/add", method= RequestMethod.POST)
    public String add(@ModelAttribute Address address, BindingResult bindingResult, Model model){
        addressDao.add(address);
        return "redirect:/";
    }
    
    @RequestMapping(value="/view/{id}")
    public String view(@PathVariable("id") Integer id, Model model){
       Address address = addressDao.get(id);
       model.addAttribute("address", address);
       
       return "view";
       
    }
    
    @RequestMapping(value="/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        addressDao.delete(id);
        return "redirect:/";
    }
    
    //(@RequestParam(value="nameinjsp") String value)
    
    @RequestMapping(value="/showEdit/{id}")
    public String showEdit(@PathVariable("id") Integer id, Model model){
        Address address = addressDao.get(id);
        model.addAttribute("address", address);
        return "showEdit";
    }
    
    @RequestMapping(value="edit/{id}" , method = RequestMethod.POST)
    public String edit(@Valid @ModelAttribute Address address,  BindingResult bindingResult, @PathVariable("id") Integer id, Model model){
        
        if(bindingResult.hasErrors()){
            model.addAttribute("address", address);
            return "showEdit";
        }
        addressDao.update(address);
        return "redirect:/";
    }
    
}
