/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbookwebapp.dao;

import com.thesoftwareguild.addressbookwebapp.model.Address;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface AddressDao {
    
    public Address add(Address address);
    public Address get(Integer id);
    public void delete(Integer id);
    public void update(Address address);
    public List<Address> list();
    public List<Address> searchByName(String lastName);
    public List<Address> searchByCity(String city);
    public List<Address> searchByState(String state);
    public List<Address> searchByZip(String zip);
}
