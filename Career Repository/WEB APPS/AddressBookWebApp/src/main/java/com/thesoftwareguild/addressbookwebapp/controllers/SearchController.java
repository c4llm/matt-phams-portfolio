/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbookwebapp.controllers;

import com.thesoftwareguild.addressbookwebapp.dao.AddressDao;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/search")
public class SearchController {

    private AddressDao addressDao;

    @Inject
    public SearchController(AddressDao dao) {
        this.addressDao = dao;
    }

    @RequestMapping(value = "")
    public String search() {
        return "search";
    }
    
    @RequestMapping(value="/searchby", method= RequestMethod.POST)
    public String searchby(@RequestParam(value = "searchTerm") String value, @RequestParam(value = "dropdownname") String value2, Model model){
        List searchlist = new ArrayList();
        switch (value2) {
            case "lastName":
                searchlist = addressDao.searchByName(value);
                break;
            case "city":
                searchlist = addressDao.searchByCity(value);
                break;
            case "state":
                searchlist = addressDao.searchByState(value);
                break;
            case "zip":
                searchlist = addressDao.searchByZip(value);
                break;
        }

        model.addAttribute("searchlist", searchlist);

        return "search";
    }

}
