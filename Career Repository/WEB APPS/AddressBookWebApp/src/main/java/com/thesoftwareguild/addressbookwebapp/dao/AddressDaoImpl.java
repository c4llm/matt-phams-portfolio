/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbookwebapp.dao;

import com.thesoftwareguild.addressbookwebapp.model.Address;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressDaoImpl implements AddressDao{
    
    private static Integer nextId = 0;
    private Map<Integer, Address> addresses = new HashMap();

    @Override
    public Address add(Address address) {
        nextId++;
        address.setId(nextId);
        addresses.put(address.getId(), address);
        return address;

    }

    @Override
    public Address get(Integer id) {
        return addresses.get(id);
    }

    @Override
    public void delete(Integer id) {
        addresses.remove(id);
    }

    @Override
    public void update(Address address) {
        addresses.put(address.getId(), address);
    }

    @Override
    public List<Address> list() {
        return new ArrayList(addresses.values());
    }

    @Override
    public List<Address> searchByName(String lastName) {
        List<Address> addressList = list();
        return addressList
                .stream()
                .filter(address -> address.getLastName().equalsIgnoreCase(lastName))
                .collect(Collectors.toList());    }

    @Override
    public List<Address> searchByCity(String city) {
        List<Address> addressList = list();
        return addressList
                .stream()
                .filter(addresses -> addresses.getCity().equalsIgnoreCase(city))
                .collect(Collectors.toList());   
    }

    @Override
    public List<Address> searchByState(String state) {
    List<Address> addressList = list();
        return addressList
                .stream()
                .filter(addresses -> addresses.getState().equalsIgnoreCase(state))
                .sorted((Address A, Address B) -> A.getCity().compareTo(B.getCity()))
                .collect(Collectors.toList());  
    }

    @Override
    public List<Address> searchByZip(String zip) {
    List<Address> addressList = list();
        return addressList
                .stream()
                .filter(addresses -> addresses.getZip().equalsIgnoreCase(zip))
                .collect(Collectors.toList());    }
    
}
