/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dao;

import com.mycompany.vendingmachine.dto.Items;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class VendingMachineDao {

    final static String DELIMETER = "::";
    final static String FILENAME = "VendingMachineItems.txt";

    private static Integer nextId = 1;
    private Map<Integer, Items> inventory = new HashMap<>();
    
    

    public VendingMachineDao() {
        
        List <Items> items = decode();
        
        for (Items i : items) {
            inventory.put(i.getId(), i);
        }
        
    }

    public Map<Integer,Items> getMap(){
        return this.inventory;
    }

    public List<Items> list() {
        
        List <Items> itemList = new ArrayList<>();
        
        itemList.addAll(inventory.values());
        
        return itemList;
        
    }
    
       public void update(Items item) {
        
        inventory.put(item.getId(), item);
        encode();
        
    }
    
    private void encode() {
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter(FILENAME));
            
            List <Items> stockList = list();
            
            for (Items i : stockList) {
                
                String line = i.getId()+DELIMETER
                        + i.getName() + DELIMETER
                        + i.getPrice() + DELIMETER
                        + i.getStock();
                out.println(line);
                out.flush();
                
            }
            
        } catch(IOException ex) {
            
        }
        
    }
    
    private List<Items> decode() {
        
        List <Items> stockList = new ArrayList<>();
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));
            
            while (sc.hasNextLine()) {
                
                String thisLine = sc.nextLine();
                String[] values = thisLine.split(DELIMETER);
                
                Items stock = new Items();
                
                stock.setId(Integer.parseInt(values[0]));
                stock.setName(values[1]);
                stock.setPrice(Double.parseDouble(values[2]));
                stock.setStock(Integer.parseInt(values[3]));
                
                stockList.add(stock);
            }
            
        } catch (FileNotFoundException fnf) {
        
    }
        return stockList;
    }
    
}
