/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.dto;

/**
 *
 * @author apprentice
 */
public class Change {

    private int changeInPennies = 0;
    private int numberOfPennies = 0;
    private int numberOfNickels = 0;
    private int numberOfDimes = 0;
    private int numberOfQuarters = 0;

    public String change(int changeDue) {
        changeInPennies = changeDue;

        if (changeInPennies >= 25) {
            numberOfQuarters = changeInPennies / 25;
            changeInPennies = changeInPennies % 25;
        }
        if (changeInPennies >= 10) {
            numberOfDimes = changeInPennies / 10;
            changeInPennies = changeInPennies % 10;
        }
        if (changeInPennies >= 5) {
            numberOfNickels = changeInPennies / 5;
            changeInPennies = changeInPennies % 5;
        }
        numberOfPennies = changeInPennies;

        String pennies = Integer.toString(numberOfPennies);
        String nickels = Integer.toString(numberOfNickels);
        String dimes = Integer.toString(numberOfDimes);
        String quarters = Integer.toString(numberOfQuarters);

        return (pennies + " pennies," + nickels + " nickels" + dimes + " dimes" + quarters + " quarters");

    }
}
