/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachine.controller;

import com.mycompany.vendingmachine.dao.VendingMachineDao;
import com.mycompany.vendingmachine.dto.Change;
import com.mycompany.vendingmachine.dto.Items;
import com.mycompany.vendingmachine.ui.ConsoleIo;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class VendingMachineController {

    private final ConsoleIo consoleio = new ConsoleIo();
    private VendingMachineDao vendingmachinedao = new VendingMachineDao();
    private Change change = new Change();
    double totalDeposit = 0;
    double initialDeposit = 0;

    public void run() {
        
        boolean runAgain = true;

        while (runAgain) {
            
            printMenu();

            int menuSelection = consoleio.getNumberBetweenMaxAndMin("\nPlease select from the above choices", 1, 4);

            switch (menuSelection) {

                case 1:
                    insertMoney();
                    break;
                case 2:
                    displayItems();
                    break;
                case 3:
                    refundMoney();
                    break;
                case 4:
                    runAgain = false;
                    consoleio.print("COME GET SOME SNACKS SOON! ");

            }

        }
    }

    private void printMenu() {
        consoleio.print("\n1. insert money\n"
                + "2. display items and prices\n"
                + "3. refund money\n"
                + "4. exit");
    }

    private void insertMoney() {

        int itemChoice = 0;
        int itemStock = 0;
        double itemPrice = 0;
        String itemName = "";
        double changeDouble = 0;
        int changeDue = 0;

        initialDeposit = consoleio.getDouble("GIMME some MONEY!!!");
        totalDeposit = initialDeposit;
        Map<Integer, Items> inventory = vendingmachinedao.getMap();
        Set<Integer> keySet = inventory.keySet();
        boolean stillHungry = true;

        while (stillHungry) {

            displayItems();
            itemChoice = consoleio.getNumberBetweenMaxAndMin("Please choose an item based on their number: ", 1, 11);

            if (itemChoice == 11) {
                refundMoney();
                stillHungry = false;
            }

            for (Integer keys : keySet) {
                if (itemChoice == keys) {
                    itemPrice = inventory.get(keys).getPrice();
                    itemName = inventory.get(keys).getName();
                    itemStock = inventory.get(keys).getStock();
                }
            }

            if (totalDeposit < itemPrice) {
                totalDeposit += consoleio.getDouble("GIMME MO MONEY" + "your have deposited $" + totalDeposit + " and the item costs " + itemPrice);
            } else {
                stillHungry = false;
            }
        }

        changeDouble = (100 * (totalDeposit - itemPrice)) + .1;
        changeDue = (int) changeDouble;
        consoleio.print("GOOD JOB , YOU GOT SOME " + itemName);

        for (Integer stockKey : keySet) {
            if (itemChoice == stockKey) {
                inventory.get(stockKey).setStock(itemStock - 1);
                vendingmachinedao.update(inventory.get(stockKey));
            }
        }
        String duhStrang = change.change(changeDue);
        consoleio.print(duhStrang);

    }

    private void displayItems() {
        Map<Integer, Items> inventory = vendingmachinedao.getMap();
        Set<Integer> keySet = inventory.keySet();
        consoleio.print("\nVending Machine Items\n");
        if (inventory.get(1).getStock() > 0) {
            consoleio.print("1. Dorritos $2.00\n");
        }
        if (inventory.get(2).getStock() > 0) {
            consoleio.print("2. Red Bull $3.00\n");

        }
        if (inventory.get(3).getStock() > 0) {
            consoleio.print("3. Fanta $2.00\n");

        }
        if (inventory.get(4).getStock() > 0) {
            consoleio.print("4. Snickers $1.50\n");

        }
        if (inventory.get(5).getStock() > 0) {
            consoleio.print("5. Twix $1.50\n");

        }
        if (inventory.get(6).getStock() > 0) {
            consoleio.print("6. Certs $0.75\n");

        }
        if (inventory.get(7).getStock() > 0) {
            consoleio.print("7. Coca Cola $2.00\n");

        }
        if (inventory.get(8).getStock() > 0) {
            consoleio.print("8. MoonMist $0.50\n");

        }
        if (inventory.get(9).getStock() > 0) {
            consoleio.print("9. Mountain Dew $2.00\n");

        }
        if (inventory.get(10).getStock() > 0) {
            consoleio.print("10. Skittles $1.25\n");

        }
        consoleio.print("11. Refund");

    }

    private void refundMoney() {
        consoleio.print("You have deposited $" + totalDeposit + ". Here is your money back.");
        initialDeposit = 0;
        totalDeposit = 0;
    }
}
