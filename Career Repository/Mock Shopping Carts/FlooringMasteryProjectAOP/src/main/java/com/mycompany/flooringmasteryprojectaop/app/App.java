package com.mycompany.flooringmasteryprojectaop.app;

import com.mycompany.flooringmasteryprojectaop.controllers.FlooringController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        FlooringController controller =  ctx.getBean("lambController", FlooringController.class);

        controller.run();
//for git push
    }//END of Main
}// END of AppClass
