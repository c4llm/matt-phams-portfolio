package com.mycompany.flooringmasteryprojectaop.dto;


public class ConfigDto {
    
    private Boolean isTest=false;

    /**
     * @return the isTest
     */
    public Boolean getIsTest() {
        return isTest;
    }

    /**
     * @param isTest the isTest to set
     */
    public void setIsTest(Boolean isTest) {
        this.isTest = isTest;
    }

//for git push

}
