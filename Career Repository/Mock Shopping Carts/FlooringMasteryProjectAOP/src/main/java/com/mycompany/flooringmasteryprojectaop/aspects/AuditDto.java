/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectaop.aspects;

import java.util.Date;
//for git push
/**
 *
 * @author apprentice
 */
public class AuditDto {
    
    private String whatHappened;
    private Date date;
    private Integer orderNumber;

    /**
     * @return the whatHappened
     */
    public String getWhatHappened() {
        return whatHappened;
    }

    /**
     * @param whatHappened the whatHappened to set
     */
    public void setWhatHappened(String whatHappened) {
        this.whatHappened = whatHappened;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the orderNumber
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }
    
}
