/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectaop.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
//for git push
/**
 *
 * @author apprentice
 */
public class Timing {
    
    public Object timer(ProceedingJoinPoint jp){
        Object returnValue = null;
        
        try{
            Long start = System.currentTimeMillis();
            returnValue = jp.proceed();
            Long end = System.currentTimeMillis();
            System.out.println(jp.getSignature().getName() + " took " + (end - start) + " ms");
        }catch(Throwable ex){
            System.out.println("do stuff");
        }
        return returnValue;
    }
}
