package com.mycompany.flooringmasteryprojectaop.dto;


public class TaxesDto {
    private Double taxRate;
    private String state;
    

    /**
     * @return the taxRate
     */
    public Double getTaxRate() {
        return taxRate;
    }
//for git push
    /**
     * @param taxRate the taxRate to set
     */
    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

   
    
    
    
    
}
