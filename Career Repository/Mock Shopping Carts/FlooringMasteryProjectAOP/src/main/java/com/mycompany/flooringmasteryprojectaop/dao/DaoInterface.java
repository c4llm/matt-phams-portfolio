/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectaop.dao;

import com.mycompany.flooringmasteryprojectaop.dto.OrdersDto;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DaoInterface {
//for git push
    OrdersDto create(OrdersDto order);

    String createOrderFile(String createFILENAME);

    void delete(OrdersDto order);

    Map findFileMap();

    int getNextId();

    OrdersDto getOrderById(Integer orderNumber);

    List<OrdersDto> list();

    void setConfig(boolean test);

    void setOrderDate(String date);

    void update(OrdersDto order);
    
}
