/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectaop.aspects;

import com.mycompany.flooringmasteryprojectaop.dto.OrdersDto;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.aspectj.lang.JoinPoint;
//for git push
/**
 *
 * @author apprentice
 */
public class Audit {

    private String FileName = "audit.txt";
    private String Delimeter = "::";
    AuditDto dto = new AuditDto();
    Date today = new Date();
    OrdersDto orders = new OrdersDto();

    public void createLog(JoinPoint jp) {
        try {
            OrdersDto thing = (OrdersDto) jp.getArgs()[0];
            dto.setOrderNumber(thing.getOrderNumber());
            String happenings = jp.getSignature().getName();

            dto.setWhatHappened(happenings);
            dto.setDate(today);

            PrintWriter out = new PrintWriter(new FileWriter(FileName));
            String line = dto.getOrderNumber() + Delimeter 
                    + dto.getDate().toString() + Delimeter 
                    + dto.getWhatHappened();
            out.println(line);
            out.flush();

        } catch (IOException ex) {
            Logger.getLogger(Audit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
