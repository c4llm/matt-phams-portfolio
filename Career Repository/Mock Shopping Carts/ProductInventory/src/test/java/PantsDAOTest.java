/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import DAOs.PantsDao;
import DTOs.Pants;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class PantsDAOTest {

    PantsDao dao = new PantsDao();

    public PantsDAOTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createTest() {
        //10args
        boolean success = dao.create("pants1", "0", "10", "5", "10.00", "medium", "Urban Pipeline", "denim blue", "denim", "relaxed");

        Assert.assertTrue(success);
        Assert.assertEquals(1, dao.getPantsList().size());
    }

    @Test
    public void retrieveTest() {
        dao.create("pants1", "0", "10", "5", "10.00", "medium", "Urban Pipeline", "denim blue", "denim", "relaxed");

        Pants testPants = dao.retrieve("pants1");
        Assert.assertEquals("pants1", testPants.getProductId());
    }

    @Test
    public void updateTest1() {
        dao.create("pants1", "0", "10", "5", "10.00", "medium", "Urban Pipeline", "denim blue", "denim", "relaxed");

        boolean success = dao.update("pants1", "color", "red");

        Assert.assertTrue(success);
        Assert.assertEquals("red", dao.getPants("pants1").getColor());
    }

    @Test
    public void updateTest2() {
        dao.create("pants1", "0", "10", "5", "10.00", "medium", "Urban Pipeline", "denim blue", "denim", "relaxed");

        boolean success = dao.update("pants1", "color", "red", "size", "large");

        Assert.assertTrue(success);
        Assert.assertEquals("red", dao.getPants("pants1").getColor());
        Assert.assertEquals("large", dao.getPants("pants1").getSize());

    }

    @Test
    public void updateTest0() {
        dao.create("pants1", "0", "10", "5", "10.00", "medium", "Urban Pipeline", "denim blue", "denim", "relaxed");

        boolean success = dao.update("pants1", "color");

        Assert.assertFalse(success);
        Assert.assertEquals("denim blue", dao.getPants("pants1").getColor());
    }

    @Test
    public void deleteTest() {
        dao.create("pants1", "0", "10", "5", "10.00", "medium", "Urban Pipeline", "denim blue", "denim", "relaxed");

        dao.delete("pants1");
        
        Assert.assertEquals(0, dao.getPantsList().size());
    }
}
