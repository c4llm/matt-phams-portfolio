/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTOs;

/**
 *
 * @author apprentice
 */
public class Pants extends Product {

    protected String fitStyle;

    public Pants(String productId, String currentStockLevel, String maxStockLevel, String restockLevel, String costPerUnit, String size, String brand, String color, String material, String fitStyle) {
        this.productId = productId;
        this.currentStockLevel = Integer.parseInt(currentStockLevel);
        this.maxStockLevel = Integer.parseInt(maxStockLevel);
        this.restockLevel = Integer.parseInt(restockLevel);
        this.costPerUnit = Double.parseDouble(costPerUnit);
        this.size = size;
        this.brand = brand;
        this.color = color;
        this.material = material;
        this.fitStyle = fitStyle;
    }
    
    public Pants(){
    }

    /**
     * @return the fitStyle
     */
    public String getFitStyle() {
        return fitStyle;
    }

    /**
     * @param fitStyle the fitStyle to set
     */
    public void setFitStyle(String fitStyle) {
        this.fitStyle = fitStyle;
    }
}
