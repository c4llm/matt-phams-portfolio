/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTOs;

/**
 *
 * @author apprentice
 */
public class Shirts extends Product{
    
    private String sleeveLength;
    private String collar;
    private String designPattern;

    /**
     * @return the sleeveLength
     */
    public String getSleeveLength() {
        return sleeveLength;
    }

    /**
     * @param sleeveLength the sleeveLength to set
     */
    public void setSleeveLength(String sleeveLength) {
        this.sleeveLength = sleeveLength;
    }

    /**
     * @return the collar
     */
    public String getCollar() {
        return collar;
    }

    /**
     * @param collar the collar to set
     */
    public void setCollar(String collar) {
        this.collar = collar;
    }

    /**
     * @return the designPattern
     */
    public String getDesignPattern() {
        return designPattern;
    }

    /**
     * @param designPattern the designPattern to set
     */
    public void setDesignPattern(String designPattern) {
        this.designPattern = designPattern;
    }    
}
