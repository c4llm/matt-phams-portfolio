/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import java.util.List;

/**
 *
 * @author apprentice
 */
public interface DAO_Interface {
    
    public boolean create(String... args);
    
    public <T> T retrieve(String... args);
    
    public boolean update(String... args);
    
    public void delete(String... args);
    
    public void encode();
    
    public List decode();
}
