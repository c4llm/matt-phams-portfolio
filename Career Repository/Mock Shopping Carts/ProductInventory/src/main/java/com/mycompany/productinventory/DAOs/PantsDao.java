/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.DAOs;

import com.mycompany.productinventory.DTOs.Pants;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class PantsDao implements DAO_Interface {
    
    List<Pants> pantsList = new ArrayList<>();

    @Override
    public boolean create(String... args) {
        try{
        pantsList.add(new Pants(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]));
        return true;
        } catch(Exception ex){
        return false;
        }
    }

    @Override
    public <T> T retrieve(String... args) {
        return (T) getPants(args[0]);
    }

@Override
    public boolean update(String... args) {
        
        boolean succedded = true;
        String pid = args[0];
        
        int repeat = (args.length - 1) / 2;
        int updateFieldInt = 1;
        int newValueInt = 2;
        
        for (int i = 0; i < repeat; i++) {
            String updateField = args[updateFieldInt];
            String newValue = args[newValueInt];
            switch (updateField) {
                case "currentStockLevel":
                    getPants(pid).setCurrentStockLevel(Integer.parseInt(newValue));
                    break;
                case "maxStockLevel":
                    getPants(pid).setMaxStockLevel(Integer.parseInt(newValue));
                    break;
                case "restockLevel":
                    getPants(pid).setRestockLevel(Integer.parseInt(newValue));
                    break;
                case "costPerUnit":
                    getPants(pid).setCostPerUnit(Double.parseDouble(newValue));
                    break;
                case "size":
                    getPants(pid).setSize(newValue);
                    break;
                case "brand":
                    getPants(pid).setBrand(newValue);
                    break;
                case "color":
                    getPants(pid).setColor(newValue);
                    break;
                case "material":
                    getPants(pid).setMaterial(newValue);
                    break;
                case "fitStyle":
                    getPants(pid).setFitStyle(newValue);
                    break;
                default:
                    succedded = false;
                    break;
                    
            }
            updateFieldInt += 2;
            newValueInt += 2;
        }
        return succedded;
    }

    @Override
    public void delete(String... args) {
        String pid = args[0];
        pantsList.remove(getPants(pid));
    }

    @Override
    public void encode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List decode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Pants getPants(String productId){
        for (Pants current: pantsList){
            if(current.getProductId().equals(productId))
                return current;
        }
        return null;
    }
    
    public List getPantsList(){
        return pantsList;
    }
}
