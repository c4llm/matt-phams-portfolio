/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.DTOs;



/**
 *
 * @author apprentice
 */
public class Product {
    
   protected String productId;
   protected int currentStockLevel;
   protected int maxStockLevel;
   protected int restockLevel;
   protected double costPerUnit;
   protected String size;
   protected String brand;
   protected String color;
   protected String material;
   

    /**
     * @return the currentStockLevel
     */
    public int getCurrentStockLevel() {
        return currentStockLevel;
    }

    /**
     * @param currentStockLevel the currentStockLevel to set
     */
    public void setCurrentStockLevel(int currentStockLevel) {
        this.currentStockLevel = currentStockLevel;
    }

    /**
     * @return the maxStockLevel
     */
    public int getMaxStockLevel() {
        return maxStockLevel;
    }

    /**
     * @param maxStockLevel the maxStockLevel to set
     */
    public void setMaxStockLevel(int maxStockLevel) {
        this.maxStockLevel = maxStockLevel;
    }

    /**
     * @return the restockLevel
     */
    public int getRestockLevel() {
        return restockLevel;
    }

    /**
     * @param restockLevel the restockLevel to set
     */
    public void setRestockLevel(int restockLevel) {
        this.restockLevel = restockLevel;
    }

    /**
     * @return the costPerUnit
     */
    public double getCostPerUnit() {
        return costPerUnit;
    }

    /**
     * @param costPerUnit the costPerUnit to set
     */
    public void setCostPerUnit(double costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return the material
     */
    public String getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(String material) {
        this.material = material;
    }
}
