package com.mycompany.windowmaster;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class WindowMaster {

    public static void main(String[] args) {

        final float MAX_HEIGHT = 25.5f;
        final float MAX_WIDTH = 18.75f;
        final float MINIMUM = 1f;

        String userInputHeight = "";
        String userInputWidth = "";
        String userInputglassCost = "";
        String userInputtrimCost = "";

        float areaOfWindow;
        float cost;
        float perimeterOfWindow;
        float glassCost;
        float trimCost;
        float height;
        float width;
        boolean appropriateSize = false;

        Scanner sc = new Scanner(System.in);

//        System.out.println("Please enter wind;ow height:");
//        userInputHeight = sc.nextLine();
//        height = Float.parseFloat(userInputHeight);
//        while (appropriateSize = false) {
//            if (height > MINIMUM && height < MAX_HEIGHT) {
//                appropriateSize = true;
//            } else {
//                System.out.println("Invalid height, please enter a height between 1 and 25.5");
//                userInputHeight = sc.nextLine();
//                height = Float.parseFloat(userInputHeight);
//            }
//        }
        height = getUserInputNow("Please enter window height:", MAX_HEIGHT, MINIMUM);

//        System.out.println("Please enter window width:");
//        userInputWidth = sc.nextLine();
//        width = Float.parseFloat(userInputWidth);
//        while (appropriateSize = false) {
//            if (width > MINIMUM && width < MAX_WIDTH) {
//                appropriateSize = true;
//            } else {
//                System.out.println("Invalid width, please enter a width between 1 and 25.5");
//                userInputWidth = sc.nextLine();
//                width = Float.parseFloat(userInputWidth);
//            }
//        }
        width = getUserInputNow("Please enter window width: ", MAX_WIDTH, MINIMUM);

//        System.out.println("Please cost of trim:");
//        userInputtrimCost = sc.nextLine();
//        trimCost = Float.parseFloat(userInputtrimCost);
        trimCost = getUserInputNow("Please enter cost of trim= $", Float.POSITIVE_INFINITY, 0);

//        System.out.println("Please enter cost of glass:");
//        userInputglassCost = sc.nextLine();
//        glassCost = Float.parseFloat(userInputglassCost);
        glassCost = getUserInputNow("Please enter cost of glass= $", Float.POSITIVE_INFINITY, 0);

        //calculate area of window
        areaOfWindow = height * width;

        //calculate perimeter of window
        perimeterOfWindow = 2 * (height + width);

        //calculate total cost
        cost = ((glassCost * areaOfWindow) + (trimCost * perimeterOfWindow));

        //print out cost
        System.out.println("Window height: " + height);
        System.out.println("Window width: " + width);
        System.out.println("Window area: " + areaOfWindow);
        System.out.println("Window perimeter = " + perimeterOfWindow);
        System.out.println("Price of trim = $" + trimCost);
        System.out.println("Price of glass = $" + glassCost);
        System.out.println("Total cost: $" + cost);
    }

    public static float getUserInputNow(String askQuestion, float maximumThreshold, float minimumThreshold) {
        boolean validate = true;
        Scanner sc = new Scanner(System.in);
        String userInput = "";
        float userFloat = 0;
       
        while (validate) {
            System.out.print(askQuestion);
            userInput = sc.nextLine();
            userFloat = Float.parseFloat(userInput);
            if (maximumThreshold < userFloat || minimumThreshold > userFloat) {
                System.out.println("Reinput variable between" + minimumThreshold + "&" + maximumThreshold);
                validate = true;
            }
            if (userFloat < maximumThreshold && userFloat > minimumThreshold) {
                validate = false;
            }}

            return userFloat;
        }

    } 
