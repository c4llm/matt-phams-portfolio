/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryproject.dao;

import com.mycompany.flooringmasteryproject.dto.OrdersDto;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DaoInterface {

    OrdersDto create(OrdersDto order);

    String createOrderFile(String createFILENAME);

    Map<Integer, OrdersDto> decode();

    void delete(OrdersDto order);

    void encode();

    Map findFileMap();

    int getNextId();

    OrdersDto getOrderById(Integer orderNumber);

    List<OrdersDto> list();

    void setConfig(boolean test);

    void setOrderDate(String date);

    void update(OrdersDto order);
    
}
