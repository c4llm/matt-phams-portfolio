package com.mycompany.flooringmasteryproject.app;

import com.mycompany.flooringmasteryproject.controllers.FlooringController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        FlooringController controller =  ctx.getBean("lambController", FlooringController.class);

        controller.run();

    }//END of Main
}// END of AppClass
