/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.warmupbballhashmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class WarmUpBballHashmap {

    public static void main(String[] args) {
        Map<String, Integer> teamStats = new HashMap<>();

        teamStats.put("Smith", 23);
        teamStats.put("Jones", 12);
        teamStats.put("Jordan", 45);
        teamStats.put("Pippen", 32);
        teamStats.put("Kerr", 15);

        Set<String> key = teamStats.keySet();

        double total = 0;
        for (String k : key) {
            Integer points = teamStats.get(k);
            System.out.println(k + " - " + points);
            total += points;
        }
        System.out.println("Team average: "+ total / teamStats.size());

    }
}
