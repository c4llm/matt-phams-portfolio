/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.linkedlist;

/**
 *
 * @author apprentice
 */
public class Link {

    public String bookName;
    public int millionsSold;
    
    public Link next;
    
    public Link(String bookName, int millionsSold){
        this.bookName = bookName;
        this.millionsSold = millionsSold;
    }
    
    public void display(){
        System.out.println(bookName + ": " + millionsSold + ",000,000");
    }
    
    public String toString(){
        return bookName;
    }
    
    public static void main(String[] args) {
         
    }
     
}

class LinkList{
    public Link firstLink;
    
    LinkList(){
        firstLink = null;
    }
    
    public boolean isEmpty(){
        return(firstLink == null); 
    }
    
    
}
 