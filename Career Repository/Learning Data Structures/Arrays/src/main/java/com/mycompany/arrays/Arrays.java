/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.arrays;

/**
 *
 * @author apprentice
 */
public class Arrays {
    public static void main(String[] args) {
        
        int[] teamScores;
        
        teamScores = new int [10];
        
        teamScores[0]= 11;
        teamScores[1]=2;
        teamScores[2]= 1;
        teamScores[3]= 3;
        teamScores[4]= 6;
        teamScores[5]= 8;
        teamScores[6]= 21;
        
        System.out.println(teamScores[4]);
        
        for ( int i = 0; i < teamScores.length; i++ ){
            
            int x = teamScores[i];
            System.out.println(x);
            
        }
        
        int[] intValues = new int[10];
        for (int i =0; i < intValues.length; i++) {
        
            intValues[i] = i + 34;
            
            System.out.println(intValues[i]);
        }
        
        //enhanced for loop takes all values in array and puts it to x ( x has to be same data type)
        for (int x : teamScores){
            System.out.println(x);
            if (x>0){
                //do something
            }else {
                //do something
            }
        }
        teamScores = getArray(teamScores);
        
        String[] stringArray = new String[10];
        
        stringArray[0]="yo";
        stringArray[1]="ho";
        stringArray[2]="lo";
        
        for (String y : stringArray) {
            System.out.println(y);
        }
        
        for (int i=0; i < stringArray.length; i++){
            String y = stringArray[i];
            System.out.println(y);
        }
    }
    
    public static int[] getArray(int[] ints){
        
        
    
    return ints;
    }
}
