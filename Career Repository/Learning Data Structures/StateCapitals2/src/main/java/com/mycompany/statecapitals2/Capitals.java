/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.statecapitals2;

/**
 *
 * @author apprentice
 */
public class Capitals {
    
    private String name;
    private int population;
    private double squareMileage;
    
    public Capitals(String name, int population, double squareMileage){
        this.name = name;
        this.population = population;
        this.squareMileage = squareMileage;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the population
     */
    public int getPopulation() {
        return population;
    }

    /**
     * @param population the population to set
     */
    public void setPopulation(int population) {
        this.population = population;
    }

    /**
     * @return the squareMileage
     */
    public double getSquareMileage() {
        return squareMileage;
    }

    /**
     * @param squareMileage the squareMileage to set
     */
    public void setSquareMileage(double squareMileage) {
        this.squareMileage = squareMileage;
    }
}
