/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.statecapitals2;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class StateCapital2 {

    private static ConsoleIo consoleio = new ConsoleIo();

    static Capitals montgomery = new Capitals("Montgomery", 205000, 155.4);
    static Capitals juneau = new Capitals("Juneau", 31000, 2716.7);
    static Capitals phoenix = new Capitals("Phoenix", 1450000, 474.9);
    static Capitals littleRock = new Capitals("Little Rock", 193500, 116.2);
    static Capitals sacramento = new Capitals("Sacramento", 466000, 97.2);
    static Capitals denver = new Capitals("Denver", 600000, 153.4);
    static Capitals hartford = new Capitals("Hartford", 124775, 17.3);
    static Capitals dover = new Capitals("Dover", 36000, 22.4);
    static Capitals tallahassee = new Capitals("Tallahasse", 600000, 61.4);
    static Capitals atlanta = new Capitals("Atlanta", 420000, 131.7);
    static Capitals honolulu = new Capitals("Honolulu", 337000, 85.7);
    static Capitals boise = new Capitals("Boise", 200000, 63.8);
    static Capitals springfield = new Capitals("Springfield", 115000, 54.0);
    static Capitals indianapolis = new Capitals("Indianapolis", 829000, 361.5);
    static Capitals desMoines = new Capitals("Des Moines", 200000, 75.8);
    static Capitals topeka = new Capitals("Topeka", 127000, 56.0);
    static Capitals frankfort = new Capitals("Frankfort", 25000, 14.7);
    static Capitals batonRouge = new Capitals("Baton Rouge", 230000, 76.8);
    static Capitals augusta = new Capitals("Augusta", 20000, 55.4);
    static Capitals annapolis = new Capitals("Annapolis", 39000, 6.73);
    static Capitals boston = new Capitals("Boston", 618000, 48.4);
    static Capitals lansing = new Capitals("Lansing", 114297, 35.0);
    static Capitals saintPaul = new Capitals("Saint Paul", 285000, 52.8);
    static Capitals jackson = new Capitals("Jackson", 173000, 104.9);
    static Capitals jeffersonCity = new Capitals("Jefferson City", 43000, 27.3);
    static Capitals helena = new Capitals("Helena", 28000, 14.0);
    static Capitals lincoln = new Capitals("Lincoln", 258000, 74.6);
    static Capitals carsonCity = new Capitals("Carson City", 55000, 143.4);
    static Capitals concord = new Capitals("Concord", 43000, 64.3);
    static Capitals trenton = new Capitals("Trenton", 85000, 7.66);
    static Capitals santaFe = new Capitals("Santa Fe", 76000, 37.3);
    static Capitals albany = new Capitals("Albany", 98000, 21.4);
    static Capitals raleigh = new Capitals("Raleigh", 400000, 114.6);
    static Capitals bismarck = new Capitals("Bismarck", 61000, 26.9);
    static Capitals columbus = new Capitals("Columbus", 824000, 210.3);
    static Capitals oklahomaCity = new Capitals("Oklahoma City", 580000, 607.0);
    static Capitals salem = new Capitals("Salem", 155000, 45.7);
    static Capitals harrisburg = new Capitals("Harrisburg", 50000, 8.11);
    static Capitals providence = new Capitals("Providence", 178000, 18.5);
    static Capitals columbia = new Capitals("Columbia", 132000, 125.2);
    static Capitals pierre = new Capitals("Pierre", 14000, 13.0);
    static Capitals nashville = new Capitals("Nashville", 636000, 473.3);
    static Capitals austin = new Capitals("Austin", 790000, 251.5);
    static Capitals saltLakeCity = new Capitals("Salt Lake City", 186000, 109.1);
    static Capitals montpelier = new Capitals("Montpelier", 8000, 10.2);
    static Capitals richmond = new Capitals("Richmond", 204000, 60.1);
    static Capitals olympia = new Capitals("Olympia", 46000, 16.7);
    static Capitals charleston = new Capitals("Charleston", 51400, 31.6);
    static Capitals madison = new Capitals("Madison", 233000, 68.7);
    static Capitals cheyenne = new Capitals("Cheyenne", 60000, 21.1);

    public static void main(String[] args) {

        Map<String, Capitals> capitals = new HashMap<>();

        capitals.put("Alabama", montgomery);
        capitals.put("Alaska", juneau);
        capitals.put("Arizona", phoenix);
        capitals.put("Arkansas", littleRock);
        capitals.put("California", sacramento);
        capitals.put("Colorado", denver);
        capitals.put("Connecticut", hartford);
        capitals.put("Delaware", dover);
        capitals.put("Florida", tallahassee);
        capitals.put("Georgia", atlanta);
        capitals.put("Hawaii", honolulu);
        capitals.put("Idaho", boise);
        capitals.put("Illinois", springfield);
        capitals.put("Indiana", indianapolis);
        capitals.put("Iowa", desMoines);
        capitals.put("Kansas", topeka);
        capitals.put("Kentucky", frankfort);
        capitals.put("Louisiana", batonRouge);
        capitals.put("Maine", augusta);
        capitals.put("Maryland", annapolis);
        capitals.put("Massachusetts", boston);
        capitals.put("Michigan", lansing);
        capitals.put("Minnesota", saintPaul);
        capitals.put("Mississippi", jackson);
        capitals.put("Missouri", jeffersonCity);
        capitals.put("Montana", helena);
        capitals.put("Nebraska", lincoln);
        capitals.put("Nevada", carsonCity);
        capitals.put("New Hampshire", concord);
        capitals.put("New Jersey", trenton);
        capitals.put("New Mexico", santaFe);
        capitals.put("New York", albany);
        capitals.put("North Carolina", raleigh);
        capitals.put("North Dakota", bismarck);
        capitals.put("Ohio", columbus);
        capitals.put("Oklahoma", oklahomaCity);
        capitals.put("Oregon", salem);
        capitals.put("Pennsylvania", harrisburg);
        capitals.put("Rhode Island", providence);
        capitals.put("South Carolina", columbia);
        capitals.put("South Dakota", pierre);
        capitals.put("Tennessee", nashville);
        capitals.put("Texas", austin);
        capitals.put("Utah", saltLakeCity);
        capitals.put("Vermont", montpelier);
        capitals.put("Virginia", richmond);
        capitals.put("Washington", olympia);
        capitals.put("West Virginia", charleston);
        capitals.put("Wisconsin", madison);
        capitals.put("Wyoming", cheyenne);

        Set<String> stateKeys = capitals.keySet();

        for (String key : stateKeys) {
            String name = capitals.get(key).getName();
            int pop = capitals.get(key).getPopulation();
            double mileage = capitals.get(key).getSquareMileage();
            consoleio.print(key + " - " + name + " | Pop: " + pop + " | Area: " + mileage);

        }

        int limit = consoleio.getInteger("Please set a lower limit for your city populations: ");

        for (String key : stateKeys) {
            String name = capitals.get(key).getName();
            int pop = capitals.get(key).getPopulation();
            double mileage = capitals.get(key).getSquareMileage();
            if (pop < limit) {
                continue;
            }
            consoleio.print(key + " - " + name + " | Pop: " + pop + " | Area: " + mileage);

        }
    }

}
