/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stacksandqueues;

import java.util.NoSuchElementException;

/**
 *
 * @author apprentice
 */
public class StackImpl<T> implements Stack<T> {

    final int INITIAL_SIZE = 5;
    
    private T[] data;
    private int total;
    
    public StackImpl(){
        
        data = (T[]) new Object[INITIAL_SIZE];
    }
    
    private void resize(int size){
        
        T[] temp = (T[]) new Object[size];
        
        System.arraycopy(data, 0, temp, 0, total);
        
        data = temp;
        
        
    }
    
    @Override
    public void push(T object) {

        if(data.length == total){
            resize(data.length*2);
        } 
        data[total++] = object;
    }

    
    @Override
    public T pop() {

        if(total == 0){
            throw new NoSuchElementException();
        }
        
        T item = data[--total];
        
        data[total] = null;
        
        if(total > 0 && total == data.length/2){
            resize(data.length/ 2);
        }
        
        return item;
    }

    @Override
    public boolean isEmpty() {

        return (total == 0);
    }

    @Override
    public int size() {
        return total;
    }
    
    
}
