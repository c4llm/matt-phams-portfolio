/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stacksandqueues;

import com.thesoftwareguild.queue.Queue;



/**
 *
 * @author apprentice
 */
public class App2 {
    public static void main(String[] args) {
        
        Queue<String> myQueue = new QueueImpl<String>();
        
        myQueue.enqueue("First string");
        myQueue.enqueue("second");
        myQueue.enqueue("third");
        myQueue.enqueue("fourth");
        myQueue.enqueue("fifth");
        myQueue.enqueue("sixth");
        
        System.out.println(myQueue.size());
        
        String dq = myQueue.dequeue();
        
        if(dq.equals("First string")){
            System.out.println("WORKED!");
            
        String dq2 = myQueue.dequeue();
        
        if(dq2.equals("second")){
            System.out.println("WORKED YO");
        }
        
        String dq3 = myQueue.dequeue();
        if(dq3.equals("third")){
            System.out.println("AGAIN!");
        }
        }
             
    }
}
