/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stacksandqueues;

/**
 *
 * @author apprentice
 */
public class App {
    
    public static void main(String[] args) {
        
        Stack<String> myStack = new StackImpl();
        
        myStack.push("First string");
        myStack.push("Second string");
        myStack.push("third");
        myStack.push("fourth");
        myStack.push("fifth");
        myStack.push("sixth");
        
        
        String popped = myStack.pop();
        
        if(popped.equals("sixth")){
            System.out.println("Worked");
        }
        
        
        
    }
}
