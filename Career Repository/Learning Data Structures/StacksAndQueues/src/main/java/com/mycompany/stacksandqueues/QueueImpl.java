/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stacksandqueues;

import com.thesoftwareguild.queue.LinkedListQueue;
import com.thesoftwareguild.queue.Queue;
import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class QueueImpl<T> implements Queue<T> {

    private T[] data;
    private int total;
    private int head;
    private int tail;
    private final int INITIAL_SIZE = 5;
    private int i = 0;
    private int size;

    public QueueImpl() {

        data = (T[]) new Object[INITIAL_SIZE];
       // head = data[0];

    }

    private void resize(int size) {

        T[] temp = (T[]) new Object[size];

        System.arraycopy(data, 0, temp, 0, total);

        data = temp;
    }

    public void enqueue(T element) {

        if (size <= INITIAL_SIZE) {
            size = INITIAL_SIZE;
        }
        head = this.head;
        tail = size - 1;

        if (total == size) {
            resize(data.length * 2);
            size = data.length;
            tail = size / 2;
            head = 0;
        }

        data[total++] = element;

        if (total < size && tail == size - 1) {
            tail = 0;
        }

    }

    public T dequeue() {

        int pop = head;
        i++;
        head = i;
        
        data[i - 1] = null;
        if (size <= INITIAL_SIZE) {
            size = INITIAL_SIZE;
        }

        if (i == size) {
            i = 0;
            head = i;
        }

        return data[head];
 
    }

    @Override
    public Boolean isEmpty() {
        return true;
    }

    @Override
    public int size() {
        return data.length;

    }

    @Override
    public T peek() {
        if (head != null) {
            return head;
        }
        throw new IndexOutOfBoundsException("Can not peek at an element of an empty quque. Index is out of bounds.");
    }

    @Override
    public String getAuthorName() {
        return "Ilya Gotfryd";
    }

    @Override
    public Iterator<T> iterator() {

      return new QueueIterator<Item>(head);
}

  private class QueueIterator<E> implements Iterator<E>
    {

        LinkedListQueue.Node<E> currentNode = null;
        
        QueueIterator(LinkedListQueue.Node<E> head)
        {
            currentNode = head;
        }
        