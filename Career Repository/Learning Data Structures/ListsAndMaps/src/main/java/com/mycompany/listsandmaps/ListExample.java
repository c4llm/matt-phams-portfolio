/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.listsandmaps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class ListExample {
    public static void main(String[] args) {
//       // List<string> is the type, things inside<> are generics ( what can fit in my list)
//        List <String> someList = new ArrayList<>();
//                     //new String() is = to ""
//        someList.add("Object 1");
//        someList.add("Object 2");
//       // someList.add(new Integer(5));
//        //              casting = overriding compiler
//        String value1=(String)someList.get(0);
//        
//        
////        someList.remove(value1);
////        someList.remove(0);
//        
//        for(String value : someList) {
//        }   //iterators is the same as the enhanced for loop
//            //keeps track of where i am in this list
//            Iterator it=someList.iterator();
//            //it.hasNext() check current index if there is a value
//            while (it.hasNext()) {
//                String value = (String) it.next();
//                //it.next is moving to the next position in the list
//                System.out.println(value);
//                
//                
//            }
//        
            Map <String, Integer> populations = new HashMap<>();
            
            populations.put("USA", 300000000);
            populations.put("Canada", 3000000);
            populations.put("UK", 6000000);
            
            populations.put("USA", 290000);
            populations.remove("USA");
            
            Set<String> populationsKeys = populations.keySet();
            
            for (String key : populationsKeys) {
                Integer value = populations.get(key);
                System.out.println(value);
            
            }
            // looked up value based on the key
            Integer canadaPopulation = populations.get("Canada");
            
            
                    
        }
    }
    

