/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.iterationiii;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class IterationIII {

    public static void main(String[] args) {
        Map<String, List> cities = new HashMap<>();
        List<String> cleveland = new ArrayList<>();
        List<String> pittsburgh = new ArrayList<>();
        List<String> cincinnati = new ArrayList<>();
        List<String> minneapolis = new ArrayList<>();
        List<String> kansasCity = new ArrayList<>();

        cleveland.add("Browns");
        cleveland.add("Indians");
        pittsburgh.add("Steelers");
        pittsburgh.add("Pirates");
        cincinnati.add("Bengals");
        cincinnati.add("Reds");
        minneapolis.add("Vikings");
        minneapolis.add("Twins");
        kansasCity.add("Chiefs");
        kansasCity.add("Royals");

        cities.put("Cleveland", cleveland);
        cities.put("Pittsburgh", pittsburgh);
        cities.put("Cincinnati", cincinnati);
        cities.put("Minneapolis", minneapolis);
        cities.put("Kansas City", kansasCity);

//        Set<String> key = cities.keySet();
//        for (String k : key) {
//            System.out.println(k + ": ");
//            System.out.println(cities.get(k));
//
//        }

        Set <Map.Entry<String, List>> set = cities.entrySet();
        
        for (Map.Entry<String, List> teams : set) {
            System.out.println(teams.getKey() + ": \n" + teams.getValue());
        }
        
    }

}
