/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.adder;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Adder {
    
    public static void main(String[] args) {
    
        //declare sum and initialize to 0
        int sum = 0;
        int operand1 = 0;
        int operand2 = 0;
        
        Scanner sc = new Scanner(System.in);
        
        String userInput1 = "";
        String userInput2 = "";
        
        //Ask the user for input
        System.out.println("Please enter the first number to be added");
        
        userInput1 = sc.nextLine();
        
        operand1 = Integer.parseInt(userInput1);
        
        
        System.out.println("Please enter the second number to be added");
        
        userInput2 = sc.nextLine();
        operand2 = Integer.parseInt(userInput2);
        
        System.out.println("Sum is:" + (operand1 + operand2) );
        
        
    }
}
