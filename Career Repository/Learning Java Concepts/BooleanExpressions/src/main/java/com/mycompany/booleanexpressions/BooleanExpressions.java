/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.booleanexpressions;

/**
 *
 * @author apprentice
 */
public class BooleanExpressions {
    
    public static void main(String[] args){
            
    
    int i = 1;
    int x = 2;
    
    if (i == x){ //for variables to be compared
        //Do this
    }
    String m ="xyz";
    String y = "mms";
    
    if (y.equals(m)) { //for objects to be compared
    }
    if (i != x) {//do this
        
    }
    
    if ( i< x ) {
        System.out.println("i less than x");
    }
    
    if ( i>x ) {
        System.out.println("i greater than x");
    }
    
    if (i <= x) {
        System.out.println("i less than or equal to x");
    }
    
    if (i >= x) {
        System.out.println("i greater than or equal to x");
    }
    
    boolean playAgain = false; //to check if expression is false
       if(!playAgain) { System.out.println("User does not want to play again");
    }
       if( playAgain && i > x ) //&& means and or to chain together
       { System.out.println("play again true, i > than x");
       }
       
       if( playAgain || i > x ) // || means or 
       { System.out.println("play again true OR i > than x");
       }
       
       if(( playAgain || i > x) && m.equals("YEP")){
       }

       if( i > x   ^  x < 3 ){ // only ONE is true it'll be true, if both is true then it's false
           //do this
       }
       
       int day = 4;
       String dayName = "";
       
       if (day == 1){
            dayName = "Monday";
       } else if (day == 2){
           dayName = "Tuesday";
       } else if ( day == 3) {
           dayName = "Wedsnesday";
       } else {
           dayName = "what calender are you using?";
       }
       
       
       switch (day) {
           case 1:
               dayName = "Monday";
               break;
           case 2:
               dayName = "Tuesday";
               break;
           case 3:
               dayName = "Wednesday";
               break;
           default: // equivalent of else
               dayName = "Invalid day";
               
        
               
       }
       
//       do {
//           int t =0;
//           int tt= 10;
//           
//           t = t+ tt;
//           
//       } while (playAgain);
//       
//       while (playAgain) {
//           
//           playAgain = false;
//       }
       //loop while i less than 10
       for (int z=0; z < 10; z++ )
           System.out.println("Value of z: " + z);
           //flowchart example
    }
}
    
