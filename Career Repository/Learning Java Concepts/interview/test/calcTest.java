/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class calcTest {
    
    public calcTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void ShouldReturnZeroWhenInputIsEmpty() {
         StringCalc target = new StringCalc();
         int expected = 0;
         
         int actual = target.Add("");
         
         Assert.assertEquals(expected,actual);
     }
     @Test
     public void ReturnNumberWhenNumberPassedIn() {
         StringCalc target = new StringCalc();
         int expected = 458;
         
         int actual = target.Add("458");
         
         Assert.assertEquals(expected,actual);
     }
       @Test
     public void ShouldAllowTwoNumbersReturnSum() {
         StringCalc target = new StringCalc();
         int expected = 20;
         
         int actual = target.Add("17,3");
         
         Assert.assertEquals(expected,actual);
     }
       @Test
     public void ShouldAllowNewLinesReturnSum() {
         StringCalc target = new StringCalc();
         int expected = 25;
         
         int actual = target.Add("5\n17,3");
         
         Assert.assertEquals(expected,actual);
     }
}
