/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.objects;

/**
 *
 * @author apprentice
 */
public class ObjectExample {
    public static void main(String[] args) {
        //data type     variable/reference to the object   how to create "NEW" instance of an object and the FA is the object 
        FacebookAccount myAccount = new FacebookAccount("Matt","Pham");
      //  myAccount. then anything public will show up
      
//        FacebookAccount.keyCode = "27";
      
     FacebookAccount myAccount2 = new FacebookAccount("Jill","Jones");
      //garbage collector will eventually delete Matt Pham FB account
      
      FacebookAccount myAccount3 = new FacebookAccount("Jerry","Smith");
      
      
      //myAccount.getFirstname();
      //can get firstname but cant change it
      
      myAccount.setFirstname("Mat");
      
      ObjectExample example = new ObjectExample();
      example.addThing(1, 2);
      //sets firstName as Matt
      
    }
    
    public void addThing (int a, int b){
    }
}
