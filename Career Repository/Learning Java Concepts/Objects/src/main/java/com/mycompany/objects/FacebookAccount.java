/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.objects;

/**
 *
 * @author apprentice
 */
public class FacebookAccount {
    
//    public static String keyCode = "11";
//   
//    public static void setKeyCode(String keyCode){
//        FacebookAccount.keyCode = keyCode;
//        // can't use this. here
//    }
            
    private String emailAddress;
    private String firstName;
    private String lastName;
    //private can only be accessed in this FacebookAccount Class
    
    public FacebookAccount(){
        //if calling this for constructor, must be the first thing
        this("Matt","Pham");
        System.out.println("Facebook account no parameters");
        
    }
    public FacebookAccount(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    public String getFirstname() {
    return firstName;
    //created public mechanism to get private firstName
    }
    public void setFirstname(String firstname){
        this.firstName = firstName;
        //setting value of firstname to this.firstname which now can be accessable outside of this class
    }
}
