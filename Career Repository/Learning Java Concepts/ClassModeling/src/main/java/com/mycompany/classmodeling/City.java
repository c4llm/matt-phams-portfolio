/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class City {
    
    private int streets;
    private int people;
    private int buildings;
    private double area;
    private String state;
    
    public City(int streets, int people, int buildings, double area, String state){
        this.streets=streets;
        this.people=people;
        this.buildings=buildings;
        this.area=area;
        this.state=state;
    }
    
    public void containMaximumPeople(){
    }

    /**
     * @return the streets
     */
    public int getStreets() {
        return streets;
    }

    /**
     * @param streets the streets to set
     */
    public void setStreets(int streets) {
        this.streets = streets;
    }

    /**
     * @return the people
     */
    public int getPeople() {
        return people;
    }

    /**
     * @param people the people to set
     */
    public void setPeople(int people) {
        this.people = people;
    }

    /**
     * @return the buildings
     */
    public int getBuildings() {
        return buildings;
    }

    /**
     * @param buildings the buildings to set
     */
    public void setBuildings(int buildings) {
        this.buildings = buildings;
    }

    /**
     * @return the area
     */
    public double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(double area) {
        this.area = area;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }
    
    
    
}
