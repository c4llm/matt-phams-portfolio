/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class House {
    
    private int height;
    private String location;
    private int squareFeet;
    private String color;
    private int yearMade;
    
    public House (int height, String location, int squareFeet, String color, int yearMade){
    this.color = color;
    this.height = height;
    this.location = location;
    this.squareFeet = squareFeet;
    this.yearMade = yearMade;   
            }
    
    public void provdieShelter(){
    }
    
    public void placeToSleep(){
    }
    
    public void provideAddress(){
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the squareFeet
     */
    public int getSquareFeet() {
        return squareFeet;
    }

    /**
     * @param squareFeet the squareFeet to set
     */
    public void setSquareFeet(int squareFeet) {
        this.squareFeet = squareFeet;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the yearMade
     */
    public int getYearMade() {
        return yearMade;
    }

    /**
     * @param yearMade the yearMade to set
     */
    public void setYearMade(int yearMade) {
        this.yearMade = yearMade;
    }
    
}
