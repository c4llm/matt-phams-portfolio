/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class Couch {
    
    private int cushions;
    private int weight;
    private String material;
    private String make;
    private String type;
    
    public Couch (int cushions, int weight, String material, String make, String type){
        this.cushions=cushions;
        this.weight=weight;
        this.material=material;
        this.make=make;
        this.type=type;
    }
    
    public void holdPerson(){
    }
    
    public void provideComfort(){
    }

    /**
     * @return the cushions
     */
    public int getCushions() {
        return cushions;
    }

    /**
     * @param cushions the cushions to set
     */
    public void setCushions(int cushions) {
        this.cushions = cushions;
    }

    /**
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @return the material
     */
    public String getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(String material) {
        this.material = material;
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    
}
