/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class School {
    
    private String district;
    private String Curriculum;
    private int yearFounded;
    private int teachers;
    private String name;
    
    public School(String district, String curriculum, int yearFounded, int teachers, String name){
    this.Curriculum = curriculum;
    this.district=district;
    this.name=name;
    this.teachers=teachers;
    this.yearFounded=yearFounded;
    
    }
    
    public void teachStudents(){
    }
    
    public void keepStudentsSafe(){}

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the Curriculum
     */
    public String getCurriculum() {
        return Curriculum;
    }

    /**
     * @param Curriculum the Curriculum to set
     */
    public void setCurriculum(String Curriculum) {
        this.Curriculum = Curriculum;
    }

    /**
     * @return the yearFounded
     */
    public int getYearFounded() {
        return yearFounded;
    }

    /**
     * @param yearFounded the yearFounded to set
     */
    public void setYearFounded(int yearFounded) {
        this.yearFounded = yearFounded;
    }

    /**
     * @return the teachers
     */
    public int getTeachers() {
        return teachers;
    }

    /**
     * @param teachers the teachers to set
     */
    public void setTeachers(int teachers) {
        this.teachers = teachers;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
}
