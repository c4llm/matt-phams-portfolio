/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class Triangle {
    
    private int angles;
    private int sideLength;
    private String type;
    private String color;
    private int area;
    
    public Triangle(int angles, int sideLength, String type, String color, int area){
    this.angles=angles;
    this.area=area;
    this.color=color;
    this.sideLength=sideLength;
    this.type=type;
    
    
    
    }
    
    public void holdShapes(){}
    
    public void stack(){}

    /**
     * @return the angles
     */
    public int getAngles() {
        return angles;
    }

    /**
     * @param angles the angles to set
     */
    public void setAngles(int angles) {
        this.angles = angles;
    }

    /**
     * @return the sideLength
     */
    public int getSideLength() {
        return sideLength;
    }

    /**
     * @param sideLength the sideLength to set
     */
    public void setSideLength(int sideLength) {
        this.sideLength = sideLength;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the area
     */
    public int getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(int area) {
        this.area = area;
    }
}
