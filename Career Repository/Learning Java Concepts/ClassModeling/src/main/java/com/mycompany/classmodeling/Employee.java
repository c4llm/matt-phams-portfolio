/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class Employee {
    
    private String ethnicity;
    private int age;
    private int heightInInches;
    private String homeTown;
    private int hiredYear;
    
    public Employee( String ethnicity, int age, int heightInInches, String homeTown, int hiredYear){
    this.ethnicity=ethnicity;
    this.age=age;
    this.heightInInches=heightInInches;
    this.homeTown=homeTown;
    this.hiredYear=hiredYear;
    }
    
    public void doTask(){
    
    }
    
    public void doTeamwork(){
    }

    /**
     * @return the ethnicity
     */
    public String getEthnicity() {
        return ethnicity;
    }

    /**
     * @param ethnicity the ethnicity to set
     */
    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the heightInInches
     */
    public int getHeightInInches() {
        return heightInInches;
    }

    /**
     * @param heightInInches the heightInInches to set
     */
    public void setHeightInInches(int heightInInches) {
        this.heightInInches = heightInInches;
    }

    /**
     * @return the homeTown
     */
    public String getHomeTown() {
        return homeTown;
    }

    /**
     * @param homeTown the homeTown to set
     */
    public void setHomeTown(String homeTown) {
        this.homeTown = homeTown;
    }

    /**
     * @return the hiredYear
     */
    public int getHiredYear() {
        return hiredYear;
    }

    /**
     * @param hiredYear the hiredYear to set
     */
    public void setHiredYear(int hiredYear) {
        this.hiredYear = hiredYear;
    }
    
    
    
}

