/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class Chair {
    
    private int legs;
    private String material;
    private double heightInInches;
    private double cost;
    private String color;
    
    public Chair( int legs, String material, double heightInInches, double cost, String color){
        this.legs=legs;
        this.material=material;
        this.heightInInches=heightInInches;
        this.cost=cost;
        this.color=color;
    }
    
    public void holdPerson(){
    }

    /**
     * @return the legs
     */
    public int getLegs() {
        return legs;
    }

    /**
     * @param legs the legs to set
     */
    public void setLegs(int legs) {
        this.legs = legs;
    }

    /**
     * @return the material
     */
    public String getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(String material) {
        this.material = material;
    }

    /**
     * @return the heightInInches
     */
    public double getHeightInInches() {
        return heightInInches;
    }

    /**
     * @param heightInInches the heightInInches to set
     */
    public void setHeightInInches(double heightInInches) {
        this.heightInInches = heightInInches;
    }

    /**
     * @return the cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }
    
   
}
