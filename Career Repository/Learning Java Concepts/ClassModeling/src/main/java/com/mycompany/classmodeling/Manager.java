/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class Manager {

    private int yearsManaged;
    private String companyWorkFor;
    private int numberOfPeopleManaged;
    private String name;
    private int age;
    
   

    public Manager(int yearsManaged, String companyWorkFor, int numberOfPeopleManaged, String name, int age) {

        this.age = age;
        this.companyWorkFor = companyWorkFor;
        this.name = name;
        this.numberOfPeopleManaged = numberOfPeopleManaged;
        this.yearsManaged = yearsManaged;
    }

    public void managesEmployees() {

    }
    
    public void completesResponsibilities(){
    
    }

    /**
     * @return the yearsManaged
     */
    public int getYearsManaged() {
        return yearsManaged;
    }

    /**
     * @param yearsManaged the yearsManaged to set
     */
    public void setYearsManaged(int yearsManaged) {
        this.yearsManaged = yearsManaged;
    }

    /**
     * @return the companyWorkFor
     */
    public String getCompanyWorkFor() {
        return companyWorkFor;
    }

    /**
     * @param companyWorkFor the companyWorkFor to set
     */
    public void setCompanyWorkFor(String companyWorkFor) {
        this.companyWorkFor = companyWorkFor;
    }

    /**
     * @return the numberOfPeopleManaged
     */
    public int getNumberOfPeopleManaged() {
        return numberOfPeopleManaged;
    }

    /**
     * @param numberOfPeopleManaged the numberOfPeopleManaged to set
     */
    public void setNumberOfPeopleManaged(int numberOfPeopleManaged) {
        this.numberOfPeopleManaged = numberOfPeopleManaged;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }
}
