/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class Airplane {
    
    private int wings;
    private int jets;
    private int seats;
    private String type;
    private int windows;
    
    public Airplane(int wings, int jets, int seats, String type, int windows){
        this.wings=wings;
        this.jets=jets;
        this.seats=seats;
        this.type=type;
        this.windows=windows;
    }
    
    public void carryPassengers(){
    }
    
    public void flyToDestination(){
    }

    /**
     * @return the wings
     */
    public int getWings() {
        return wings;
    }

    /**
     * @param wings the wings to set
     */
    public void setWings(int wings) {
        this.wings = wings;
    }

    /**
     * @return the jets
     */
    public int getJets() {
        return jets;
    }

    /**
     * @param jets the jets to set
     */
    public void setJets(int jets) {
        this.jets = jets;
    }

    /**
     * @return the seats
     */
    public int getSeats() {
        return seats;
    }

    /**
     * @param seats the seats to set
     */
    public void setSeats(int seats) {
        this.seats = seats;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the windows
     */
    public int getWindows() {
        return windows;
    }

    /**
     * @param windows the windows to set
     */
    public void setWindows(int windows) {
        this.windows = windows;
    }
    
    
    
}
