/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.classmodeling;

/**
 *
 * @author apprentice
 */
public class Square {
    
    private int width;
    private int length;
    private String color;
    private int perimeter;
    private int area;
    
    public Square(int width, int length, String color, int perimeter, int area ) {
    
        this.width=width;
        this.length=length;
        this.color=color;
        this.perimeter=perimeter;
        this.area=area;
        
        
    }
    
    public void holdShapes(){}

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the perimeter
     */
    public int getPerimeter() {
        return perimeter;
    }

    /**
     * @param perimeter the perimeter to set
     */
    public void setPerimeter(int perimeter) {
        this.perimeter = perimeter;
    }

    /**
     * @return the area
     */
    public int getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(int area) {
        this.area = area;
    }
    
}
