/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbd;

/**
 *
 * @author apprentice
 */
public class NumbersAndMath {
    
    public static void main( String[] args )
	{
                // Prints out string as is.
		System.out.println( "I will now count my chickens:" );
                //Prints out Hens plus a space as well as dividing 30 by 6, then adding 25
		System.out.println( "Hens " + ( 25f + 30f / 6f ) );
                //Prints out Roosters, multiplies first, then divides and provides only the remainder,
                //which is then subtracted from 100.
		System.out.println( "Roosters " + ( 100f - 25f * 3f % 4f ) );
                //Prints string
		System.out.println( "Now I will count the eggs:" );
                //Divides first, then adds, then subtracts, and then prints answer 
		System.out.println( 3f + 2f + 1f - 5f + 4f % 2f - 1f / 4f + 6f );
                //Prints string
		System.out.println( "Is it true that 3 + 2 < 5 - 7?" );
                //Performs operations and determines if it's true/false, then prints
		System.out.println( 3f + 2f < 5f - 7f );
                //Prints string, then prints answer to 3 + 2.
		System.out.println( "What is 3 + 2? " + ( 3f + 2f ) );
                //Prints string, then prints answer for 5-7
		System.out.println( "What is 5 - 7? " + ( 5f - 7f ) );
                //Prints String
		System.out.println( "Oh, that's why it's false." );
                //Prints string
		System.out.println( "How about some more." );
                //Prints string, then prints true because 5 is greater than -2 
		System.out.println( "Is it greater? " + ( 5f > -2f ) );
                //Prings string, then prints true because 5 is greater than -2
		System.out.println( "Is it greater or equal? " + ( 5f >= -2f ) );
                //Prints string, then prints false because 5 isn't less than or equal to 2.
		System.out.println( "Is it less or equal? " + ( 5f <= -2f ) );
	}
    
}
