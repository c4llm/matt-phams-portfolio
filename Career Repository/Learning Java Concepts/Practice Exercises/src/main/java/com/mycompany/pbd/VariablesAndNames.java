/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbd;

/**
 *
 * @author apprentice
 */
public class VariablesAndNames { 
    
    public static void main( String[] args )
    {
        int cars, drivers, passengers, cars_not_driven, cars_driven;
        double space_in_a_car, carpool_capacity, average_passengers_per_car;
        //cars assigned to 100
        cars = 100;
        //space in a car assigned to 4
        space_in_a_car = 4.0;
        //30 drivers were present
        drivers = 30;
        //there was 90 passengers
        passengers = 90;
        //the number of cars not driven is equal to total cars minus total drivers
        cars_not_driven = cars - drivers;
        //cars driven is assigned to the value of drivers (30)
        cars_driven = drivers;
        //the carpool capacity is equivalent to the number of cars driven times the 4 spaces in a car
        carpool_capacity = cars_driven * space_in_a_car;
        //the average is assigned to passengers divided by the number of cars driven
        average_passengers_per_car = passengers / cars_driven;


        System.out.println( "There are " + cars + " cars available." );
        System.out.println( "There are only " + drivers + " drivers available." );
        System.out.println( "There will be " + cars_not_driven + " empty cars today." );
        System.out.println( "We can transport " + carpool_capacity + " people today." );
        System.out.println( "We have " + passengers + " to carpool today." );
        System.out.println( "We need to put about " + average_passengers_per_car + " in each car." );
    }
    
}
