/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbd;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AgeInFiveYears {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name = "";
        int age = 0;
        System.out.print("Hello. What is your name? ");
        name = sc.nextLine();
        System.out.println("");
        System.out.print("Hi, "+name+"! How old are you? ");
        age = sc.nextInt();
        System.out.println("Did you know that in five years you will be "+(age+5)+" years old?");
        System.out.println("And five years ago you were "+(age-5)+"! Imagine that!");
                
        
        
    }
    
}
