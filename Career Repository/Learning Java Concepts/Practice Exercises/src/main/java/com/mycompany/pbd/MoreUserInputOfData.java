/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbd;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MoreUserInputOfData {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String Fname;
        String Lname;
        int Grade;
        int StudentId;
        String Login;
        double GPA;

        System.out.println("Please enter the following information so I can sell it for profit\n");
        System.out.print("First name: ");
        Fname = keyboard.next();
        System.out.print("Last name: ");
        Lname = keyboard.next();
        System.out.print("Grade (9-12): ");
        Grade = keyboard.nextInt();
        System.out.print("Student ID: ");
        StudentId = keyboard.nextInt();
        System.out.print("Login: ");
        Login = keyboard.next();
        System.out.print("GPA (0.0-4.0: ");
        GPA = keyboard.nextDouble();
        System.out.println("");
        System.out.println("Your information:");
        System.out.println("        Login: " + Login);
        System.out.println("        ID " + StudentId);
        System.out.println("        Name: " + Lname + ", " + Fname);
        System.out.println("        GPA: " + GPA);
        System.out.println("        Grade: " + Grade);

    }

}
