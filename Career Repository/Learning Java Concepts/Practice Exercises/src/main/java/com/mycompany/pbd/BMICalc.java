/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbd;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BMICalc {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int feet=0;
        int inches=0;
        int pounds=0;
        double kilos=0;
        double centiFeet=0;
        double centiInch=0;
        double heightSum=0;
        double BMI=0;
                
        System.out.print("Your height (feet only): ");
        feet=sc.nextInt();
        System.out.print("Your height (inches): ");
        inches=sc.nextInt();
        System.out.print("Your weight in pounds: ");
        pounds=sc.nextInt();
        kilos=(pounds*(0.45));
        centiFeet=((12*feet)*(0.025));
        centiInch=(inches*(0.025));
        heightSum=centiFeet+centiInch;
        BMI=kilos/(heightSum*heightSum);
        System.out.print("Your BMI is "+BMI);
        
        
        
    }
    
}


//   weight in kilo / height squared in centimeters