/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbd;

/**
 *
 * @author apprentice
 */
public class YourSchedule {

    public static void main(String[] args) {
        String[] courses = {"English III", "Precalculus", "Music Theory", "Biotechnology", "Principles of Technology I", "Latin II", "AP US History", "Business Computer Information Systems"
        };
        String[] teachers = {"Ms. Lapan", "Mrs. Gideon", "Mr. Davis", "Ms. Palmer", "Ms. Garcia", "Mrs. Barnett", "Ms. Johannessen", "Mr.James"
        };
        int theLength = 0;
        horizontalEdge();
        for (int s = 0; s < 9; s++) {
            theLength = lengthCounter(courses[s]);
            numberEdge(theLength,courses[s]);
        }

    }

    public static void horizontalEdge() {
        System.out.println("+-------------------------------------------------+");
    }

    public static void numberEdge(int length, String courses ) {

        for (int i = 0; i < 9; i++) {
            System.out.print("| ");
            System.out.print(i);
            System.out.print(" |");
            String spaces = " ";
            //int number = Integer.parseInt(spaces);
            int numberOfTimes = 26 - length;
            for ( int g = 0; g< numberOfTimes; g++){
                System.out.print(spaces);}
             System.out.print(courses);   
        }
    }

    public static int lengthCounter(String courses) {
        int theLength = 0;
        theLength = courses.length();
        return theLength;
    }
}


