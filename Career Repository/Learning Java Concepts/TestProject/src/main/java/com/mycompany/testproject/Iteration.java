/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testproject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Iteration {
    
    public static void main(String[] args) {
        List <String> footballTeams = new ArrayList<>();
        footballTeams.add("Vikings");
        footballTeams.add("Packers");
        footballTeams.add("Lions");
        footballTeams.add("Bears");
        footballTeams.add("Browns");
        footballTeams.add("Bengels");
        footballTeams.add("Steelers");
        footballTeams.add("Ravens");
        
        for (String teams: footballTeams){
            System.out.println(teams);
        }
        
        Iterator it = footballTeams.iterator();
        while (it.hasNext()){
            String value = (String)it.next();
            System.out.println(value);
        }
        
        for( int i=0; i<footballTeams.size(); i++){
            String getValue=(String)footballTeams.get(i);
            System.out.println(getValue);
        }
    }
    
}
