/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GreatPartyTest {
      
    private GreatParty myParty;
    
    public GreatPartyTest() {
    }
    
    @Before
    public void setUp() {
        myParty= new GreatParty();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGreatParty30False(){
        
      

        boolean result = myParty.greatParty(30 , false);
        
        Assert.assertFalse(result);
        

}
 
    @Test
    public void tesGreatParty50False(){
        
        boolean result = myParty.greatParty(50, false);
        
        Assert.assertTrue(result);
    }
    
    @Test
    public void testGreatParty70AndTrue(){
    
        boolean result = myParty.greatParty(70, true);
        Assert.assertTrue(result);
    }
}
