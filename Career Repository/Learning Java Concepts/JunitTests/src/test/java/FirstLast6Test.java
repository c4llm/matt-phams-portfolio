/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FirstLast6Test {
FirstLast6 firstLast6 = new FirstLast6();
    
    public FirstLast6Test() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
//    FirstLast6({1, 2, 6}) -> true
//FirstLast6({6, 1, 2, 3}) -> true
//FirstLast6({13, 6, 1, 2, 3}) -> false
    
    
    @Test
    public void testFirstLast6_1_2_6() {
        
        int[] array = {
            1, 2, 6
        };
        
        boolean result = firstLast6.firstLast6(array);
        
        Assert.assertTrue(result);
        
    }
    

    @Test
    public void testFirstLast6_6_1_2_3() {
        
    }
    
    @Test
    public void testFirstLast6_13_6_1_2_3() {
        
    }
}
