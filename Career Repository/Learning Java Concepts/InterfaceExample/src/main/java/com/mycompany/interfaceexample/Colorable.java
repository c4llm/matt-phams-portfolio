/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceexample;

/**
 *
 * @author apprentice
 */
public interface Colorable extends Thing{
    
    public final String DEFAULT_COLOR = "Blue";
    public void setColor(String color);
    public String getColor();
}
