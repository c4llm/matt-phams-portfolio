/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.olympian;

import java.util.Date;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author apprentice
 */
public class OlympicOfficial {
    
    public void inspectEquipment(JoinPoint jp){
        System.out.println("Inspecting equipment...");
        Date dateOfEvent = (Date)jp.getArgs()[0];
        System.out.println("Inspecting at: "+dateOfEvent.toString());
    }
 
    public void testforPED(){
        System.out.println("Testing for PEDs...");
    }
}
