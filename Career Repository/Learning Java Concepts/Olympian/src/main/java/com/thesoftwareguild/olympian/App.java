/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.olympian;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class App {
    
    public static void main(String[] args) {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        
        Olympian skiJumper = ctx.getBean("usaSkiJumper",Olympian.class);
        
        Olympian usaSpeedSkater = ctx.getBean("usaSpeedSkater",Olympian.class);
        
        Olympian canadianCurler = ctx.getBean("canadianCurler",Olympian.class);
        
        List<Olympian> list = new ArrayList();
        list.add(skiJumper);
        list.add(usaSpeedSkater);
        list.add(canadianCurler);
        
        for(Olympian o : list){
            o.compete();
        }
        
        skiJumper.compete();
    }
}
