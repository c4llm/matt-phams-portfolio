/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.olympian;

import java.util.Date;

/**
 *
 * @author apprentice
 */
public interface Event {
    
    public String compete(Date currentDay);
}
