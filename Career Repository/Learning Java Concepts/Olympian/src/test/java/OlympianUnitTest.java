/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class OlympianUnitTest {
    
    ApplicationContext ctx;
    
    public OlympianUnitTest() {
    }
    
    @Before
    public void setUp() {
        
        ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
//    
//    @Test
//    public void skiJumpTest(){
//        SkiJumper jumper = new SkiJumper();
//        Assert.assertEquals(jumper.compete(),"SkiJump");
//    }
//    
//    @Test
//    public void dependencyInjectionTest(){
//        SkiJumper sj = (SkiJumper) ctx.getBean("mySkiJumper");
//        Assert.assertEquals(sj.compete(),"SkiJump");
//    }
}
