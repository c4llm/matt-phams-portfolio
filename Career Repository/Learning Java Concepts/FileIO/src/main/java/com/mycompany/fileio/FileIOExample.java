/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fileio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class FileIOExample {
    // constants
    final static String FILENAME = "students.txt";
    final static String DELIMETER= "::";
    
    public static void main(String[] args) {
//                                            //filewriter creates the file if it doesnt exist, and if it does, it overwrites it
//        try {
//            PrintWriter pw = new PrintWriter(new FileWriter("Outfile.txt"));
//            
//            pw.println("this is the first line");
//            
//            pw.flush();
//            
//            pw.println("this is the second line");
//            pw.println("this is the third line");
//            
//            // flush writes ^ to the file
//            pw.flush();
//            
//            //close frees up memory that the file is using
//            pw.close();
//            
//        } catch (IOException ex) {
//            Logger.getLogger(FileIOExample.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    
//        // try catch in case file is not found
//        // this reads our text file using a scanner
//        // this deletes the previous lines and writes over it
//        try {
//            Scanner sc = new Scanner( new BufferedReader(new FileReader("Outfile.txt")));
//            
//            while(sc.hasNextLine()){
//                String currentLine = sc.nextLine();
//                System.out.println(currentLine);
//            }
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(FileIOExample.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//            
//    
//    

        List<Student> students = new ArrayList<Student>();
        
        Student student1 = new Student();
        student1.setFirstName("John");
        student1.setLastName("Smith");
        student1.setId(1);
        student1.setCohort("January");
        
        students.add(student1);
        
                
        Student student2 = new Student();
        student2.setFirstName("Mike");
        student2.setLastName("Singletary");
        student2.setId(2);
        student2.setCohort("June");
        
        students.add(student2);
        
                
        Student student3 = new Student();
        student3.setFirstName("Elias");
        student3.setLastName("Gousinta");
        student3.setId(3);
        student3.setCohort("December");
        
        students.add(student3);
        
        
        
        encode(students);
        List<Student> decode = decode();
        
        
        
        
    }
    
    public static void encode(List<Student> students){
        
        try {
            PrintWriter pw = new PrintWriter(new FileWriter("students.txt"));
            
            for (Student dude : students){
            
                String line = dude.getId() + DELIMETER
                              + dude.getFirstName() + DELIMETER
                              + dude.getLastName() + DELIMETER
                              + dude.getCohort();
                
                pw.println(line);
                
            }
           pw.flush();
            pw.close();
            
        } catch (IOException ex) {
        }
    
    }
    
    public static List<Student> decode(){
       // declared array here so that it is in scope when returning value
        List<Student> students = new ArrayList <Student>();
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));
            
            while( sc.hasNextLine()) {
            
                String currentLine = sc.nextLine();
                //split takes each value thats seperated by the DELIMTER, and puts it into a string array
                String[] values=currentLine.split(DELIMETER);
                
                Student dude = new Student();
                
                dude.setId(Integer.parseInt(values[0]));
                dude.setFirstName(values[1]);
                dude.setLastName(values[2]);
                dude.setCohort(values[3]);
                
                students.add(dude);
                
                
                
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIOExample.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return students;
    }

     
}
