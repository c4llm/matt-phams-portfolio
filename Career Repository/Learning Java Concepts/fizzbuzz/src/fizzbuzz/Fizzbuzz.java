/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizzbuzz;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Fizzbuzz {

    private static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // when 3 have fizz
        // when 5 have buzz
        // when 15 have fizz buzz
        int number = 0;

        System.out.println("Enter a number");
        number = sc.nextInt();
        fizz(number);
        buzz(number);
        
    }

    public static void fizz(int number) {
        if (number % 3 == 0) {
            System.out.print("fizz ");
        }
    }

    public static void buzz(int number) {
        if (number % 5 == 0) {
            System.out.print("buzz");
        }

    }

}
