/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author apprentice
 */
public class Manager extends Employee {
    
    public Manager(){
        super("pat");
        System.out.println("Created new manager");
        
        this.name="Blah";
        this.myPrivateMethod();
    }
    
    @Override //must have same method signature to use this method when calling Manager
    public void doWork(){
        super.doWork();
        System.out.println("Manager: do work lol");
    }
    
    public void hire(){
        System.out.println("I give you jobs");
    }
    
    public void fire(){
        System.out.println("I'm firing you");
    }
    
    public void givePerformancheReview(){
        System.out.println("Here is your performance review");
    }
    
}
