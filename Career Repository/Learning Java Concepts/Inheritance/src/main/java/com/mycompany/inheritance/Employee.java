/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author apprentice
 */
public abstract class Employee extends Person{
    
    protected String name;
    
    public Employee(String name){
        System.out.println("Created new employee");
    }
    
    public abstract void getPaid();
    
    public void doWork() {
        System.out.println("Employee: do work");
    }
    
    public void createObjectives(){
        System.out.println("Don't get fired");
    }
    
    protected void myPrivateMethod(){
        
    }
    
}
