/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author apprentice
 */
public class SummerIntern extends Employee {
    
    public SummerIntern (String name){
        super("pat");
    }
    
    @Override
    public void doWork(){
        System.out.println("SummerIntern: do work");
    }
    
    public void requestPerformanceReview() {
        
    }
    
    
}
