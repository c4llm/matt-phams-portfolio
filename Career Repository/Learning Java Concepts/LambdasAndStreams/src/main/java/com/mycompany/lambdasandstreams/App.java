/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lambdasandstreams;
import com.thesoftwareguild.consoleio.ConsoleIo;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class App {
    public static void main(String[] args) {
        
        ConsoleIo conosle = new ConsoleIo();
        List<Server> servers = new ArrayList<Server>();
        
        Server one = new Server();
        one.setName("web01");
        one.setIp("192.168..1.1");
        one.setManufacturer("Dell");
        one.setRam(8);
        one.setNumProcessors(9);
        one.setPurchaseDate(LocalDate.parse("2010-01-01",DateTimeFormatter.ISO_DATE));
        
        servers.add(one);
        
        one = new Server();
        one.setName("db01");
        one.setIp("192.168..3.45");
        one.setManufacturer("HP");
        one.setRam(16);
        one.setNumProcessors(24);
        one.setPurchaseDate(LocalDate.parse("2013-01-01",DateTimeFormatter.ISO_DATE));
        
        servers.add(one);
        
        one = new Server();
        one.setName("hr124");
        one.setIp("192.168..32.111");
        one.setManufacturer("IBM");
        one.setRam(16);
        one.setNumProcessors(12);
        one.setPurchaseDate(LocalDate.parse("2014-01-01",DateTimeFormatter.ISO_DATE));
        
        servers.add(one);
        
        one = new Server();
        one.setName("eng16");
        one.setIp("192.168..35.56");
        one.setManufacturer("HP");
        one.setRam(4);
        one.setNumProcessors(8);
        one.setPurchaseDate(LocalDate.parse("2001-01-01",DateTimeFormatter.ISO_DATE));
        
        servers.add(one);
        
        one = new Server();
        one.setName("eng64");
        one.setIp("192.168..34.56");
        one.setManufacturer("HP");
        one.setRam(8);
        one.setNumProcessors(16);
        one.setPurchaseDate(LocalDate.parse("2001-01-01",DateTimeFormatter.ISO_DATE));
        
        servers.add(one);
        
        servers
                .stream()
                .forEach((Server s) -> {
                            System.out.println("+++++++++++");
                            System.out.println(s.getName());
                });
        

        String manufacturer = "Dell";
        servers
                .stream()
                .filter(server -> server.getManufacturer().equalsIgnoreCase(manufacturer))
                .forEach(s -> System.out.println(s.getName())); //filter takes a lambda
        
        long age = 3;
        servers
                .stream()
                .filter(s -> s.getServerAge() > 3)
                .forEach(s -> System.out.println(s.getName()));
        
        
        
        List <Server> oldServers =
                servers
                    .stream()
                    .filter(s -> (s.getServerAge() > age))
                    .collect(Collectors.toList());
        
        double averageAge = 
                servers
                        .stream()
                        .mapToLong(s -> s.getServerAge())
                        .average()
                        .getAsDouble();
        
        System.out.println("Average age = "+averageAge);
    }
}
