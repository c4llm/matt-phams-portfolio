/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lambdasandstreams;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class ServerDao {

    private Server server = new Server();
    private static Map<String, Server> servers = new HashMap<>();
    public static void main(String[] args) {
        //create crud and practice searching
        Server one = new Server();
        one.setName("web01");
        one.setIp("192.168.1.1");
        one.setManufacturer("Dell");
        one.setRam(8);
        one.setNumProcessors(9);
        one.setPurchaseDate(LocalDate.parse("2010-01-01", DateTimeFormatter.ISO_DATE));
        servers.put(one.getName(), one);
        
        one = new Server();
        one.setName("db01");
        one.setIp("192.168.3.45");
        one.setManufacturer("HP");
        one.setRam(16);
        one.setNumProcessors(24);
        one.setPurchaseDate(LocalDate.parse("2013-01-01",DateTimeFormatter.ISO_DATE));
        servers.put(one.getName(), one);
        
        one = new Server();
        one.setName("hr124");
        one.setIp("192.168.32.111");
        one.setManufacturer("IBM");
        one.setRam(16);
        one.setNumProcessors(12);
        one.setPurchaseDate(LocalDate.parse("2014-01-01", DateTimeFormatter.ISO_DATE));
        servers.put(one.getName(), one);
        
        one = new Server();
        one.setName("eng16");
        one.setIp("192.168.32.56");
        one.setManufacturer("HP");
        one.setRam(4);
        one.setNumProcessors(8);
        one.setPurchaseDate(LocalDate.parse("2001-01-01", DateTimeFormatter.ISO_DATE));
        servers.put(one.getName(), one);
        
        one = new Server();
        one.setName("eng64");
        one.setIp("192.168.34.56");
        one.setManufacturer("HP");
        one.setRam(8);
        one.setNumProcessors(16);
        one.setPurchaseDate(LocalDate.parse("2001-01-01", DateTimeFormatter.ISO_DATE));
        servers.put(one.getName(), one);
        
        String make = "Dell";
        System.out.println("All " + make + " servers in inventory: ");
        servers.values()
                .stream()
                .filter(s -> s.getManufacturer().equalsIgnoreCase(make))
                .forEach(e -> System.out.println(e.getName()));
        
        int testAge = 3;
        System.out.println("All servers older than " + testAge + " years in inventory:");
        servers.values()
                .stream()
                .filter(s -> s.getServerAge() > testAge)
                .forEach(e -> System.out.println(e.getName()));
        
        Collection<Server> oldServers
                =servers.values()
                .stream()
                .filter(s -> s.getServerAge() > testAge)
                .collect(Collectors.toList());
        
        System.out.println(oldServers.size());
        oldServers.forEach(s -> System.out.println(s.getName()));
        
        double averageAge
                = servers.values()
                .stream()
                .mapToLong(Server::getServerAge)
                .average()
                .getAsDouble();
        System.out.println("Average age of server is "+ averageAge +"years.");
        
        
    }
    public List<Server> findServersByManufacturer(String manufacturer) {
        return servers.values()
                .stream()
                .filter(s -> s.getManufacturer().equals(manufacturer))
                .collect(Collectors.toList());
    }  
        
        
}
