/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapesperimeters;

/**
 *
 * @author apprentice
 */
public class Rectangle extends Shape{
    private double length;
    private double width;

    @Override
    public double area() {
        return getLength()*getWidth();
    }

    @Override
    public double perimeter() {
        return length*2 + width*2;
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }
    
}
