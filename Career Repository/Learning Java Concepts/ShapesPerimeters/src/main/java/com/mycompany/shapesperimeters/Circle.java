/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapesperimeters;

/**
 *
 * @author apprentice
 */
public class Circle extends Shape{
    private double radius;

    @Override
    public double area() {
        return Math.PI*getRadius()*getRadius();
    }

    @Override
    public double perimeter() {
        return 2*Math.PI*getRadius();
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }
    
}
