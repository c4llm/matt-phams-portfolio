/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapesperimeters;

/**
 *
 * @author apprentice
 */
public class Triangle extends Shape {
    private double base;     
    private double height;
    
    @Override
    public double area() {
        return (getBase()*getHeight())/2;
    }

    @Override
    public double perimeter() {
        double hypotenuse  = Math.sqrt(getBase()*getBase() + getHeight()*getHeight());
        return hypotenuse + getBase() + getHeight();
    }

    /**
     * @return the base
     */
    public double getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(double base) {
        this.base = base;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }
}
