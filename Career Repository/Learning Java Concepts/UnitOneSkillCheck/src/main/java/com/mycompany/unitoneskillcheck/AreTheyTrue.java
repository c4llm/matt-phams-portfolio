/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unitoneskillcheck;

/**
 *
 * @author apprentice
 */
public class AreTheyTrue {
    public static void main(String[] args) {
        String test1="";
        String test2="";
        String test3="";
        String test4="";
        test1 = howTrue(true,true);
        System.out.println(test1);
        test2 = howTrue(false,false);
        System.out.println(test2);
        test3 = howTrue(true,false);
        System.out.println(test3);
        test4 = howTrue(false,true);
        System.out.println(test4);
        
    }
    
    public static String howTrue(boolean one, boolean two){
        String neither="NEITHER";
        String both="BOTH";
        String onlyOne="ONLY ONE";
        if (one!=true || two!= true){
        return neither;
        }
        if (one==true && two==true){
        return both;
        }
        return onlyOne;
            }
}
