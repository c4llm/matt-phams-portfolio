/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unitoneskillcheck;

/**
 *
 * @author apprentice
 */
public class Adder {
    
    public static void main(String[] args) {
        int sum1;
        int sum2;
        int sum3;
        int sum4;
        
        sum1 = add( 1, 1 );
        System.out.println("1+1="+sum1);
        sum2 = add( 2, 3 );
        System.out.println("2+3="+sum2);
        sum3 = add( 5, 8 );
        System.out.println("5+8="+sum3);
        sum4 = add( 95, 42 );
        System.out.println("95+42="+sum4);
        
        
    }
    public static int add(int x, int y){
        int sum;
        sum = x + y;
        return sum;
    }
}
