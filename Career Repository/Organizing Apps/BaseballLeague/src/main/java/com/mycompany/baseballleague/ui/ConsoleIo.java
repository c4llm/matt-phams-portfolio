/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.baseballleague.ui;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIo {
    
    private final Scanner sc = new Scanner(System.in);
    
    public void print(String message){
        System.out.println(message);
    }
    
    public String getString(String prompt){
        print(prompt);
        String userInput=sc.nextLine();
        return userInput;
    }

    public int getIntInRange (String prompt, int max, int min){
        print(prompt);
        int integer = sc.nextInt();
        sc.nextLine();
        
        while (integer < min || integer > max){
            print("Please input a number between "+min+" & "+max);
            integer = sc.nextInt();
            sc.nextLine();
        }
        return integer;
    }
    
    public int getInteger (String prompt){
        print(prompt);
        int integer = sc.nextInt();
        sc.nextLine();
        
        return integer;
    }
    
}
