/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.baseballleague.controllers;

import com.mycompany.baseballleague.dao.PlayerDao;
import com.mycompany.baseballleague.dao.TeamDao;
import com.mycompany.baseballleague.dto.PlayerDto;
import com.mycompany.baseballleague.dto.TeamDto;
import com.mycompany.baseballleague.ui.ConsoleIo;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class LeagueController {

    private final ConsoleIo consoleio = new ConsoleIo();
    private final PlayerDao playerdao = new PlayerDao();
    private final TeamDao teamdao = new TeamDao();
    Integer counter = 0;
    Integer counter2 = 0;
    Integer teamId = 0;
    Integer teamId2 = 0;

    public void run() {

        boolean runAgain = true;

        while (runAgain) {
            printMenu();

            int menuSelection = consoleio.getIntInRange("\nPlease select from the above choices (1-7): ", 7, 1);

            switch (menuSelection) {

                case 1:
                    createNewTeam();
                    break;
                case 2:
                    createNewPlayer();
                    break;
                case 3:
                    listAllTeams();
                    break;
                case 4:
                    listAllPlayersOnATeam();
                    break;
                case 5:
                    tradePlayers();
                    break;
                case 6:
                    deletePlayer();
                    break;
                case 7:
                    runAgain = false;
                    consoleio.print("Goodbye");
            }
        }
    }

    private void printMenu() {
        consoleio.print("\n1. create new team\n"
                + "2. create new players\n"
                + "3. list all teams in league\n"
                + "4. list all players on a team\n"
                + "5. trade players\n"
                + "6. delete player\n"
                + "7. exit");
    }

    private void createNewTeam() {

        TeamDto team = new TeamDto();

        String teamName = consoleio.getString("Enter the name of the team: ");

        team.setTeamName(teamName);

        teamdao.create(team);

    }

    private PlayerDto createNewPlayer() {
        int counter = 0;
        PlayerDto player = new PlayerDto();

        String firstName = consoleio.getString("Enter the player's first name: ");
        String lastName = consoleio.getString("Enter the player's last name: ");
        String position = consoleio.getString("Enter the player's position: ");

        List<String> teamList = teamdao.list();
        consoleio.print("Here's a list of teams & their corresponding ID's: ");
        for (String team : teamList) {
            consoleio.print(team);
            counter++;
        }
        consoleio.print("");

        //String currentTeam = consoleio.getString("Enter the player's current team from the list above");
        int teamNumber = consoleio.getIntInRange("Please choose a team number that you'd like to assign your player to: ",counter , 1);

        player.setFirstName(firstName);
        player.setLastName(lastName);
        player.setPosition(position);
        // player.setCurrentTeam(currentTeam);

        player = playerdao.create(player);

        playerdao.placePlayerInTeam(player, teamNumber);

        return player;

    }

    private void listAllTeams() {
        List<String> teamList = teamdao.list();
        consoleio.print("Here's a list of teams & their corresponding ID's: ");
        for (String team : teamList) {
            consoleio.print(team);
            counter++;
        }
    }

    private void listAllPlayersOnATeam() {

        listAllTeams();

        teamId = consoleio.getIntInRange("Please choose a team based on their ID to view roster: ", counter, 0);

        Map<Integer, List> teamMembersMap = playerdao.getMap();

        List<PlayerDto> playerList = teamMembersMap.get(teamId);

        consoleio.print("Here's a list of players & their corresponding ID's: ");
        for (PlayerDto player : playerList) {
            consoleio.print(player.getId().toString() + ": " + player.getFirstName() + " " + player.getLastName() + " " + player.getPosition());
            counter2++;
        }
    }

    private void deletePlayer() {
        listAllPlayersOnATeam();
        Integer playerId = 0;

        playerId = consoleio.getIntInRange("Please choose the ID of the player you wish to delete", counter2, 0);

        Map<Integer, List> teamMembersMap = playerdao.getMap();
        List<PlayerDto> playerList = teamMembersMap.get(teamId);
        for (PlayerDto player : playerList) {
            if (playerId == player.getId()) {
                playerdao.delete(player);
            }
        }
    }

    private void tradePlayers() {
        Integer playerId = 0;
        Integer playerId2 = 0;
        PlayerDto player1 = null;
        PlayerDto player2 = null;
        
        Map<Integer, List> teamMembersMap = playerdao.getMap();
        
        listAllTeams();
        teamId = consoleio.getIntInRange("Please choose a team based on their ID to view roster: ", counter, 0);
        
        List<PlayerDto> playerList = teamMembersMap.get(teamId);
        
        consoleio.print("Here's a list of players & their corresponding ID's: ");
        for (PlayerDto player : playerList) {
            consoleio.print(player.getId().toString() + ": " + player.getFirstName() + " " + player.getLastName() + " " + player.getPosition());
        }

        playerId = consoleio.getIntInRange("Please choose the ID of the player you wish to trade", counter2, 0);

//        Map<Integer, List> teamMembersMap = playerdao.getMap();
//        List<PlayerDto> playerList = teamMembersMap.get(teamId);
        for (PlayerDto player : playerList) {
            if (playerId == player.getId()) {
                player1 = player;
                playerdao.delete(player);
            }
        }
        
        listAllTeams();
        teamId2 = consoleio.getIntInRange("Please choose a team based on their ID to view roster: ", counter, 0);
        
         consoleio.print("Here's a list of players & their corresponding ID's: ");
        for (PlayerDto player : playerList) {
            consoleio.print(player.getId().toString() + ": " + player.getFirstName() + " " + player.getLastName() + " " + player.getPosition());
        }

        
        playerId2 = consoleio.getIntInRange("Please choose the ID of the player you wish to trade", counter2, 0);

        for (PlayerDto players : playerList) {
            if (playerId2 == players.getId()) {
                player2=players;
                playerdao.delete(players);
            }

        }playerdao.placePlayerInTeam(player2, teamId);
        playerdao.placePlayerInTeam(player1,teamId2);
    }
}
