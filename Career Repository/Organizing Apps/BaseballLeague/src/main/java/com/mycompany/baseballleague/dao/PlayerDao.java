/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.baseballleague.dao;

import com.mycompany.baseballleague.dto.PlayerDto;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class PlayerDao {

    final static String DELIMETER = "::";
    final static String FILENAME = "Player.txt";
    TeamDao teamdao = new TeamDao();
    private Map<Integer, List> teamMembers = new HashMap<>();
    List<PlayerDto> roster = new ArrayList<>();

    private static Integer nextId = 1;

//    public PlayerDao() {
//
//        List<PlayerDto> player = decode();
//
//        for (PlayerDto thePlayer : player) {
//          
////
////            if (thePlayer.getId() > nextId) {
////                nextId = thePlayer.getId() + 1;
////            }
//        }
//    }
    public PlayerDto create(PlayerDto player) {

        player.setId(nextId);
        nextId++;

        return player;

    }

    public void delete(PlayerDto player) {
        roster.remove(player);
        encode();
    }

    public List placePlayerInTeam(PlayerDto player, Integer teamId) {

        roster.add(player);
        teamMembers.put(teamId, roster);
        encode();
        return roster;
    }

    public void remove(Integer idNumber) {

    }

    public Map<Integer, List> getMap() {
        return this.teamMembers;
    }

    private void encode() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(FILENAME));
 
            Map<Integer, List> duhMap = getMap();
            List<PlayerDto> playerList = roster;
            Set<Integer> keySet = duhMap.keySet();

            for (Integer i : keySet) {
                    String playerLine = i + DELIMETER;
                for (PlayerDto p : playerList) {
                            playerLine = p.getId() + DELIMETER
                            + p.getFirstName() + DELIMETER
                            + p.getLastName() + DELIMETER
                            + p.getPosition();

                    out.println(playerLine);
                    out.flush();
                }
            }
        } catch (IOException ex) {

        }
    }

    private List<PlayerDto> decode() {
        List<PlayerDto> playerList = new ArrayList<>();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String thisLine = sc.nextLine();
                String[] values = thisLine.split(DELIMETER);

                PlayerDto crib = new PlayerDto();

                crib.setId(Integer.parseInt(values[0]));
                crib.setFirstName(values[1]);
                crib.setLastName(values[2]);

                playerList.add(crib);

            }

        } catch (FileNotFoundException fnf) {

        }

        return playerList;

    }
}
