/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.baseballleague.dao;

import com.mycompany.baseballleague.dto.PlayerDto;
import com.mycompany.baseballleague.dto.TeamDto;
import com.mycompany.baseballleague.ui.ConsoleIo;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class TeamDao {

    final static String DELIMETER = "::";
    final static String FILENAME = "Team.txt";
    private ConsoleIo consoleio = new ConsoleIo();
    private static Integer nextId = 0;
    private Map<Integer, String> listings = new HashMap<>();
//    private Map<Integer, List> teamMembers = new HashMap<>();
    List<PlayerDto> roster = new ArrayList<>();

    public TeamDao() {
        
        Map <Integer, String> teams = decode(); 
        Set<Map.Entry<Integer, String>> set = teams.entrySet();
        
        for (Map.Entry<Integer, String> teamList : set) {
            teams.put(teamList.getKey(), teamList.getValue());
        }
        
        
    }
    
    public TeamDto create(TeamDto team) {
        nextId++;

        team.setId(nextId);
        listings.put(team.getId(), team.getTeamName());

        return team;
    }

    public List<String> list() {

        List<String> teamList = new ArrayList<>();
        Set<Map.Entry<Integer, String>> set = listings.entrySet();

        for (Map.Entry<Integer, String> teams : set) {
            teamList.add(teams.getKey() + ": " + teams.getValue());
        }

        return teamList;

    }

    public void encode() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(FILENAME));

            Set<Map.Entry<Integer, String>> set = listings.entrySet();

            for (Map.Entry<Integer,String>  teams : set){
            
                String teamLine = teams.getKey() + DELIMETER
                        + teams.getValue();
                out.println(teamLine);
                out.flush();
            }

        } catch (IOException ex){
            
        }

    }
    
    private Map<Integer, String> decode() {
        

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String thisLine = sc.nextLine();
                String[] values = thisLine.split(DELIMETER);

                TeamDto teams = new TeamDto();

                teams.setId(Integer.parseInt(values[0]));
                teams.setTeamName(values[1]);
               
                listings.put(teams.getId(), teams.getTeamName());
            }

        } catch (FileNotFoundException fnf) {

        }

        return listings;

    }

}
