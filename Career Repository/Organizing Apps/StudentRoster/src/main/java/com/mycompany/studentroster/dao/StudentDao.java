/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentroster.dao;

import com.mycompany.studentroster.dto.Student;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class StudentDao {

    final static String DELIMETER = "::";
    final static String FILENAME = "student.txt";

    private static Integer nextId = 1;
    private Map<Integer, Student> data = new HashMap<>();
    
    public StudentDao(){
    
        List<Student> students = decode();
        
        for (Student theStudent : students) {
            data.put(theStudent.getId(), theStudent);
            
            if(theStudent.getId() > nextId){
                nextId = theStudent.getId() + 1;
            }
        }
    }

    public Student create(Student student) {

        student.setId(nextId);

        nextId++;

        data.put(student.getId(), student);
        
        encode();

        return student;
        // has id, and if i don't return this, we won't know how to look for the student
    }

    public Student get(Integer id) {

        return data.get(id);
    }

    public void update(Student student) {
        data.put(student.getId(), student);
        encode();
    }

    public void delete(Student student) {
        data.remove(student.getId());
        encode();
    }

    public List<Student> list() {
//            Set<Integer> keySet = data.keySet();

        List<Student> studentList = new ArrayList<>();

        studentList.addAll(data.values());

//            for (Integer key : keySet){
//                studentList.add(data.get(key));
//            }
//            
        return studentList;
    }

    private void encode() {

        try {
            PrintWriter pw = new PrintWriter(new FileWriter(FILENAME));
            
            List<Student> studentList = list();

            for (Student dude : studentList) {

                String line = dude.getId() + DELIMETER
                        + dude.getFirstName() + DELIMETER
                        + dude.getLastName() + DELIMETER
                        + dude.getCohort();
                pw.println(line);
                pw.flush();

            }

        } catch (IOException ex) {

        }

    }

    private List<Student> decode() {

        List<Student> students = new ArrayList<Student>();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                Student dude = new Student();

                dude.setId(Integer.parseInt(values[0]));
                dude.setFirstName(values[1]);
                dude.setLastName(values[2]);
                dude.setCohort(values[3]);

                students.add(dude);

            }

        } catch (FileNotFoundException ex) {

        }

        return students;

    }

}
