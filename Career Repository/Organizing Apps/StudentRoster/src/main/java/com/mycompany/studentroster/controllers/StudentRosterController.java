/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentroster.controllers;

import com.mycompany.studentroster.dao.StudentDao;
import com.mycompany.studentroster.dto.Student;
import com.mycompany.studentroster.ui.ConsoleIo;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class StudentRosterController {

    private final ConsoleIo console = new ConsoleIo();
    private final StudentDao studentDao = new StudentDao();

    public void run() {
        boolean runAgain = true;

        while (runAgain) {

            printMenu();

            int menuSelection = console.getUserInputInt("Please select from the above choices:");

            switch (menuSelection) {

                case 1:

                    ListStudents();
                    break;
                case 2:
                    createStudents();
                    break;
                case 3:
                    viewStudent();
                    break;
                case 4:
                    removeStudent();
                    break;
                case 5:
                    runAgain = false;
                    console.println("Goodbye");
            }
        }
    }

    private void printMenu() {
        console.println("1. List Students");
        console.println("2. Create Students");
        console.println("3. View Students");
        console.println("4. Remove Students");
        console.println("5. Exit");
    }

    private void ListStudents() {
        List<Student> students = studentDao.list();

        for (Student thisStudent : students) {

            printStudent(thisStudent);
        }

        console.getUserInputString("Please hit any button to continue");
    }

    private void printStudent(Student thisStudent) {
        console.println(thisStudent.getId() + ": " + thisStudent.getFirstName() + " " + thisStudent.getLastName() + " " + thisStudent.getCohort());
    }

    private void createStudents() {

        Student stud = new Student();

        String firstName = console.getUserInputString("Please enter first name");
        String lastName = console.getUserInputString("Please enter last name");
        String cohort = console.getUserInputString("Please enter cohort");

        stud.setFirstName(firstName);
        stud.setLastName(lastName);
        stud.setCohort(cohort);

        studentDao.create(stud);

        console.getUserInputString("Please hit any button to continue");

    }

    private void viewStudent() {
        Integer id = console.getUserInputInt("Please enter the ID of the student you wish to view");

        Student thisStudent = studentDao.get(id);

        if (thisStudent != null) {
            printStudent(thisStudent);
        } else {
            console.println("No student with ID: " + id);
        }

        console.getUserInputString("Please hit any button to continue");
    }

    private void removeStudent() {
        Integer id = console.getUserInputInt("Please enter the Id of the student you wish to delete");

        Student thisStudent = studentDao.get(id);

        if (thisStudent != null) {
            studentDao.delete(thisStudent);
            console.getUserInputString("Student successfully removed. Please hit any button to continue");
        } else {
            console.println("No student with ID: " + id);
        }
        console.getUserInputString("Please hit any button to continue");
        
    }
}
