/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.dvdcollection.dao.DvdDaoLambdaImpl;
import com.thesoftwareguild.interfaces.dao.DvdLibraryDao;
import com.thesoftwareguild.interfaces.dto.Dvd;
import java.time.LocalDate;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoTest {

    Dvd dvd = new Dvd();
    DvdLibraryDao library = new DvdDaoLambdaImpl();
    int teardownId;
    int teardownId2;

    public DvdLibraryDaoTest() {

    }

    @Before
    public void setUp() {
        
    }

    @After
    public void tearDown() {
        library.remove(teardownId);
        library.remove(teardownId2);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getByTitleTest(){
        Dvd dvd = new Dvd();
        DvdLibraryDao library = new DvdDaoLambdaImpl();
        
        dvd.setTitle("tim");
        dvd.setDirector("eric");
        dvd.setMpaaRating("R");
        dvd.setStudio("studio");
        LocalDate ld = LocalDate.parse("2016-01-01");
        dvd.setReleaseDate(ld);
        dvd.setNote("cool and stuff");
        
        library.add(dvd);
        
        
        List<Dvd> list;
        
        list = library.getByTitle("tim");
        
        Assert.assertEquals(true, list.contains(dvd));
        library.remove(dvd.getId());
    }
    
    @Test
    public void getByRating(){
        Dvd dvd1 = new Dvd();
        DvdLibraryDao library = new DvdDaoLambdaImpl();
        
        dvd1.setTitle("tim");
        dvd1.setDirector("eric");
        dvd1.setMpaaRating("R");
        dvd1.setStudio("studio");
        LocalDate ld = LocalDate.parse("2016-01-01");
        dvd1.setReleaseDate(ld);
        dvd1.setNote("cool and stuff");
        
        library.add(dvd1);
        
        List<Dvd> list = library.getByRating("R");
        
        Assert.assertEquals(true, list.contains(dvd1));
        library.remove(dvd1.getId());
        
    }
    
    @Test
    public void getByStudio(){
        Dvd dvd1 = new Dvd();
        DvdLibraryDao library = new DvdDaoLambdaImpl();
        
        dvd1.setTitle("tim");
        dvd1.setDirector("eric");
        dvd1.setMpaaRating("R");
        dvd1.setStudio("studio");
        LocalDate ld = LocalDate.parse("2016-01-01");
        dvd1.setReleaseDate(ld);
        dvd1.setNote("cool and stuff");
        
        library.add(dvd1);
        
        List<Dvd> list = library.getByStudio("studio");
        
        Assert.assertEquals(true, list.contains(dvd1));
        library.remove(dvd1.getId());
    }
    
    @Test
    public void update(){
        Dvd dvd1 = new Dvd();
        DvdLibraryDao library = new DvdDaoLambdaImpl();
        
        dvd1.setTitle("tim");
        dvd1.setDirector("eric");
        dvd1.setMpaaRating("R");
        dvd1.setStudio("studio");
        LocalDate ld = LocalDate.parse("2016-01-01");
        dvd1.setReleaseDate(ld);
        dvd1.setNote("cool and stuff");
        
        library.add(dvd1);
        
        dvd1.setTitle("fred");
        library.update(dvd1);
        
        Assert.assertEquals("fred", dvd1.getTitle());
        library.remove(dvd1.getId());
    }
    
    @Test
    public void addTest() {

        dvd.setTitle("JUNGLE");
        LocalDate ld = LocalDate.parse("2015-01-01");
        dvd.setReleaseDate(ld);
        dvd.setMpaaRating("R");
        dvd.setDirector("NICK");
        dvd.setStudio("THEStudio");
        dvd.setNote("AWESOME MOVIE");


        library.add(dvd);
        Assert.assertEquals("JUNGLE", dvd.getTitle());
        Assert.assertEquals(ld, dvd.getReleaseDate());
        Assert.assertEquals("R", dvd.getMpaaRating());
        Assert.assertEquals("NICK", dvd.getDirector());
        Assert.assertEquals("THEStudio", dvd.getStudio());
        Assert.assertEquals("AWESOME MOVIE", dvd.getNote());

        teardownId = dvd.getId();
    }

    @Test
    public void removeTest() {
        Dvd dvd = new Dvd();
        DvdLibraryDao library = new DvdDaoLambdaImpl();
        dvd.setTitle("APPLE");
        LocalDate ld = LocalDate.parse("2015-01-01");
        dvd.setReleaseDate(ld);
        dvd.setMpaaRating("X");
        dvd.setDirector("NICKER");
        dvd.setStudio("StudioX");
        dvd.setNote("SUCKY MOVIE");

        library.add(dvd);
        library.remove(dvd.getId());

        Assert.assertEquals(null, library.getById(dvd.getId()));

//        Assert.assertEquals(null, dvd.getTitle());
//        Assert.assertEquals(null, dvd.getReleaseDate());
//        Assert.assertEquals(null, dvd.getMpaaRating());
//        Assert.assertEquals(null, dvd.getDirector());
//        Assert.assertEquals(null, dvd.getStudio());
//        Assert.assertEquals(null, dvd.getNote());
    }

    @Test
    public void listAllTest() {
        Dvd dvd = new Dvd();
        DvdLibraryDao library = new DvdDaoLambdaImpl();
        List<Dvd> dvdList = library.listAll();

        dvd.setTitle("APPLE");
        LocalDate ld = LocalDate.parse("2015-01-01");
        dvd.setReleaseDate(ld);
        dvd.setMpaaRating("X");
        dvd.setDirector("NICKER");
        dvd.setStudio("StudioX");
        dvd.setNote("SUCKY MOVIE");
        library.add(dvd);
        teardownId2 = dvd.getId();
 
        
        dvd.setTitle("JUNGLE");
        LocalDate lds = LocalDate.parse("2015-01-01");
        dvd.setReleaseDate(lds);
        dvd.setMpaaRating("R");
        dvd.setDirector("NICK");
        dvd.setStudio("THEStudio");
        dvd.setNote("AWESOME MOVIE");
        library.add(dvd);
        teardownId = dvd.getId();

        int expected = dvdList.size();

        Assert.assertEquals(dvdList.size(), expected);

    } 
    
    @Test
    public void getByIdTest(){
        dvd.setTitle("January");
        LocalDate ld = LocalDate.parse("2015-01-01");
        dvd.setReleaseDate(ld);
        dvd.setMpaaRating("G");
        dvd.setDirector("SUPERDIRECTOR");
        dvd.setStudio("StudioMOUNTAINS");
        dvd.setNote("THE BEST MOVIE");
        library.add(dvd);
        
        int id = dvd.getId();
       
        Assert.assertEquals("January" , library.getById(id).getTitle());
        
        teardownId = dvd.getId();
            }

}
