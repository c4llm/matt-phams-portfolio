package com.mycompany.dvdcollection.controller;

//import DAO.DvdDaoImpl;
import com.mycompany.dvdcollection.dao.NoteDao;
import com.thesoftwareguild.interfaces.dto.Dvd;
import com.mycompany.dvdcollection.dto.Note;
import com.mycompany.dvdcollection.ui.ConsoleIO;
import com.thesoftwareguild.interfaces.dao.DvdLibraryDao;
import java.time.LocalDate;

import java.util.List;
//for git push
public class DvdController {

    ConsoleIO io = new ConsoleIO();
    DvdLibraryDao dao;
    NoteDao noteDao = new NoteDao();

    public DvdController(DvdLibraryDao dao) {
        this.dao = dao;
    }

    public void run() {

        String newLine = System.getProperty("line.separator");
        boolean runAgain = true;

        int[] options = {1, 2, 3, 4, 5, 6, 7};
        String error = " Please enter a valid menu choice. ";
        String menu = "" + newLine + " [1] Search Collection "
                + newLine + " [2] Display All "
                + newLine + " [3] Add Dvd "
                + newLine + " [4] Edit Dvd "
                + newLine + " [5] Remove Dvd "
                + newLine + " [6] Show Library Stats "
                + newLine + " [7] Quit "
                + newLine + "Your selection: ";

        while (runAgain) {
            io.println(" Welcome to your Dvd collection. ");

            int menuChoice = io.getUserInputInt(menu, 0, 8);

            switch (menuChoice) {

                case 1:

                    int[] searchOptions = {1, 2, 3, 4, 5, 6, 7};

                    String searchMenu = "" + newLine + " [1] TITLE "
                            + newLine + " [2] ID NUMBER "
                            + newLine + " [3] DIRECTOR "
                            + newLine + " [4] STUDIO "
                            + newLine + " [5] RATING "
                            + newLine + " [6] YEAR "
                            + newLine + " [7] Main Menu "
                            + newLine + "Your selection: ";

                    io.println(" Search by: ");

                    int searchMenuChoice = io.getUserInputInt(searchMenu, 0, 8);

                    switch (searchMenuChoice) {

                        case 1:
                            searchDvdByTitle();
                            break;
                        case 2:
                            getDvdById();
                            break;
//                        case 3:
//                            findAllDvdsByDirector();
//                            break;
                        case 4:
                            findAllDvdsByStudio();
                            break;
                        case 5:
                            findAllDvdsWithMPAA();
                            break;
//                        case 6:
//                            findDvdsByYear();
//                            break;
                        case 7:
                            run();//THIS IS NOT A GOOD WAY TO BREAK OUT OF THE MENU, BUT I'M STUCK
                        //SO HELP ME OLLIE AND MATT, YOU'RE MY ONLY HOPE!
                        default:
                            break;
                    }

                case 2:
                    listDvd();
                    break;
                case 3:
                    addDvd();
                    break;
//                case 4:
//                    editDvd();
//                    break;
                case 5:
                    removeDvd();
                    break;
                case 6:
                    //showLibraryStats();
                    break;
                case 7:
                    runAgain = quit();
                default:
                    break;

            }
        }//END run()
    }

    //CASE 1: SEARCH COLLECTION
    //
    //
    private void searchDvdByTitle() {
        String userInput = io.getUserInputString("Search Movie Titles: ");
        List<Dvd> Dvds = dao.getByTitle(userInput);
        boolean isValid = false;
        if (Dvds.size() > 0) {
            for (Dvd dvd : Dvds) {
                printDvd(dvd);

            }
        } else {
            io.println(" No titles matching search. \n");
        }
    }

    private void printDvd(Dvd dvd) {

        io.println("");
        io.println(dvd.getTitle() + "   (ID# " + dvd.getId() + ")");
        io.println("Year Released: " + dvd.getReleaseDate());
        io.println("Directed By: " + dvd.getDirector());
        io.println(dvd.getStudio());
        io.println("Rated: " + dvd.getMpaaRating());
        io.println("");
        io.println("******** - NOTES - ********");
        for (Note i : noteDao.getNotes(dvd.getId())) {
            io.println(i.getContent());

        }

        io.println("");
        io.println("---------------------------");
    }

    private void printDvdnoID(Dvd dvd) {

        io.println("");
        io.println(dvd.getTitle());
        io.println("Year Released: " + dvd.getReleaseDate());
        io.println("Directed By: " + dvd.getDirector());
        io.println(dvd.getStudio());
        io.println("Rated: " + dvd.getMpaaRating());
        io.println("");
        io.println("******** - NOTES - ********");
        io.println(dvd.getNote());
        io.println("");
        //FOR LOOP HERE PRINTING NOTES FROM NOTEDAO
        if (dvd.getNote() != null) {
            io.println(dvd.getNote());
        }
        io.println("");
        io.println("---------------------------");
    }

    //CASE 2: DISPLAY ALL Dvds IN COLLECTION
    //
    //
    private void listDvd() {
        List<Dvd> Dvds = dao.listAll();

        for (Dvd dvd : Dvds) {
            printDvd(dvd);
        }
    }

    //CASE 3: ADD Dvd TO COLLECTION
    //
    //
    private void addDvd() {

        //New Dvd object created:
        Dvd dvd = new Dvd();
        Note note = new Note();

        //User input written to object:
        String title = io.getUserInputString(" Title: ");
        while (title.equals("")) {
            io.print(" Invalid input. ");
            title = io.getUserInputString(" Title: ");
        }

        String date = io.getUserInputString(" Enter the year this movie released: ");

        String director = io.getUserInputString(" Director: ");
        while (director.equals("")) {
            io.print(" Who directed this movie? Enter the director here: ");
            director = io.getUserInputString(" Director: ");
        }
        String studio = io.getUserInputString(" Studio: ");
        while (studio.equals("")) {
            io.print(" What studio released this movie? Enter it here: ");
            studio = io.getUserInputString(" Studio: ");
        }

        String rating = io.getUserInputString(" MPAA Rating: ");
        while (rating.equals("")) {
            io.print(" Please add a rating: ");
            rating = io.getUserInputString(" MPAA RATING: ");
        }

        String noteToAdd = io.getUserInputString("Note: ");
        while (noteToAdd.equals("")) {
            io.print(" Add any note about this movie you would like. ");
            noteToAdd = io.getUserInputString("Note: ");
        }
        dvd.setDirector(director);
        LocalDate ld = LocalDate.parse(date);
        dvd.setReleaseDate(ld);
        dvd.setMpaaRating(rating);
        note.setContent(noteToAdd);
        dvd.setStudio(studio);
        dvd.setTitle(title);

        dao.add(dvd);
        Integer movieID = dvd.getId();
        note.setMovieId(movieID);
        noteDao.addNote(note);

        boolean wantsAnotherNote = false;
        wantsAnotherNote = (io.getTrueFalse("Would you like to add an additional note? Type y or n. ", "Invalid Input."));
        while (wantsAnotherNote == true) {
            String noteToAdd2 = io.getUserInputString("Note: ");
            while (noteToAdd2.equals("")) {
                io.print(" Add any note about this movie you would like. ");
                noteToAdd2 = io.getUserInputString("Note: ");
            }
            Note bNote = new Note();
            bNote.setContent(noteToAdd2);
            bNote.setMovieId(movieID);
            noteDao.addNote(bNote);
            wantsAnotherNote = (io.getTrueFalse("Would you like to add an additional note? Type y or n. ", "Invalid Input."));
        }

        io.println("Your movie has been added!");

    }

    //CASE 4: EDIT Dvd IN COLLECTION
    //
    //
//    private void editDvd() {
//
//        boolean editValid = false;
//
//        String newLine = System.getProperty("line.separator");
//        String menu = "" + newLine + " [1] Search by TITLE "
//                + newLine + " [2] Quit to Menu ";
//
//        while (!editValid) {
//            io.println(" Please Choose A Menu Selection: ");
//
//            int editChoice = io.getUserInputInt(menu, 0, 3);
//
//            switch (editChoice) {
//
//                case 1:
//                    String userInput = io.getUserInputString("Search Movie Titles: ");
//                    List<Dvd> Dvds = dao.getByTitle(userInput);
//                    boolean isValid = false;
//                    int i = 1;
//                    if (Dvds.size() > 0) {
//                        for (Dvd dvd : Dvds) {
//
//                            io.println("");
//                            io.println("-------------------Movie ID: " + i);
//                            i++;
//                            printDvdnoID(dvd);
//
//                        }
//                        int movieToEdit = io.getUserInputInt(" Movie ID to edit: ", " That's not an ID on file. Try again! ");
//                        movieToEdit = movieToEdit - 1;
//                        Dvd editingDvd = Dvds.get(movieToEdit);
//                        String submenu = "" + newLine + " [1] Edit TITLE "
//                                + newLine + " [2] Edit RELEASE YEAR "
//                                + newLine + " [3] Edit STUDIO "
//                                + newLine + " [4] Edit DIRECTOR "
//                                + newLine + " [5] Edit RATING "
//                                + newLine + " [6] Edit NOTE "
//                                + newLine + " [7] Quit to MENU";
//
//                        while (!editValid) {
//                            io.println(" Please Choose A Menu Selection: ");
//
//                            int subMenuchoice = io.getUserInputInt(submenu, 0, 8);
//                            Dvd editedDvd = new Dvd();
//
//                            switch (subMenuchoice) {
//
//                                case 1: //EDIT TITLE
//                                    io.println("Current Title: " + editingDvd.getTitle());
//
//                                    editingDvd.setTitle(io.getUserInputString("New Title: "));
//                                    break;
//
//                                case 2:// EDIT DATE
//                                    io.println("Current Release Year: " + editingDvd.getReleaseDate());
//
////                                    editingDvd.setDate(io.getUserInputString("New Date: "));
//                                    break;
//
//                                case 3: //EDIT STUDIO
//                                    io.println("Current Studio: " + editingDvd.getStudio());
//
//                                    editingDvd.setStudio(io.getUserInputString("New Studio: "));
//                                    break;
//
//                                case 4: //EDIT DIRECTOR
//                                    io.println("Current Director: " + editingDvd.getDirector());
//
//                                    editingDvd.setDirector(io.getUserInputString("New Director: "));
//                                    break;
//
//                                case 5: //EDIT RATING
//                                    io.println("Current Rating: " + editingDvd.getMpaaRating());
//
//                                    editingDvd.setMpaaRating(io.getUserInputString("New Rating: "));
//                                    break;
//
//                                case 6: //EDIT NOTE
//                                    io.println("Current Note: " + editingDvd.getNote());
//
//                                    editingDvd.setNote(io.getUserInputString("New Note: "));
//
//                                    if (editingDvd.getNote() != null) {
//                                        io.println("Current Second Note: " + editingDvd.getNote());
//                                        editingDvd.setNote(io.getUserInputString("New Second Note: "));
//                                    }
//
//                                    break;
//
//                                case 7: //QUIT TO MENU
//                                    editValid = true;
//
//                            }
//                            dao.editDvd(editingDvd);
//                        }
//
//                    } else {
//                        io.println(" No titles matching search. \n");
//                    }
//
//                    break;
//
//                case 2:
//                    run();
//                    break;
//
//            }
//        }
//
//    }

    //CASE 5: REMOVE Dvd IN COLLECTION
    //
    //
    private void removeDvd() {

        if (!io.getTrueFalse("Do you know the ID# of the movie you'd like to delete? Type y or n. ")) {
            listDvd();
        }

        dao.remove(io.getUserInputInt(" Movie ID to delete: ", " That's not an ID on file. Try again! "));

    }

    //CASE 6: QUIT
    //
    //
    private boolean quit() {
        io.println(" Happy Movie Watching! ");

        try {
            Thread.sleep(1100);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return false;

    }

    private void getDvdById() {
        dao.getById(io.getUserInputInt(" Movie ID: ", " That's not an ID on file. Try again! "));
    }

//    private void findAllDvdsByDirector() {
//        String userInput = io.getUserInputString("Search Movies By Director: ");
//        List<Dvd> Dvds = dao.findAllDvdsByDirector(userInput);
//        boolean isValid = false;
//        if (Dvds.size() > 0) {
//            for (Dvd dvd : Dvds) {
//                printDvd(dvd);
//
//            }
//        } else {
//            io.println(" No titles matching search. \n");
//        }
//    }
    private void findAllDvdsByStudio() {
        String userInput = io.getUserInputString("Search Movies By Studio: ");
        List<Dvd> Dvds = dao.getByStudio(userInput);
        boolean isValid = false;
        if (Dvds.size() > 0) {
            for (Dvd dvd : Dvds) {
                printDvd(dvd);

            }
        } else {
            io.println(" No titles matching search. \n");
        }
    }

    private void findAllDvdsWithMPAA() {

        String[] mpaaRatings = {"G", "PG", "PG", "PG-13", "PG13", "R", "X", "XXX", "NC-17", "NC17", "UNRATED", "NR"};

        String userInput = io.getUserInputString("Search Movies By Rating: ", mpaaRatings, " That is not an official MPAA rating. Please try again. ");
        List<Dvd> Dvds = dao.getByRating(userInput);
        boolean isValid = false;
        if (Dvds.size() > 0) {
            for (Dvd dvd : Dvds) {
                printDvd(dvd);

            }
        } else {
            io.println(" No titles matching ratings search. \n");
        }
    }

    private void findDvdsNewerThan() {
        io.println("GREG HASN'T BUILT THIS PART YET.");
    }

//    private void findDvdsByYear() {
//        int userInput = io.getUserInputYear("Search Movies By Release Year: ");
//        List<Dvd> Dvds = dao.findDvdsByYear(userInput);
//        boolean isValid = false;
//        if (Dvds.size() > 0) {
//            for (Dvd dvd : Dvds) {
//                printDvd(dvd);
//
//            }
//        } else {
//            io.println(" No titles in the database with that release year.");
//        }
//    }

}//END of ControllerClass

