package com.mycompany.dvdcollection.dao;

import com.mycompany.dvdcollection.dto.Note;
import com.thesoftwareguild.interfaces.dao.DvdLibraryDao;
import com.thesoftwareguild.interfaces.dto.Dvd;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
//for git push
/**
 *
 * @author Ollie
 */
public class DvdDaoLambdaImpl implements DvdLibraryDao {

    private Map<Integer, Dvd> dvdMap = new HashMap<>();
    final String DELIMETER = "::";
    final String FILENAME = "DvdLibrary.txt";
    private Integer nextId = 1;
    private Dvd dvd = new Dvd();

    public DvdDaoLambdaImpl() {

        List<Dvd> dvds = decode();

        dvds.forEach(i -> {
            dvdMap.put(i.getId(), i);
            if (i.getId() > nextId) {
                nextId = i.getId() + 1;
            }
        });

    }

//    @Override
//    public Dvd addDvd(Dvd dvd) {
//        dvd.setId(getNextID());
//        dvdMap.put(dvd.getId(), dvd);
//        encode();
//        return dvd;
//    }
    private int getNextID() {
        nextId++;
        return nextId;
    }
//
//    public void editDvd(Dvd dvd) {
//        dvdMap.replace(dvd.getId(), dvd);
//        encode();
//    }

//    @Override
//    public Dvd getDvdById(int id) {
//        return dvdMap.get(id);
//    }
//    @Override
//    public List<Dvd> listAll() {
//        List<Dvd> dvds = new ArrayList<>(dvdMap.values());
//        return dvds;
//    }
//    @Override
//    public List<Dvd> listAllByTitle(String title) {
//
//        List<Dvd> dvds = listAll();
//
//        List<Dvd> newList = dvds.stream()
//                .filter(dvd -> dvd.getTitle().equalsIgnoreCase(title))
//                .collect(Collectors.toList());
//
//        return newList;
//
//    }
//    @Override
//    public void removeDvd(Integer id) {
//        dvdMap.remove(id);
//        encode();
//
//    }
    private List<Dvd> decode() {
        List<Dvd> dvds = new ArrayList<>();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                Dvd dvd = new Dvd();

                dvd.setId(Integer.parseInt(values[0]));
                dvd.setTitle(values[1]);
                LocalDate ld = LocalDate.parse(values[2]);
                dvd.setReleaseDate(ld);
                dvd.setDirector(values[3]);
                dvd.setStudio(values[4]);
                dvd.setMpaaRating(values[5]);

                dvds.add(dvd);

            }

        } catch (FileNotFoundException ex) {

        }
        return dvds;
    }

    /**
     *
     * @param age : number of years from now
     * @return returns Dvd list of Dvd's newer than the parameter years
     */
    public List<Dvd> findDvdsNewerThan(Integer age) {

        List<Dvd> dvdList = listAll();
        List<Dvd> newDvds = dvdList.stream().filter(s -> s.getReleaseDate().getYear() < age).collect(Collectors.toList());
        return newDvds;

    }

    /**
     *
     * @param rating
     * @return List of Dvd's with given rating
     */
//    @Override
//    public List<Dvd> findAllDvdsWithMPAA(String rating) {
//
//        List<Dvd> dvdList = listAll();
//        List<Dvd> newList = dvdList.stream()
//                .filter(dvd -> dvd.getRating().equalsIgnoreCase(rating))
//                .collect(Collectors.toList());
//
//        return newList;
//    }
    /**
     *
     * @param director
     * @return List of directors sorted by MPAA rating
     */
    public List<Dvd> findAllDvdsByDirector(String director) {

        List<Dvd> newDvdList = listAll();

        List<Dvd> newList = newDvdList
                .stream().filter(dvd -> dvd.getDirector().equalsIgnoreCase(director))
                .sorted((Dvd A, Dvd B) -> A.getDirector().compareTo(B.getDirector()))
                .collect(Collectors.toList());

        return newList;

    }

//    @Override
//    public List<Dvd> findAllDvdsByStudio(String studio) {
//
//        List<Dvd> studioList = listAll();
//
//        List<Dvd> newList = studioList
//                .stream().filter(dvd -> dvd.getStudio().equalsIgnoreCase(studio))
//                .sorted((Dvd A, Dvd B) -> A.getStudio().compareTo(B.getStudio()))
//                .collect(Collectors.toList());
//
//        return newList;
//
//    }
    public List<Dvd> findDvdsByYear(Integer year) {

        List<Dvd> yearList = listAll();

        List<Dvd> newList = yearList
                .stream().filter(s -> s.getReleaseDate().getYear() < year)
                .collect(Collectors.toList());

        return newList;

    }

    /**
     *
     * @return average age of movies in list as double
     */
    public double findAverageAge() {

        List<Dvd> dvdAgeList = listAll();

        double averageAge = dvdAgeList.stream().mapToLong(dvd -> (Calendar.getInstance().get(Calendar.YEAR)) - dvd.getReleaseDate().getYear()).average().getAsDouble();

        return averageAge;
    }

    public Dvd findNewestDvd() {

        List<Dvd> dvdYoungest = listAll();

        return dvdYoungest.stream()
                .min((a, b) -> Long.compare(a.getReleaseDate().getYear(), b.getReleaseDate().getYear()))
                .get();

    }

    public Dvd findOldestDvd() {

        List<Dvd> dvdOldest = listAll();


        return dvdOldest.stream()
                .max((a, b) -> Long.compare(a.getReleaseDate().getYear(), b.getReleaseDate().getYear()))
                .get();

    }

    public double findAverageNotes() {
        NoteDao noteDao = new NoteDao();
        ArrayList<Note> notesArrayList = noteDao.getAllNotesList();

        return notesArrayList.size() / dvdMap.size();

    }

    private void encode() {

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILENAME)));
            List<Dvd> dvds = listAll();
            String line = "";
            for (Dvd dvd : dvds) {

                line = dvd.getId() + DELIMETER
                        + dvd.getTitle() + DELIMETER
                        + dvd.getReleaseDate() + DELIMETER
                        + dvd.getDirector() + DELIMETER
                        + dvd.getStudio() + DELIMETER
                        + dvd.getMpaaRating();

                out.println(line);
                out.flush();
            }

            out.close();
        } catch (IOException e) {

        }

    }

    @Override
    public void add(Dvd dvd) {
        dvd.setId(getNextID());
        dvdMap.put(dvd.getId(), dvd);
        encode();
    }

    @Override
    public void remove(int id) {
        dvdMap.remove(id);
        Dvd dvds = getById(id);
        dvds = null;

        encode();
    }

    @Override
    public List<Dvd> listAll() {
        List<Dvd> dvds = new ArrayList<>(dvdMap.values());
        return dvds;

    }

    @Override
    public Dvd getById(int id) {
        return dvdMap.get(id);
    }

    @Override
    public List<Dvd> getByTitle(String title) {

        List<Dvd> dvds = listAll();

        List<Dvd> newList = dvds.stream()
                .filter(dvd -> dvd.getTitle().equalsIgnoreCase(title))
                .collect(Collectors.toList());

        return newList;
    }

    @Override
    public List<Dvd> getByRating(String rating) {

        List<Dvd> dvdList = listAll();
        List<Dvd> newList = dvdList.stream()
                .filter(dvd -> dvd.getMpaaRating().equalsIgnoreCase(rating))
                .collect(Collectors.toList());

        return newList;
    }

    @Override
    public List<Dvd> getByStudio(String studio) {

        List<Dvd> studioList = listAll();

        List<Dvd> newList = studioList
                .stream().filter(dvd -> dvd.getStudio().equalsIgnoreCase(studio))
                .sorted((Dvd A, Dvd B) -> A.getStudio().compareTo(B.getStudio()))
                .collect(Collectors.toList());

        return newList;
    }

    @Override
    public void update(Dvd dvd) {
        dvdMap.replace(dvd.getId(), dvd);
        encode();
    }

}
