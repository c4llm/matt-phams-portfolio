package com.mycompany.dvdcollection.dao;

import com.mycompany.dvdcollection.dto.Note;
import com.thesoftwareguild.interfaces.dao.DvdLibraryDao;
import com.thesoftwareguild.interfaces.dto.Dvd;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
//for git push
public class DvdDaoImpl implements DvdLibraryDao {

    private Map<Integer, Dvd> dvdMap = new HashMap<>();
    final String DELIMETER = "::";
    final String FILENAME = "DvdLibrary.txt";
    private Integer nextId = 1;

    public DvdDaoImpl() {

        List<Dvd> dvds = decode();

        for (Dvd i : dvds) {
            dvdMap.put(i.getId(), i);

            if (i.getId() > nextId) {
                nextId = i.getId() + 1;
            }

        }
    }

    private int getNextID() {
        nextId++;
        return nextId;
    }
//
//    @Override
//    public Dvd addDvd(Dvd dvd) {
//        dvd.setId(getNextID());
//        dvdMap.put(dvd.getId(), dvd);
//        encode();
//        return dvd;
//    }
//
//    @Override
//    public void removeDvd(Integer id) {
//        dvdMap.remove(id);
//
//        encode();
//    }
//
//    @Override
//    public List<Dvd> listAll() {
//        List<Dvd> dvds = new ArrayList<>(dvdMap.values());
//        return dvds;
//    }
//
//    @Override
//    public List<Dvd> listAllByTitle(String title) {
//        ArrayList<Dvd> dvds = new ArrayList<>();
//
//        for (Dvd dvd : listAll()) {
//            if (title.toLowerCase().equals(dvd.getTitle().toLowerCase())) {
//
//                dvds.add(dvd);
//
//            }
//        }
//        return dvds;
//    }

//    @Override
//    public Dvd getDvdById(int id) {
//        return dvdMap.get(id);
//
//    }
//    public void editDvd(Dvd dvd) {
//        dvdMap.replace(dvd.getId(), dvd);
//        encode();
//
//    }

    // DECODE     
    private List<Dvd> decode() {
        List<Dvd> dvds = new ArrayList<>();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);

                Dvd dvd = new Dvd();

                dvd.setId(Integer.parseInt(values[0]));
                dvd.setTitle(values[1]);
                LocalDate ld = LocalDate.parse(values[2]);
                dvd.setReleaseDate(ld);
                dvd.setDirector(values[3]);
                dvd.setStudio(values[4]);
                dvd.setMpaaRating(values[5]);

                dvds.add(dvd);

            }

        } catch (FileNotFoundException ex) {

        }
        return dvds;
    }

    // ENCODE
    private void encode() {

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILENAME)));
            List<Dvd> dvds = listAll();
            String line = "";
            for (Dvd dvd : dvds) {

                line = dvd.getId() + DELIMETER
                        + dvd.getTitle() + DELIMETER
                        + dvd.getReleaseDate() + DELIMETER
                        + dvd.getDirector() + DELIMETER
                        + dvd.getStudio() + DELIMETER
                        + dvd.getMpaaRating();

                out.println(line);
                out.flush();
            }

            out.close();
        } catch (IOException e) {

        }

    }

    public List<Dvd> findDvdsNewerThan(Integer age) {
        List<Dvd> dvdList = listAll();
        List<Dvd> newestDvds = new ArrayList<>();
        for (Dvd newest : dvdList) {
            if (2016 - (newest.getReleaseDate().getYear()) > age) {
                newestDvds.add(newest);
            }
        }
        return newestDvds;
    }

    public List<Dvd> findDvdsByYear(Integer year) {
        List<Dvd> dvdList = listAll();
        List<Dvd> years = new ArrayList<>();
        for (Dvd byYear : dvdList) {
            if (byYear.getReleaseDate().getYear() == year) {
                years.add(byYear);
            }
        }
        return years;
    }

//    public List<Dvd> findAllDvdsWithMPAA(String rating) {
//        List<Dvd> dvdList = listAll();
//        List<Dvd> mpaa = new ArrayList<>();
//        for (Dvd ratings : dvdList) {
//            if (ratings.getMpaaRating().equalsIgnoreCase(rating)) {
//                mpaa.add(ratings);
//            }
//        }
//        return mpaa;
//    }
    public List<Dvd> findAllDvdsByDirector(String director) {
        List<Dvd> dvdList = listAll();
        List<Dvd> direct = new ArrayList<>();
        for (Dvd d : dvdList) {
            if (d.getMpaaRating().equalsIgnoreCase(director)) {
                direct.add(d);
            }
        }
        return direct;
    }

    public double findAverageAge() {
        double total;
        double trueTotal = 0;
        List<Dvd> dvdList = listAll();
        List<Double> average = new ArrayList<>();
        for (Dvd age : dvdList) {
            total = LocalDate.now().getYear() - age.getReleaseDate().getYear();
            average.add(total);
        }
        for (Double ages : average) {
            trueTotal += ages;
        }
        double trueAverage = trueTotal / (average.size());
        return trueAverage;
    }

    public double findAverageNotes() {
        NoteDao noteDao = new NoteDao();
        ArrayList<Note> notesArrayList = noteDao.getAllNotesList();

        return notesArrayList.size() / dvdMap.size();
    }
//
//    @Override
//    public List<Dvd> findAllDvdsByStudio(String studio) {
//        List<Dvd> dvdList = listAll();
//        List<Dvd> stud = new ArrayList<>();
//        for (Dvd s : dvdList) {
//            if (s.getStudio().equalsIgnoreCase(studio)) {
//                stud.add(s);
//            }
//        }
//        return stud;
//    }

    @Override
    public void add(Dvd dvd) {
        dvd.setId(getNextID());
        dvdMap.put(dvd.getId(), dvd);
        encode();
    }

    @Override
    public void remove(int id) {
        dvdMap.remove(id);
        encode();
    }

    @Override
    public List<Dvd> listAll() {
        List<Dvd> dvds = new ArrayList<>(dvdMap.values());
        return dvds;
    }

    @Override
    public Dvd getById(int id) {
        return dvdMap.get(id);
    }

    @Override
    public List<Dvd> getByTitle(String title) {
        ArrayList<Dvd> dvds = new ArrayList<>();

        for (Dvd dvd : listAll()) {
            if (title.toLowerCase().equals(dvd.getTitle().toLowerCase())) {

                dvds.add(dvd);

            }
        }
        return dvds;
    }

    @Override
    public List<Dvd> getByRating(String rating) {
        List<Dvd> dvdList = listAll();
        List<Dvd> mpaa = new ArrayList<>();
        for (Dvd ratings : dvdList) {
            if (ratings.getMpaaRating().equalsIgnoreCase(rating)) {
                mpaa.add(ratings);
            }
        }
        return mpaa;
    }

    @Override
    public List<Dvd> getByStudio(String studio) {
        List<Dvd> dvdList = listAll();
        List<Dvd> stud = new ArrayList<>();
        for (Dvd s : dvdList) {
            if (s.getStudio().equalsIgnoreCase(studio)) {
                stud.add(s);
            }
        }
        return stud;
    }

    @Override
    public void update(Dvd dvd) {
        dvdMap.replace(dvd.getId(), dvd);
        encode();
    }

}//END of DaoClass

