package com.mycompany.dvdcollection.app;


import com.mycompany.dvdcollection.controller.DvdController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//for git push
public class App {

    public static void main(String[] args) {
       ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
       DvdController dvdController = ctx.getBean("lambdaController",DvdController.class);

       dvdController.run();






    }//END of Main
}// END of AppClass

