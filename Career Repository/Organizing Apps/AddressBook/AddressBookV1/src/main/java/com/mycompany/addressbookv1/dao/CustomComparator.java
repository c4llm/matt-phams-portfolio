package com.mycompany.addressbookv1.dao;

import com.thesoftwareguild.interfaces.dto.Address;
import java.util.Comparator;

/**
 *
 * @author apprentice
 */
public class CustomComparator implements Comparator<Address> {
//for git push
    @Override
    public int compare(Address a, Address b) {
        return a.getCity().compareTo(b.getCity());
    }
    
}


