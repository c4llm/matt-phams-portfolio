package com.mycompany.addressbookv1.dao;

import com.mycompany.addressbookv1.dto.Address;
import java.util.List;
import java.util.Set;

/**
 *
 * @author apprentice
 */
//for git push
public interface AddressDAO {

    Address addAddress(Address address);

    Set<Integer> deleteAddress(Integer id);

    Integer getNextId();

    List<Address> list();
    
    List<Address> searchByName(String lastName);
    
    List<Address> searchByCity(String city);
    
    List<Address> searchByState(String state);
    
    List<Address> searchByZip(String zip);
    
    
    
}
