package com.mycompany.addressbookv1.app;

import com.mycompany.addressbookv1.controller.Controller;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
      //for git push  
     
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        
        Controller addressController = ctx.getBean("lambController",Controller.class);
        
        addressController.run();
                

    }

}
