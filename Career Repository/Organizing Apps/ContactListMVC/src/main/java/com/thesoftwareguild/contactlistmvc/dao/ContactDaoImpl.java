/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.contactlistmvc.dao;

import com.thesoftwareguild.contactlistmvc.models.Contact;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class ContactDaoImpl implements ContactDao {

    private static Integer nextId=0;
    private Map<Integer,Contact> contacts = new HashMap();
    
    
    public ContactDaoImpl (){
        
        Contact contact1 = new Contact();
        contact1.setFirstName("Bill");
        contact1.setLastName("Jones");
        contact1.setCompany("Microsoft");
        contact1.setPhone("111-888-0909");
        contact1.setEmail("blah@jonn.com");
        add(contact1);
        
        Contact contact2 = new Contact();
        contact2.setFirstName("Jerry");
        contact2.setLastName("Smith");
        contact2.setCompany("Oracle");
        contact2.setPhone("111-222-0909");
        contact2.setEmail("asdf@asdf.com");
        add(contact2);
        
        Contact contact3 = new Contact();
        contact3.setFirstName("Sally");
        contact3.setLastName("Red");
        contact3.setCompany("The");
        contact3.setPhone("222-222-0909");
        contact3.setEmail("my.email.com");
        add(contact3);
        
    }
    
    
    @Override
    public Contact add(Contact contact) {
        
        nextId++;
        contact.setId(nextId);
        contacts.put(contact.getId(),contact);
        return contact;
        
    }

    @Override
    public Contact get(Integer id) {
        return contacts.get(id);
    }

    @Override
    public void remove(Integer id) {
     contacts.remove(id);
    }

    @Override
    public void update(Contact contact) {
        contacts.put(contact.getId(), contact);
    }

    @Override
    public List<Contact> list() {
        return new ArrayList(contacts.values());
    }
    
}
