/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.contactlistmvc.dao;

import com.thesoftwareguild.contactlistmvc.models.Contact;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface ContactDao {
    
    public Contact add(Contact contact);
    public Contact get(Integer id);
    public void remove(Integer id);
    public void update(Contact contact);
    public List<Contact> list();
}
