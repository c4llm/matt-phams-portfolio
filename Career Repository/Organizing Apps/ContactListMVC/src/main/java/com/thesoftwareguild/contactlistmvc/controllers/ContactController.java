/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.contactlistmvc.controllers;

import com.thesoftwareguild.contactlistmvc.dao.ContactDao;
import com.thesoftwareguild.contactlistmvc.models.Contact;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/contact")
public class ContactController {

    private ContactDao contactDao;

    @Inject
    public ContactController(ContactDao dao) {
        this.contactDao = dao;
    }
    
    @RequestMapping(value="/add" , method= RequestMethod.POST)
    public String add(@ModelAttribute Contact contact, BindingResult bindingResult, Model model){
        
        contactDao.add(contact);
        return "redirect:/";
  }
    
    @RequestMapping(value="/view/{id}")
    public String show(@PathVariable("id") Integer contactId, Model model){
        
        Contact contact = contactDao.get(contactId);
        
        model.addAttribute("contact",contact);
        
        return "viewContact";
    }
    
    @RequestMapping(value="/delete/{id}")
    public String delete(@PathVariable("id") Integer contactId){
        
        contactDao.remove(contactId);
        return "redirect:/";
        
    }
    
    @RequestMapping(value="showEdit/{id}")
    public String showEdit(@PathVariable("id") Integer contactId, Model model){
       
        Contact contact = contactDao.get(contactId);
        
        model.addAttribute("contact", contact);
        
        return "showEdit";
        
    }
    
      @RequestMapping(value="/edit/{id}" , method= RequestMethod.POST)
    public String edit(@ModelAttribute Contact contact,@PathVariable("id") Integer contactId, BindingResult bindingResult, Model model){
        
        contactDao.update(contact);
        
        return "redirect:/";
  }
}
