/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankaccountmanager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Account {

    protected double balance;
    protected List<Double> pending = new ArrayList<>();

    public boolean withdraw(double amount, double penalty, boolean checking) {
        if (balance > 0 && checking && balance-amount >= -100) {
            balance -= balance < 0 ? 10+amount: amount;
            return true;
        } else if (balance >=0 && !checking && balance-amount>0){
            balance -= amount - ((penalty/100)*amount);
            return true;
        } else
        return false;
    }
    
        

    public boolean deposit(double amount) {
        if (amount>10000){
            getPending().add(amount);
        } else {
            balance+=amount;
        }
        return amount < 10000;
    }

public List getPending(){
    return pending;
}    
    
public void clearPending(){
    for (Double deposit : pending){
        balance+=deposit;
    }
}

    /**
     * @return the balance
     */
    public String getBalance() {
        DecimalFormat myFormatMoney = new DecimalFormat ("$###,###.##");
        return myFormatMoney.format(balance);
    }

}
