/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankaccountmanager;

/**
 *
 * @author apprentice
 */
public class SavingsAccount extends Account implements AccountInterface {
    private double interestRatePercent;
    private double penaltyFeePercent;
    
    public SavingsAccount (double interestRate, double penalty){
        this.interestRatePercent = interestRate;
        this.penaltyFeePercent = penalty;
    }
       
    public boolean withdraw(double amount){
        return super.withdraw(amount, penaltyFeePercent , false);
    }
    
    public boolean deposit(double amount){
        return super.deposit(amount);
    }
    
    public String getBalance(){
        return super.getBalance();
    }
    
    public void timeTravel(int years){
    for(int i = 0; i < years ; i++)
        balance = calculateAYearsInterest();
    }
    private double calculateAYearsInterest(){
        return balance+((interestRatePercent/100)*balance);
    }
    
}
