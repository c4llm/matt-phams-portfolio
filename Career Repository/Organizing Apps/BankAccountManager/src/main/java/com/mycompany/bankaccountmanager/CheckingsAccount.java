/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankaccountmanager;

/**
 *
 * @author apprentice
 */
public class CheckingsAccount extends Account implements AccountInterface {
     
    public boolean withdraw(double amount){
        return super.withdraw(amount, 0, true);
    }
      
    public boolean deposit(double amount){
        return super.deposit(amount);
    }
    
    public String getBalance(){
        return super.getBalance();
    }
}
