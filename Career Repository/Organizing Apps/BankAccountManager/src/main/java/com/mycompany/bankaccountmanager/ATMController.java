/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankaccountmanager;

/**
 *
 * @author apprentice
 */
public class ATMController {

    private final double INTEREST_RATE_PERCENT = 2.5;
    private final double PENALTY_PERCENT = 1;

    ConsoleIo console = new ConsoleIo();
    CheckingsAccount cAccount = new CheckingsAccount();
    SavingsAccount sAccount = new SavingsAccount(INTEREST_RATE_PERCENT, PENALTY_PERCENT);

    public void run() {
        String pin;
        do {
            pin = console.getUserInputString("Pin: ");
            pin = pin.replaceAll("[\\D]", "");
        } while (!pin.equals("1337") && !(pin.length() == 4));

        console.print("SUCCESS!!!");
        menu();
    }

    public void menu() {
        String selection;
        do {
            selection = console.getMultiChoiceString("Please handle your money", new String[]{"Checkings", "Savings", "TimeTravel", "Ask for pending clearance", "Exit(Please take your card)"});

            switch (selection) {
                case "Checking":
                    accessChecking(cAccount);
                    break;
                case "Savings":
                    accessSavings(sAccount);
                    break;
                case "Time Travel":
                    accessTimeTravel();
                    break;
                case "Ask for pending clearance":
                    accessForClearance();
                    break;
                case "Exit (Please take your card)":
                    return;
            }
        } while (!selection.equals("Exit(Please take your card)"));
    }

    private void accessChecking(AccountInterface account) {
        accountInteraction(account, "Checkings");
    }

    private void accessSavings(AccountInterface account) {
        accountInteraction(account, "Savings");
    }

    private void accessTimeTravel() {
        sAccount.timeTravel(console.getUserInputInt("How many years would you like to time travel: ", true, 0, false));
    }

    private void accessForClearance() {
        console.println("Clearance granted!");
        cAccount.clearPending();
        sAccount.clearPending();
    }

    private void accountInteraction(AccountInterface Account, String accName) {
        String selection;
        boolean success;
        do {
            selection = console.getMultiChoiceListString("Checking: " + cAccount.getBalance(), new String[]{"Withdraw", "Deposit", "Return"});

            switch (selection) {
                case "Withdraw":
                    success = cAccount.withdraw(console.getUserInputDouble("How much: $", true, 0, false));
                    console.println("The withdraw has" + (success ? "Succeeded!" : "FAILED!"));
                    break;
                case "Deposit":
                    success = cAccount.deposit(console.getUserInputDouble("How much: $", true, 0, false));
                    console.println("The deposit has" + (success ? "Succeeded!" : "is pending please seek clearance"));
                    break;
                case "Return":
                    return;
            }
        } while (!selection.equals("Return"));
    }

}
