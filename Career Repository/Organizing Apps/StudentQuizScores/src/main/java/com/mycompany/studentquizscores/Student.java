/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentquizscores;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Student {

    private List quizzes = new ArrayList<>();
    private QuizScoresHashmap quiz = new QuizScoresHashmap();
    private ConsoleIo consoleio = new ConsoleIo();
    private String name = "";
    private boolean again = true;

    public Student() {

        name = consoleio.getString("Please input a student name: ");

        do {
            quizzes.add(consoleio.getNumberBetweenMaxAndMin("Please insert quiz scores: ", 0, 100));
        } while (again);
    }

    /**
     * @return the quizzes
     */
    public List getQuizzes() {
        return quizzes;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

}
