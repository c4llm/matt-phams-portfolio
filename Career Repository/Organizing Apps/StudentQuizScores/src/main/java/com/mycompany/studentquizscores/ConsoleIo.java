/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentquizscores;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIo {

    private Scanner scan = new Scanner(System.in);

    public void print(String message) {
        System.out.println(message);
    }

    public int getInteger(String prompt) {
        print(prompt);
        int integer = scan.nextInt();
        return integer;
    }

    public int getNumberBetweenMaxAndMin(String prompt, int max, int min) {
        print(prompt);
        int integer = scan.nextInt();

        while (integer < min || integer > max) {
            print("Please input a number between " + min + " & " + max);
            integer = scan.nextInt();
        }
        return integer;
    }

    public String getString(String prompt) {
        print(prompt);
        String string = scan.next();
        string = string.toLowerCase();
        return string;

    }

    public boolean getYesOrNo(String prompt) {
        boolean answer = true;
        String string = "";

        while (!string.equals("yes") || !string.equals("no")) {
            print(prompt);
            string = scan.next();
            string = string.toLowerCase();
            switch (string) {
                case "yes":
                    answer = true;
                    break;
                case "no":
                    answer = false;
                    break;
            }

        }
        return answer;
    }

    public float getFloat(String prompt) {
        print(prompt);
        float numberFloat = scan.nextFloat();
        return numberFloat;

    }

    public float getFloatBetweenMaxAndMin(String prompt, float max, float min) {
        print(prompt);
        float numberFloat = scan.nextFloat();

        while (numberFloat < min || numberFloat > max) {
            print("Please input a number between " + min + " & " + max);
            numberFloat = scan.nextFloat();
        }
        return numberFloat;
    }

    public double getDouble(String prompt) {
        print(prompt);
        double numberDouble = scan.nextDouble();
        return numberDouble;

    }

    public double getDoubleBetweenMaxAndMin(String prompt, double max, double min) {
        print(prompt);
        double numberDouble = scan.nextDouble();

        while (numberDouble < min || numberDouble > max) {
            print("Please input a number between " + min + " & " + max);
            numberDouble = scan.nextDouble();
        }
        return numberDouble;
    }

}
