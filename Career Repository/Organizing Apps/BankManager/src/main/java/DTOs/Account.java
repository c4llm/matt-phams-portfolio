/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTOs;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Account {
    protected double balance;
    private final List<Double> pending = new ArrayList<>();

    public boolean withdraw(double amount, double penalty, boolean checking) {
        if (balance > 0 && checking && balance-amount >= -100) {
            balance -= balance < 0 ? 10 + amount : amount;
            return true;
        } else if (balance >= 0 && !checking && balance - amount >= 0) {
            balance -= amount-((penalty/100)*amount);
            return true;
        } else {
            return false;
        }
    }

    public boolean deposit(double amount) {        
        if (amount > 10000) {
            getPending().add(amount);
        } else{
            balance += amount;
        }
        return amount < 10000;
    }
    
    public void clearPending(){
        for (Double deposit : getPending()){
            balance+=deposit;
        }
    }

    public String getBalance() {
        DecimalFormat formatMoney = new DecimalFormat("$###,###.##");
        return formatMoney.format(balance);
    }

    /**
     * @return the pending
     */
    public List<Double> getPending() {
        return pending;
    }
}
