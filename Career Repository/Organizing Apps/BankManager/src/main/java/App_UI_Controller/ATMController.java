/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App_UI_Controller;

import DAOs.SavingsAccount;
import DAOs.CheckingAccount;
import DAOs.AccountInterface;
import DTOs.Account;

/**
 *
 * @author apprentice
 */
public class ATMController {

    private final double INTEREST_RATE_PERCENT = 2.5;
    private final double PENALTY_PERCENT = 1;

    private ConsoleIO io = new ConsoleIO();
    private CheckingAccount cAccount = new CheckingAccount();
    private SavingsAccount sAccount = new SavingsAccount(INTEREST_RATE_PERCENT, PENALTY_PERCENT);
    private Object account;

    public void run() {
        String pin;
        do {
            pin = io.getUserInputString("Pin: ");
            pin = pin.replaceAll("[\\D]", "");
        } while (!pin.equals("1337") && !(pin.length() == 4));
        io.println("login success");

        menu();

    }

    public void menu() {
        String selection;
        do {
            selection = io.getMultiChoiceString("Please handle your money:", new String[]{"Checking", "Savings", "Time Travel", "Ask for pending clearance", "Exit (Please take your card)"});
            switch (selection) {
                case "Checking":
                    accessChecking(cAccount);
                    break;
                case "Savings":
                    accessSavings(sAccount);
                    break;
                case "Time Travel":
                    accessTimeTravel();
                    break;
                case "Ask for pending clearance":
                    askForClearance();
                    break;
                case "Exit (Please take your card)":
                    return;
            }
        } while (!selection.equals("Exit (Please take your card)"));
    }

    private void accessChecking(AccountInterface account) {
        accountInteraction(account, "Checking");
    }

    private void accessSavings(AccountInterface account) {
        accountInteraction(account, "Savings");
    }

    private void accessTimeTravel() {
        sAccount.timeTravel(io.getUserInputInt("How many years would you like to time travel: ", true, 0, false));
    }

    private void askForClearance() {
        io.println("Clearance granted");
        sAccount.clearPending();
        cAccount.clearPending();
    }

    private void accountInteraction(AccountInterface account, String accName) {
        String selection;
        boolean success;

        do {
            selection = io.getMultiChoiceString(accName + ": " + account.getBalance(), new String[]{"Withdraw", "Deposit", "Return"});

            switch (selection) {
                case "Withdraw":
                    success = account.withdrawl(io.getUserInputDouble("How much: $", true, 0, false));
                    io.println("The withdraw has " + (success ? "succeeded!" : "failed..."));
                    break;
                case "Deposit":
                    success = account.deposit(io.getUserInputDouble("How much: $", true, 0, false));
                    io.println("The deposit " + (success ? "has succeeded!" : "is pending please seek clearance..."));
                    break;
                case "Return":
                    return;
            }
        } while (!selection.equals("Return"));
    }
}
