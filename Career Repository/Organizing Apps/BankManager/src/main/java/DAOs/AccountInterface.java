/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

/**
 *
 * @author apprentice
 */
public interface AccountInterface {

    boolean withdrawl(double amount);
    
    boolean deposit(double amount);
    
    String getBalance();
    
}
