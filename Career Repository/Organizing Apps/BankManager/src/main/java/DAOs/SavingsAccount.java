/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import DAOs.AccountInterface;
import DTOs.Account;

/**
 *
 * @author apprentice
 */
public class SavingsAccount extends Account implements AccountInterface{

    private final double INTEREST_RATE_PERCENT;
    private final double PENALTY_PERCENT;

    public SavingsAccount(double interestRate, double penalty) {
        this.INTEREST_RATE_PERCENT = interestRate;
        this.PENALTY_PERCENT = penalty;
    }
    
    @Override
    public boolean withdrawl(double amount) {
        return super.withdraw(amount, PENALTY_PERCENT, false);
    }
    
    @Override
    public boolean deposit(double amount){
        return super.deposit(amount);
    }
    
    @Override
    public String getBalance(){
        return super.getBalance();
    }
    
    public void timeTravel(int years){
        for (int i = 0; i < years; i++){
            balance = calculateAYearsInterest();
        }
    }
    
    private double calculateAYearsInterest(){
        return balance+((INTEREST_RATE_PERCENT/100)*balance);
    }

}
