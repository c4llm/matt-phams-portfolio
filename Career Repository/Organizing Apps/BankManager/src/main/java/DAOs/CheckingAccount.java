/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import DAOs.AccountInterface;
import DTOs.Account;

/**
 *
 * @author apprentice
 */
public class CheckingAccount extends Account implements AccountInterface{
    
    @Override
    public boolean withdrawl(double amount){
        return super.withdraw(amount, 0, true);
    }
    
    @Override
    public boolean deposit(double amount){
        return super.deposit(amount);
    }
    
    @Override
    public String getBalance(){
        return super.getBalance();
    }

    
}
